---
title: Math 342, Sep 17 Assignment
<#include nav-m342.yml>
---

Thanks for submitting great questions again!  I'm writing responses to some of your questions below, and will respond in class to the others.  Please feel free to continue to ask questions! 

### Submit at least one question about the material from the past week of class.

**I think I'm still a little confused about how the set.seed command words in RStudio. I understood that the number we put into this command will ensure we can reproduce the results(or get the same results?), but what do these numbers, for example set.seed(1), set.seed(200), set.seed(1000), mean?**

Without going into the technical details, the number (ie. 1, 200, 1000) is somewhat arbitrary.  The way that the computer generates random numbers is via a complicated function whose input is one of these numbers.  After this input is set, it generates numbers that appear to be random but are actually deterministic.  If we all agree to use the same input number, then we're all getting the same "random" numbers.

**I just want to clarify the notation for the random variable. So usually $X$ represents and assignment of a particular event and $Y$ represents the sum of the results in a particular event?**

No, this was just a coincidence.  It's typical to use the capital letters $X,Y,Z$ to represent random variables.  It just happened that in the examples we presented, I chose those letters.

**For the random variable, does the range refer to all the possible values? When we see something like {X=3}, are we expected to list out all the outcomes which correspond to 3?**

The range of $X$ refers to the possible values that $X$ can take.  As another example, $X$ could represent the number of tosses of a coin that need to be made until you get heads for the first time.  Then the range of $X$ would be the natural numbers $\\{1, 2, 3, ...\\}$ because you might get heads on the first toss, the second toss, ....  The events of the form $\\{X = k\\}$ consist of outcomes of the experiment (ie. the strings of tails/heads that occur).  For example, $\\{X = 1\\} = \\{H\\}, $\\{X = 2\\} = \\{TH\\}$, $\\{X = 3\\} = \\{TTH\\}$, and in general $\\{X = 1\\} = \\{T \\cdots TH\\}$, where there are $k-1$ $T$'s.

As another example, suppose $X$ refers to the height (in centimeters) of a randomly selected person in a country.  Since one doesn't know the precise heights of all the people in the country beforehand, we would say that the range is simply all positive real numbers even though it's unlikely for someone to be, for example, 1 million centimeters tall.  The event $\\{X = 100\\}$ would consists of the set of all people in the country who are 100 cm tall.  In the introductory problems, you're asked to list out all outcomes in events like $\\{X = k\\}$, but this is just to get practice understanding the meaning of such events.  Later, you typically won't be asked to do this.

**My question is related to the R portion of the class. So when I tried to stimulate the experiment with two coin tosses and record the sum of them, only 2,7, and 12 appear. I don't think it's a normal result...**

When running the 'sample' command, I bet that you sampled from the set 'c(1,6)'.  However, this means you're sampling from the set $\\{1, 6\\}$ when you should be sampling from the set $\\{1, 2, 3, 4, 5, 6\\}$.  You should change 'c(1,6)' to '1:6'.

**Is uniform distribution the same with normal distribution? The dice simulation we did in R shows up as a bell curved normal distribution in the graph. Is this always the case?**

The uniform distribution is different from the normal distribution.  If $X$ has the uniform distribution, say on all the values in the set $\\{2, 3, \ldots, 12\\}$, then each value is equally likely.  That is $P(X = k) = 1/11$ since there are 11 values in the set of numbers from 2 to 12.  However, in the dice simulation, we see that certain values are more likely than others.  For example, 7 is much more likely than 2.  This is because the event that $X=7$ consists of many more outcomes: $\\{X = 7\\} = \\{16, 25, 34, 43, 52, 61\\}$ versus $\\{X = 2\\} = \\{11\\}$.  Later, we'll see that the 'bell curve' shape of this distribution is not a coincidence (although it's not actually a normal distribution in this example).

**Can you explain how "replace = true" works in generating random numbers in R?**

Suppose you draw 3 elements from the set $\\{1, 2, \\ldots, 10\\}$ one at a time.  If you draw with replacement (ie. using "replace = true"), then after each draw, you record what element was drawn but put/replace it back in the set and then draw again until 3 draws have been made.  This means, you might see a certain element drawn more than once.  On the other hand, if you draw without replacement (ie. using "replace = false"), and you draw the element 2 first, then on the second draw, you'll draw from the set $\\{1, 3, 4, \\ldots, 10\\}$ which contains only 9 elements this time.

**Can one random variable be assigned to multiple outcomes in a random experiment? For example $X=4$ represent all outcomes smaller than 4?**

I don't think it's possible, but to make things clear, I think we need some specific context.  Let's consider a simple experiment, like rolling two dice.  Let $X$ represent the sum of the two dice.  Then $\\{X = 4\\} = \\{13, 31, 22\\}$ consists of all outcomes where the sum is exactly 4.  If we want the outcomes for which we roll a sum of 4 or less, we would write this as $\\{X \\leq 4 \\} = \\{X = 2\\} \\cup \\{ X = 3\\} \\cup \\{X = 4\\} = \\{11\\} \\cup \\{12, 21\\} \\cup \\{13, 31, 22\\}$.

On the other hand, we could let $Y$ denote the maximum of the two dice, instead of their sum.  Then $\\{Y = 4\\} = \\{14, 23, 24, 34, 44, 43, 42, 41\\}$ represents the outcomes for which at least one die was 4 and the other was a value less than 4.  However, we could not define a random variable $Z$ where $Z = k$ represents the outcomes where at least one die was at most $k$.  The reason for this is that the $Z$ value assigned to an outcome like $32$ would be ambiguous.  We wouldn't know for example, whether $32$ should be assigned to $Z = 3, 4, 5,$ or $6$ since $3, 4, 5$, $6$ are all values greater than or equal to $2$ and $3$.

**Random variables make sense to me in the context of many of the simple experiments we've talked about, such as flipping a coin n times and assigning X to be the number of heads that came up. However, I can imagine many scenarios in which the sample space is continuous so random variables only make sense expressed as a range of numbers. For example, if I throw a dart at a unit circle, the probability that I hit a given point, say (.1,-.1), seems infinitesimally small, so I think that in order to calculate meaningful probabilities we would have to break the unit circle into regions. My question then is whether random variables can be used similarly when the range of outcomes is continuous.**

This is a really good question, and you've made an important observation about it being natural to have a continuous sample space.  The short answer to your question, is yes, absolutely, random variables can be used when the sample space is continuous.  In fact, your suggestion of partitioning a continuous sample space into a grid, is actually the first step in creating a "continuous theory" of probability.  Isn't it reminiscent of Riemann sums in calculus and using partitioning and areas of rectangles to define the integral?  It's not a coincidence; the second half of the semester will be devoted to the continuous side of probability, for which we'll use the tools of calculus together with the intuition we gain about discrete probability in the first half of the semester.

