---
title: Math 211
footer: Last modified <#exec date>
<#include nav-m211.yml>
---

<script src="https://sagecell.sagemath.org/static/embedded_sagecell.js"></script>
<script>sagecell.makeSagecell({"inputLocation": ".sage"});</script>

This page will be updated throughout the semester with Sage commands relavent and useful to the topics we discuss.

### RREF

The following code allows you to compute the row reduced echelon form of a matrix.

<div class="sage">
  <script type="text/x-sage">
A = Matrix([[1,2,3],[3,2,1],[1,1,1]])
print A
print A.echelon_form()
  </script>
</div>

### Plotting vectors

As you get used to vectors, it's useful to visualize their plots individually, along with their linear combinations

<div class="sage">
  <script type="text/x-sage">
v = vector([1, 2])
w = vector([3, 1])
plot(v, color='red') + plot(w, color='blue') + plot(1.5*v + 2*w, color='black')
  </script>
</div>

### Matrix inverse

Given $A$, we can get $A^{-1}$ by running the following commands.  Note that there Sage will output a long error message
when $A$ is not invertible.

<div class="sage">
  <script type="text/x-sage">
A = Matrix([[1,2,3],[0,2,1],[0,0,3]])
print A.inverse()
  </script>
</div>

### Eigenvalues and eigenvectors

The following commands can be used to compute the characteristic polynomial $\\det(A-\\lambda I)$, the eigenvalues of $A$, and its eigenvectors.

<div class="sage">
  <script type="text/x-sage">
A = Matrix([[1,2,3],[0,2,1],[0,0,3]])  
print A.charpoly('t')
print A.eigenvalues()
print A.eigenvectors_right()
  </script>
</div>

### Your calculations

You can edit any of the above input boxes to do Sage calculations of your own.  Here is a box that you can use for any work.

<div class="sage"><script type="text/x-sage">






</script></div>


### Sage resources

Sage is a really powerful tool, and new features are constantly being added.  As such, we've given here just a glimpse at some of the calculations that can be done.  Here are some additional resources you might find helpful.

- [Sage Linear algebra quick reference][].  A cheat sheet of common commands relevant to linear algebra

- [A First Course in Linear Algebra][] by Robert A. Beezer.  A full, interactive textbook that makes use of Sage throughout its contents.  This might be useful for learning new commands applicable to things we learn in class.


[Sage Linear algebra quick reference]: https://wiki.sagemath.org/quickref?action=AttachFile&do=get&target=quickref-linalg.pdf
[A First Course in Linear Algebra]: http://linear.ups.edu/html/fcla.html

