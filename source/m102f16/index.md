---
title: Math 102
footer: Last modified <#exec date>
<#include nav-m102.yml>
---

Instructor: [Tim Chumley](/~tchumley)  
Office: Clapp 404b   
Phone: 538-2299  
e-mail: tchumley  
Office Hours: Tuesdays 4-5, Wednesdays 1-2, Thursdays 4-5, Fridays 11-12.

Textbook: *Single Variable Calculus: Early Transcendentals*, 7th Edition by James Stewart, ISBN: 9780538498678;  
on reserve in library under QA303.2 .S772 2012

---------

## Announcements #

Announcements will be posted here throughout the semester.

<!-- 
**Dec 14**: Better late than never, I've written [answers](answers.html) to anonymous questions
submitted throughout the semester.
 -->

**Dec 13**: Our classmate, Megan, has kindly shared with the class her table of series tests as it stands so far.
I've posted a scan [here](when-to-use-which-test-megan.pdf).  Please send yours along too if you'd like to share.

Also, here are my office hours for the reading days:

+	Wednesday 1-2:30pm, 5-6:30pm
+	Thursday 9am-1pm
+	Friday 9-11am, 1-3pm

**Dec 11**: The final exam schedule is posted [here][finals].  A few remarks on self-scheduled finals:

+	3 exam periods per day on Saturday, Sunday, and Monday
+	1 exam period on Friday night, 1 on Tuesday morning
+	only one exam can be taken per exam period
+	exam pick-up is in Hooker Auditorium (except on Friday night, when it's in Kendade Atrium)
+	you'll take your exam in an open-book room, but you'll only be allowed a calculator

**Nov 01**: As mentioned in class, the date of the next exam was pushed back a little to November 15.  Also, I will cover Nhu's evening help session on Wednesday.
			Please let me know if you plan to come to evening help after 8pm.  Otherwise, I might go home early at 8pm.

**Oct 30**: A small typo in Problem 2.1 of Problem Set 6 was fixed.  'y = 0' was changed to 'y = 2.'

**Oct 18**: Due to the LEAP symposium, we won't have class on Friday.  We'll have a take-home quiz which I'll post on the class web page and via email on Friday.

**Sep 30**: There is only WebWork homework assigned this week due to Thursday's exam.

**Sep 21**: Evening help desk hours for the semester are posted at the bottom of this page.

**Sep 20**: A small typo in problem 2.5 of problem set 2 was fixed.  Please check that you have the most
recent version.

**Sep 15**: [Here](./geometric-series-practice.pdf) is the worksheet handed out in class today.

## Syllabus #

Check the [syllabus][] for
all the important class policies (grades, attendance, etc.).


## Homework #

There will be weekly homework assignments comprising an online portion (Webwork) and a 
written portion.

- **Online assignments**. Weekly homework will be assigned through [Webwork][] and will be due **Wednesdays** at 11:59 pm. Please feel free to email me if you ever have any technical issues.
- **Written assignments**. In addition, a selection of textbook problems will be assigned to be due **Thursdays** at 5pm in the folder outside my office. 
	* These assignments are given so that you can practice writing mathematics and receive feedback on your progress.
	* If you would like feedback on a particular steps in a problem, then you can indicate this on your assignment.  
	* Each problem will be given a score of 3, 2, or 1. A 1 will be 
	given to indicate that to stay on track you should get help from your instructor or a TA.
- **Suggested problems**. There will also be suggested problems assigned with each textbook section covered 
	(see the Course Plan below). These are for practice and not to be turned in, but quiz and exam problems will be very similar.
	
	
  Written Assignment			Due			
  -----------					-----		
  [Problem set 1](./hw1.pdf)	Sep 15
  [Problem set 2](./hw2.pdf)	Sep 22
  [Problem set 3](./hw3.pdf)	Sep 29
  [Problem set 4](./hw4.pdf)    Oct 20
  [Problem set 5](./hw5.pdf)    Oct 27
  [Problem set 6](./hw6.pdf)    Nov 3
  [Problem set 7](./hw7.pdf)	Nov 10
  [Problem set 8](./hw8.pdf)	Dec 1
  [Problem set 9](./hw9.pdf)	Dec 9 (Friday)


## Quizzes #

There will be weekly 15 minute quizzes on **Fridays** except the
first week, Thanksgiving week, reading week, and on weeks when there is an exam. Quizzes 
will be one to two problems and cover material discussed through the previous
Tuesday's lecture. The precise sections covered will be listed below and problems will be 
similar to suggested problems.


  Quiz                              Date     Material               Solutions
  ---------                         -------- -------------          ----------
  Quiz 1                            Sep 16   11.1		            [(pdf)](quiz1s.pdf)
  Quiz 2	                        Sep 23	 11.2		            [(pdf)](quiz2s.pdf)
  Quiz 3	                        Sep 30	 11.4		            [(pdf)](quiz3s.pdf)
  [Quiz 4](quiz4.pdf) (take-home)	Oct 22   5.4, 5.5	            [(pdf)](quiz4s.pdf)
  Quiz 5							Oct 29   6.1, 6.2 (discs only)  [(pdf)](quiz5s.pdf)
  Quiz 6							Nov 4	 6.3, 7.1				[(pdf)](quiz6s.pdf)
  Quiz 7							Nov 11	 7.3, 7.4				[(pdf)](quiz7s.pdf)
  Quiz 8							Dec 2	 7.8, 11.3				[(pdf)](quiz8s.pdf)
  Quiz 9							Dec 9	 11.8, 11.9				[(pdf)](quiz9.pdf)




## Exams #

There will be three exams:


  Exam         Date     	Time and Location       Material                   		  Study material             							Solutions
  ------------ -------- 	-------------------- 	-----------------------    		  -------------------------- 							----------------------
  Exam 1       Oct 06   	in class                11.1-11.6                  		  [(pdf)](review1.pdf)       							[(pdf)](exam1s.pdf)
  Exam 2       Nov 15   	in class				5.3, 5.5, 6.1-6.3, 7.1, 7.3, 7.4  [(pdf)](review2.pdf) [(solutions)](review2s.pdf)   	[(pdf)](exam2s.pdf)
  Final		   Dec 16-20	self-scheduled			cumulative			       		  [(pdf)](review3.pdf) [(solutions)](review3s.pdf)    	.



## Course plan #

Below is a rough outline of the sections to be covered week by week
through the semester, including suggested textbook problems. Some of
this is subject to change, so please check regularly.


  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Week              Sections      Suggested problems													In-class activities
  ----------------- ---------     ----------------------------											-----------------------
  Sep 05 - Sep 09   11.1          **11.1**:	4-14 even, 18, 22, 40, 46, 60, 62, 66

  Sep 12 - Sep 16   11.1, 11.2    **11.2**: 4, 6, 8, 12, 14, 16, 20, 22, 28, 34, 36, 38, 42, 48, 70		[Geometric series](geometric-series-practice.pdf)\\
  																										[More on series](more-series.pdf)

  Sep 19 - Sep 23   11.3, 11.4    **11.4**:	4, 8, 10, 12, 16, 20, 30, 37								[Limit comparison test](limit-comparison-test.pdf)
								  

  Sep 26 - Sep 30   11.5, 11.6    **11.5**:	6, 7, 8, 10, 16, 24, 28 \\ 									[Alternating series](alternating-series.pdf)\\
								  **11.6**:	2, 4, 6, 8, 10, 12, 14, 18, 20, 24							[Ratio test](ratio-test.pdf)

  Oct 03 - Oct 07   5.1, 5.2      **5.2**: 33-40, 48, 49												[Introduction to integration](intro-to-integration.pdf)
  
  Oct 10 - Oct 14   5.3, 5.4      **5.3**: 8-42 even, 61, 76, 77 \\										[Fundamental theorem](ftoc-substitution.pdf)\\
  								  **5.4**: 22-34 even, 51-59 odd, 63-69 odd \\							[Net change and symmetry](net-change-symmetry.pdf)
  								  
  								  
  Oct 17 - Oct 21   5.5, 6.1      **5.5**: 8-24 even, 32-40 even, 54, 56, 62, 64 \\						
  						          **6.1**: 6, 8, 10, 17, 18, 19, 21, 27, 42, 46, 50, 52
  
  Oct 24 - Oct 28   6.2, 6.3      **6.2**: 1-17 odd, 94 \\												[Discs and washers](discs-washers.pdf)\\
  	                              **6.3**: 4, 6, 12, 14, 18, 38, 42, 44, 48								[Washers and shells](washers-shells-solutions.pdf)
  	                              
  Oct 31 - Nov 04   7.1, 7.3      **7.1**: 4, 6, 10, 18, 34, 38, 60 \\									[Integration by parts](integration-by-parts-solutions.pdf)\\
  								  **7.3**: 1, 3, 5, 11, 13, 18, 43 \\									[Pizza warmup](trig-sub-warmup.pdf), [Trig substitution](trig-sub.pdf) \\
  								  
                                  
  Nov 7 - Nov 11    7.4           **7.4**: 8, 10, 12, 20, 22, 34, 51, 66								[Partial fractions](partial-fractions.pdf)

  Nov 14 - Nov 18   7.8, 11.3     **7.8**: 2, 6, 8, 14, 22, 28, 30, 34 \\								[Improper integrals](improper-integrals.pdf)\\
                                  **11.3**: 2, 16, 22, 24 \\   											[Integral test](integral-test.pdf) \\
     
  
  Nov 21 - Nov 25   11.8         																																

  Nov 28 - Dec 02   11.8, 11.9    **11.8**: 4, 8, 10, 14, 18, 20, 22\\									[Intervals of convergence](intro-to-power-series-solutions.pdf)\\
                                  **11.9**: 4, 8, 14, 18												[New power series from old](power-series-manipulations.pdf)


  Dec 05 - Dec 09   11.10		  **11.10**: 2, 6, 14, 30, 66, 68										[Taylor and Maclaurin series](taylor-series.pdf)\\
  																										[More on Taylor series](more-taylor-series.pdf)
                                  
  Dec 12 - Dec 16	Review	    
 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




## Getting help #

Here's a few ways to get help:

-   **Office hours** (listed at the top of this page).  These are times I hold on my schedule 
	to be available for students.  I like to use these times as open drop in sessions 
	where there may be multiple students getting help simultaneously, but it's expected 
	that you come with specific questions about notes, homework problems, or the text that
	you have thought about already.  If you need to schedule a time to meet with me 
	one-on-one, either to discuss something private or because you can't make my office 
	hours, please send me an email with times that you are available to meet, and I will 
	find a time that works for both of us.
-	**Evening help desk**.  Student TA's staff a math-oriented help desk most evenings in Clapp.
	More information, including a schedule, will be posted here.
	+ Mondays 7-9 pm with Gabi in Clapp 407
	+ Tuesdays 7-9 pm with Mirha in Clapp 416
	+ Wednesdays 6:45-8:45 pm with Nhu in Clapp 420
-	**Study groups**.  I strongly encourage you to form small groups and work together on problems.
	My advice though is to try problems by yourself before discussing them.  We have 
	several spaces on the fourth floor of Clapp where students can work together for 
	extended periods of time.
	
	
[webwork]: https://ww.mtholyoke.edu/webwork2/Math102-02FA16/
[syllabus]: ./syllabus
[finals]: https://www.mtholyoke.edu/sites/default/files/registrar/docs/FA16_Exam_Schedulev2.pdf

