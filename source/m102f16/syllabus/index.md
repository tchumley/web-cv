---
title: Math 102 Syllabus
<#include nav-m102.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley   

---------

**Course**: Math 102, Calculus II 

**Prerequisites**: Math 101, Calculus I, or equivalent

**Textbook**: *Single Variable Calculus: Early Transcendentals*, 7th Edition by James Stewart, ISBN: 9780538498678

**Learning goals**: During the semester you will be learning how to

- use a Riemann sum to approximate quantities in geometry and applications.
- evaluate integrals using appropriate techniques including substitution, integration by parts, and partial fraction decomposition.
- set up differential equations to model phenomena in the real world.
- analyze solutions to differential equations qualitatively using slope fields.
- solve differential equations numerically using Euler's method.
- solve differential equtaions analytically using appropriate techniques including separation of variables.
- use appropriate tests to determine if a given series converges.
- find the interval of convergence of a power series.
- use Taylor series to approximate important functions including the sine, cosine, exponential function, and logarithm.

**Office hours**: Tuesdays 4-5, Wednesdays 1-2, Thursdays 4-5, Fridays 11-12. 
	
These are times I hold on my schedule 
to be available for students.  I like to use these times as open drop in sessions 
where there may be multiple students getting help simultaneously, but it's expected 
that you come with specific questions about notes, homework problems, or the text that
you have thought about already.  If you need to schedule a time to meet with me 
one-on-one, either to discuss something private or because you can't make my office 
hours, please send me an email with times that you are available to meet, and I will 
find a time that works for both of us.

**Homework**: There will be weekly homework assignments (except on exam weeks) comprising an online portion and a written portion.  Your lowest online homework score and lowest written homework score will not be counted toward your grade.

**Quizzes**: There will be weekly 15 minute quizzes (except on exam weeks) similar to suggested textbook problems.  Your two lowest quiz scores will not be counted toward your grade.

**Exams**: There will be two midterm exams and a self-scheduled final. The midterms are tentatively scheduled for October 6th and November 10th. 

**Attendance**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class you should prepare by doing any assigned reading and suggested problems. No work missed due to unexcused absences can be made up. Excused absences are granted only in extreme circumstances and may require written documentation.

**Participation**: We all learn math by doing it. Please expect to talk about math in small groups as well as in whole-class discussions. Other classroom activities may involve worksheets, computer explorations, and informal presentations at the board.

**Late work, makeups**:  No late homework due to unexcused absence will be accepted.  Likewise, no quiz missed due to unexcused absence will be allowed to be made up.  Excused absences are granted only in extreme circumstances and may require written documentation.

**Grading**: Grades will be assigned based on homework, quizzes, and exams according to the following weighting:

- Homework: 20%
- Quizzes: 15%
- Exam I: 20%
- Exam II: 20%
- Final: 25%

Overall letter grades will be based on a scale no stricter than:

- 90-100:			A
- 80-89:			B
- 70-79:			C
- 60-69:			D
- 0-59:				F

**Academic integrity**: It is very important for you to follow the Honor Code in all of your work for this course. Collaboration on homework assignments is encouraged. However, it is important that you only write what you understand, and that it is in your own words. If you have any questions about what constitutes an Honor Code violation in this class please ask your instructor. Honor Code violations will be brought to the dean. The penalty for violating the Honor Code on any assignment, quiz or exam will be a score of 0 for it. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please contact AccessAbility Services, located in Wilder Hall B4, at (413) 538-2634 or accessability-services@mtholyoke.edu. If you are eligible, they will give you an accommodation letter which you should bring to me as soon as possible.


