---
title: Math 211
footer: Last modified <#exec date>
<#include nav-m211.yml>
---

Instructor: [Tim Chumley](/~tchumley)  
Office: Clapp 404b  
Phone: 538-2299  
e-mail: tchumley  
Office Hours: Mondays 4:15-5:15, Tuesdays 2:30-5:00, Wednesdays 1:00-2:00 & 4:15-5:15, Thursdays 2:30-3:30

Textbook: *Linear Algebra with Applications*, 5th Edition by Otto Bretscher, ISBN: 9780321796974; \\
on library reserve under QA184.2 .B73 2013

---------

## Announcements #

Announcements will be posted here throughout the semester.

**May 14**: Some of the final exam notes sheets that stood out are now [posted](notes-sheets.html).  Here's a great one:

![](/~tchumley/assets/img/m211/wow.jpg){width=300px class="img-responsive center-block"}

**Apr 27**: There is no quiz this week!  My office hours for reading week are as follows:

-	Monday: 4:15-5:15
-	Tuesday: 3:30-5
-	Wednesday: 1-2
-	Thursday: 1:30-3

**Apr 26**: The final exam schedule is posted [here][self-scheduled].

**Apr 25**: There was a computer glitch on Monday which made Problem Set 10 disappear temporarily.  It's
back now, but I've pushed the due date back to Friday in case this caused any issues in your study
plans.

**Feb 16**: Because of last week's snow day, I've shifted our exam schedule a little.  Our first
exam will now be March 2 in class.

**Feb 7**: [Here][gauss-jordan-calculator] is a link to a useful Gauss-Jordan elimination calculator.
In case you're still a little unsure of how it works, it goes through each step of the algorithm.

**Jan 31**: [Here][3dvectors] is a link to the plane and perpendicular vector visualization
tool that I showed in class today.



## Syllabus #

Check the [syllabus][] for
all the important class policies (grades, attendance, etc.).


## Homework #

There will be weekly homework assignments throughout the semester to be turned in, as well
as suggested problems to be considered as additional practice.

- **Written assignments**. A selection of problems taken from the textbook or other sources
	will be assigned to be due **Thursdays** at 5pm in the folder outside my office. 
	*	These assignments are given so that you can practice writing mathematics and 
		receive feedback on your progress.
	* 	If you would like feedback on a particular steps in a problem, then you can 
		indicate this on your assignment.  
	* 	Each problem will be given a score of 3, 2, or 1. A 1 will be given to indicate 
		that to stay on track you should get help from your instructor or a TA.
	*	Please refer to these [guidelines](guidelines.pdf) when writing your homework.
- **Suggested problems**. There will also be suggested problems assigned with each 
	textbook section covered (see the Course Plan below). These are for practice and not 
	to be turned in, but quiz and exam problems will typically be similar.
	
	
  Written Assignment			Due			
  -----------					-----		
  [Problem set 0](hw0.pdf)		Jan 27
  [Problem set 1](hw1.pdf)		Feb 2
  [Problem set 2](hw2.pdf)		Feb 10
  [Problem set 3](hw3.pdf)		Feb 17
  [Problem set 4](hw4.pdf)		Feb 23
  [Problem set 5](hw5.pdf)		Mar 9
  [Problem set 6](hw6.pdf)		Mar 23
  [Problem set 7](hw7.pdf)		Mar 30
  [Problem set 8](hw8.pdf)		Apr 6
  [Problem set 9](hw9.pdf)		Apr 20
  [Problem set 10](hw10.pdf)	Apr 28
  


## Quizzes #

There will be weekly 15 minute quizzes on **Fridays** except the
first week, Spring break week, reading week, and on weeks when there is an exam. Quizzes 
will be one to two problems and cover material discussed through the previous
Tuesday's lecture. The precise sections covered will be listed below and problems will be 
similar to suggested problems.


  Quiz          Date     Material         Solutions
  ---------     -------- -------------    ----------
  Quiz 1        Feb 3    1.1, 1.2         [(pdf)](quiz1s.pdf)
  Quiz 2		Feb 17	 2.1 - 2.3		  [(pdf)](quiz2s.pdf)
  Quiz 3		Feb 24   2.4			  [(pdf)](quiz3s.pdf)
  Quiz 4		Mar 9    3.1, 3.2		  [(pdf)](quiz4s.pdf)
  Quiz 5		Mar 23   3.3			  [(pdf)](quiz5s.pdf)
  Quiz 6		Mar 30	 3.4			  [(pdf)](quiz6s.pdf)
  Quiz 7		Apr 7	 6.1, 6.2		  [(pdf)](quiz7s.pdf)
  Quiz 8		Apr 21	 7.1 - 7.3		  [(pdf)](quiz8s.pdf)
  



## Exams #

There will be three exams.  The dates for the two mid-terms are subject to change slightly:


  Exam         Date     	Time and Location       Material                   		  Study material             							Solutions
  ------------ -------- 	-------------------- 	-----------------------    		  -------------------------- 							----------------------
  Exam 1       Mar 2	   	in class                Chapters 1, 2                     [(pdf)](review1.pdf) [solutions](review1s.pdf)		[(pdf)](exam1s.pdf)
  Exam 2       Apr 13    	in class				Chapters 3, 6					  [(pdf)](review2.pdf) [solutions](review2s.pdf)		[(pdf)](exam2s.pdf)
  Final		   May 4-8		[self-scheduled][]		cumulative			       		  [(pdf)](review3.pdf) [solutions](review3s.pdf)		.



## Course plan #

Below is a rough outline of the sections to be covered week by week
through the semester, including suggested textbook problems. Some of
this is subject to change, so please check regularly.


  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Week              Sections      Suggested problems													In-class activities
  ----------------- ---------     ----------------------------											-----------------------
  Jan 23 - Jan 27   1.1, 1.2      **1.1**:	5, 7, 13, 15, 18, 19, 24, 30, 33, 34, 41, 49 \\				[Geometry of solution sets](geometry-of-linear-systems.pdf) \\
  								  **1.2**:	5, 7, 9, 18, 21, 27, 36, 37, 43								[Reduced row-echelon form](rref.pdf) \\
  								  																		[Gauss-Jordan elimination](gauss-jordan.pdf)

  Jan 30 - Feb 3    1.3, 2.1      **1.3**:	1-19 odd, 34, 47, 52, 53, 55, 58, 63, 65 \\					[Rank](rank.pdf) ([solutions and summary](rank-solution.pdf))
  								  **2.1**:	1-3, 5, 9, 16-23, 33, 34, 36, 47, 60																	

  Feb 6 - Feb 10    2.2		      **2.2**:	1-15 odd, 20, 21, 26, 27, 28, 41, 42 \\
  								  

  Feb 13 - Feb 17   2.3, 2.4      **2.3**:	3-11 odd, 17, 19, 29, 30, 49, 51 \\							[Matrix products](matrix-products.pdf) \\
  								  **2.4**:	1-13 odd, 28, 34, 35, 37, 40, 43, 69, 72, 73, 75			[Matrix inverse](matrix-inverse.pdf)
  								 
  								  
  Feb 20 - Feb 24   3.1		      **3.1**:	1-15 odd, 23, 25, 31, 35, 39, 44, 49
  							      
  								 
  								 
  Feb 27 - Mar 3    3.2      	  **3.2**:	1-4, 11, 15, 19, 27, 32, 36, 37, 39, 42, 45
  
  
  Mar 6 - Mar 10	3.2, 3.3	  **3.3**:	3, 7, 9, 15, 27, 28, 38, 62-64, 82
  								  
  								  
  Mar 20 - Mar 24   3.3, 3.4      **3.4**:	1, 5, 9, 13, 17, 21, 23, 29, 37-42 odd, 45, 53, 57			[Summary of 3.1-3.3 ideas](ch3-quick-review.pdf)
  
  								  
  Mar 27 - Mar 31   6.1, 6.2      **6.1**:	1-43 odd, 44, 46, 56, 57 \\									[Determinants](determinants-solutions.pdf) \\
  								  **6.2**:	1-15 odd, 29, 30, 38, 39, 46								[More on determinants](more-determinants.pdf) \\
  								  
  								  
  Apr 3 - Apr 7     7.1, 7.2      **7.1**:	1-21 odd, 46-49, 59 \\ 										[Eigenstuff](eigen-stuff.pdf) \\
  								  **7.2**:	1-19 odd							 
  								  
  Apr 10 - Apr 14   7.2, 7.3	  **7.3**:	1-19 odd, 31, 41-47 odd, 53									[Finding eigenvalues](finding-eigenvalues.pdf)
  
  
  Apr 17 - Apr 21   7.3, 7.4      **7.4**: 1-33 odd 													[Two contrasting examples](diagonalizability.pdf) \\
  																										[Predator-prey model](predator-prey.pdf)
  																																

  Apr 24 - Apr 28   5.1, 5.2      **5.1**: 1-15 odd, 22-28 \\
  								  **5.2**:

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




## Getting help #

Here's a few ways to get help:

-   **Office hours** (listed at the top of this page).  These are times I hold on my schedule 
	to be available for students.  I like to use these times as open drop in sessions 
	where there may be multiple students getting help simultaneously, but it's expected 
	that you come with specific questions about notes, homework problems, or the text that
	you have thought about already.  If you need to schedule a time to meet with me 
	one-on-one, either to discuss something private or because you can't make my office 
	hours, please send me an email with times that you are available to meet, and I will 
	find a time that works for both of us.
-	**Evening help desk**.  Student TA's staff a math-oriented help desk most evenings in Clapp.
	Here is the schedule as it stands:
	*	Mondays, 6-8pm, in Clapp 407
	*	Wednesdays, 7-9pm, in Clapp 420 with Ayla and Young
	*	Thursdays, 7-9pm, in Clapp 420
	
	The Monday and Thursday night sessions will have TA's from other sections, but everyone is welcome to attend.
	You should feel free to ask the TA's for help on homework, as well as on any other topics from class.
-	**Study groups**.  I strongly encourage you to form small groups and work together on problems.
	My advice though is to try problems by yourself before discussing them.  We have 
	several spaces on the fourth floor of Clapp where students can work together for 
	extended periods of time.
	
	
[webwork]: https://ww.mtholyoke.edu/webwork2/Math102-02FA16/
[syllabus]: ./syllabus
[3dvectors]: http://www.math.unb.ca/~preynol1/math/sage/plane.html
[gauss-jordan-calculator]: http://www.gregthatcher.com/Mathematics/GaussJordan.aspx
[self-scheduled]: https://www.mtholyoke.edu/sites/default/files/registrar/docs/SP17_Exam_Schedule_v2.pdf

