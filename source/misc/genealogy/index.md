---
title: Math genealogy
footer: Last modified <#exec date>
<#include nav-main.yml>
---

Math genealogy
==============

<img style="float: right;" src="../../assets/img/chumley.png" width=400px class="img-responsive">


The [Mathematics Genealogy Project][mgp] is an interesting site that keeps a record of people with PhDs in mathematics, statistics, and related fields, as well as who their advisor was, and who their advisor's advisor was, and so on.

Here are the mathematical family trees of some people in the MHC math/stat department I made using the [Geneagrapher][] software:

-	[Tim Chumley][tc]
-	[Giuliana Davidoff][gd]
-	[Andrea Foulkes][af]
-	[Alanna Hoyer-Leitzel][ahl]
-	[Dan Kelleher][dk]
-	[Margaret Robinson][mr]
-	[Dylan Shepardson][ds]
-	[Jessica Sidman][js]
-	[Jordan Tirrell][jt]
-	[Ashley Wheeler][aw]

Please remind me to keep this updated!

[mgp]: https://www.genealogy.math.ndsu.nodak.edu
[geneagrapher]: https://github.com/davidalber/Geneagrapher

[tc]: http://www.mtholyoke.edu/~tchumley/assets/img/chumley.png
[gd]: http://www.mtholyoke.edu/~tchumley/assets/img/davidoff.png
[af]: http://www.mtholyoke.edu/~tchumley/assets/img/foulkes.png
[ahl]: http://www.mtholyoke.edu/~tchumley/assets/img/moeckel.png
[mr]: http://www.mtholyoke.edu/~tchumley/assets/img/robinson.png
[ds]: http://www.mtholyoke.edu/~tchumley/assets/img/shepardson.png
[js]: http://www.mtholyoke.edu/~tchumley/assets/img/sidman.png
[jt]: http://www.mtholyoke.edu/~tchumley/assets/img/tirrell.png
[dk]: http://www.mtholyoke.edu/~tchumley/assets/img/kelleher.png
[aw]: http://www.mtholyoke.edu/~tchumley/assets/img/wheeler.png

