---
title: Math 232
footer: Last modified <#exec date>
<#include nav-m232.yml>
---

<img style="float: right;" src="/~tchumley/assets/img/m232.png" width=350px class="img-responsive">

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 538-2299  
**e-mail**: tchumley  
**Office Hours**: <#include officehours-f18.txt>   

**Textbook**: *Book of Proof, 2nd edition*, by Richard Hammack, ISBN: 978-0989472104; \\
available as a free [e-text][]


---------

## Announcements ##

Announcements will be posted here throughout the semester.

**Nov 30**: For our unit on probability, we'll follow along with Hammack's [Discrete Mathematics][] book.

<details>
  <summary>Past announcements</summary>

**Oct 19**: Here's a reminder that we'll have class in Reese 304 today.

**Oct 10**: Like I mentioned in class, the average score on the exam was an 84%, the median was 92%, and they went well overall.
I am happy to talk over questions one-on-one with everyone, whether it's about some technical details, grading decisions, or anything else.

Here are some comments about revisions.  If you lost a significant number points on a problem then that indicates there is some learning that needs to take place on that topic, and I want to encourage it.  So, any student who scored below 75% overall and lost 7 or more points on a problem should:

1. Try to redo the problem on their own.
2. Write a sentence or two explaining what went wrong the first time (bad interpretation, didn't know the binomial theorem, etc).
3. Come see me after 1 and 2 and then we can talk and I'll give another problem like it to check if the concept has been learned.
4. After doing this they can earn back up to half the missed points on that problem.

I think this will allow students who have some learning left to do to have the motivation and opportunity to do so in a way that they can lift their grade.  I also want the process to be clear and fair for everyone.  Students who do revisions won't end up with higher grades than those who don't, but I want everyone to be aware of the policy.

**Sep 21**: Please complete the following online assignment before class on [Monday, September 24][]

**Sep 13**: Two announcements:

+	Please complete the following online assignment before class on [Monday, September 17][].
+	Remember that we'll have a short quiz (5-10 minutes at the start of class) on Monday 
	on truth tables and forming if, then statements, and we'll have a slightly longer quiz 
	(15 minutes at the start of class) on Wednesday on Chapter 1 material.

**Sep 11**: This semester we're adding one more TA resource.  Lu, our TA, will hold a few 20 minute one-on-one
tutoring sessions each week.  You should sign up in advance by reserving an appointment slot on this
[Google calendar](https://calendar.google.com/calendar/selfsched?sstoken=UUR6ZGJJUnZpMFYtfGRlZmF1bHR8ZmZkNjM2ZDI1YzIwY2E1YWY1NzAwZDEyOWVhYzVmMGE)
link.  Lu's plan is to be available Wednesdays, 5:00 to 6:00 pm, on the landing of the 4th floor of Clapp (near the big sloth fossil).

**Sep 10**: You've asked great questions in the online assignment over the weekend!  I'm posting some responses
[here](assignment-0910.html).

**Sep 8**: A few announcements:

+	Lu Yu is the TA for our class. She will hold evening help 
	on Thursdays 7:00 – 9:00 pm in Clapp 422.  There are TA's for other sections as well,
	who you should feel free to go to.  Here is the full evening help schedule:
	The evening help schedule is as follows:
	*	Wednesdays, 7:00 – 9:00 pm in Clapp 422 with Khanh Ngo
	*	Thursdays, 7:00 – 9:00 pm in Clapp 422 with Olivia Fisher and **Lu Yu**
+	I've made a [Google Group][] for our class.  Please make sure you join
	in order to receive emails from me with important announcements.
+	Please complete the following online assignment before class on [Monday, September 10][].

**Sep 7**: I skimmed through your survey responses and will look more closely over the weekend.  Thank you for
being honest and open in your responses.  For now, here are some responses/comments to things that came up a few times.

*	You'll be ok if you don't have experience in CS.  The course doesn't require much background knowledge,
	just some mathematical experience and interest, which you all have.
*	The course is a lot about learning how to learn and think logically.  This is useful if you want to go on
	in math or computer science, but also useful more generally.
*	We'll spend time learning how to write mathematically (which generally means writing logical arguments or proofs).
	This will probably be different than the kind of proof writing you did in high school, but in some ways, I think
	will feel pretty natural after a while.  It will be satisfying I hope.
*	The class might feel tough at times but I will do my best to make sure it's not overwhelming.

We should try to work together to make class a supportive environment.  Here are some positive classroom norms taken
from Prof. Sidman's page:

+	Everyone can learn mathematics at a high level.
+	Mistakes are valuable.
+	Questions are a normal part of the process of learning.
+	Math is about creativity and making sense.
+	Math is about connections and communication.
+	Math class is about learning, not performing.
+	Depth is more important than speed.
</details>


## Syllabus ##

Check the [syllabus][] for all the important class policies (grades, attendance, etc.).

## Homework ##

There will be weekly homework assignments throughout the semester to be turned in.

- **Written assignments**. A selection of problems taken from the textbook or other sources
	will be assigned to be due **Fridays in class**. 
	*	These assignments are given so that you can practice writing mathematics and 
		receive feedback on your progress.
	* 	If you would like feedback on a particular steps in a problem, then you can 
		indicate this on your assignment.  
	* 	Each problem will be given a score out of 5. A score of 1 will be given to indicate 
		that to stay on track you should get help from your instructor.
	*	Please read these [guidelines][] for writing up your homework solutions.
- **Suggested problems**. The book has a good variety of problems, both in terms of content
	and difficulty level.  As you study throughout the semester, you might consider browsing 
	and trying the problems at the end of the chapter for extra practice.  I'll always be happy
	to help over email or in office hours if you're stuck.
- **Redo's**.  You'll be allowed to redo some missed homework problems this semester.  Here are some details:
	*	You can attempt to redo problems once per assignment to earn back credit.
	*	You should write your redo in full on a new sheet of paper and attach the old assignment; make clear what has changed from your original solution.
	*	You should turn in your redo in class on the Friday after the original due date.  (Generally graded
		work will be returned the Monday after it's due.)
- **Collaboration**. I want you to work together on the written homework; 
	the process of explaining your ideas to one another really helps in learning math. However, you must write up the problems on your own,
	listing the people with whom you worked on the problems. Please acknowledge any source of help, including classmates or outside texts,
	by writing  "help from ..." at the top of your assignment.  Also, please only write what you understand so that I know where to help.	
			

Assignment																			Due			
--------------------																-----		
[Homework 0](hw0.pdf)																Sep 7
[Homework 1](hw1.pdf) [(solutions)](hw1s.pdf)										Sep 14
[Homework 2](hw2.pdf) [(solutions)](hw2s.pdf)										Sep 21
[Homework 3](hw3.pdf) [(solutions)](hw3s.pdf)										Sep 28
[Homework 4](hw4.pdf) [(solutions)](hw4s.pdf)										Oct 12
[Homework 5](hw5.pdf) [(solutions)](hw5s.pdf)										Oct 19
[Homework 6](hw6.pdf) [(solutions)](hw6s.pdf)										Oct 26
[Homework 7](hw7.pdf) [(solutions)](hw7s.pdf)										Nov 2
[Homework 8](hw8.pdf) [(solutions)](hw8s.pdf)										Nov 9
[Homework 9](hw9.pdf) [(solutions)](hw9s.pdf)										Nov 19
[Homework 10](hw10.pdf) [(solutions)](hw10s.pdf)									Nov 30
[Homework 11](hw11.pdf) [(solutions)](hw11s.pdf)									Dec 7


## Quizzes #

There will be a few quizzes during the semester.  There will be some short (5-10 minute) self-check quizzes 
on **Mondays** with straightforward questions (for example, stating a definition).
They are written to make sure you're comfortable with the basics.
These will be counted toward the participation part of your grade.
There will be some longer (15-20 minutes) quizzes on **Wednesdays** with problems similar to the previous week's homework.
These are intended to break up the material between exams and get everyone studying. 
The quizzes will be announced below with some advance notice and we'll discuss ways to study in class.


Quiz        Date     Material         			Solutions
---------   -------- -------------    			----------
Quiz 1      Sep 19   Chapter 1		  			[(pdf)](quiz1s.pdf)
Quiz 2		Sep 26   2.1 - 2.6		  			[(pdf)](quiz2s.pdf)
Quiz 3		Oct 17	 Chapter 4		  			[(pdf)](quiz3s.pdf)
Quiz 4		Oct 24	 Chapter 4 - 6	  			[(pdf)](quiz4s.pdf)
Quiz 5		Oct 31	 Chapter 7 - 9				[(pdf)](quiz5s.pdf)
Quiz 6		Nov 7	 GCD, Euclidean algorithm	[(pdf)](quiz6s.pdf)
Quiz 7		Nov 28	 Induction					[(pdf)](quiz7s.pdf)
Quiz 8		Dec 5	 Functions					[(pdf)](quiz8s.pdf)


## Exams ##

There will be three exams.  The dates for the two mid-terms are subject to change slightly:


Exam         	Date     	    Time and Location       Material                   		  	Study material             							   
------------ 	-------- 	    -------------------- 	-----------------------    		  	-------------------------- 							   
Exam 1       	Oct 5	   	    in class			  	Chapters 1 - 3	  		  		  	[review guide](review1.pdf) [(solutions)](review1s.pdf)  
Exam 2       	Nov 14	    	in class				Chapters 4 - 9, Levin 4.1 - 4.3		[review guide](review2.pdf) [(solutions)](review2s.pdf)  
Final			Dec 14 - 18 	self-scheduled			cumulative							[review guide](review3.pdf) [(solutions)](review3s.pdf)



## Course plan ##

Our plan is to cover most of chapters 1-7, 10 in the textbook, as well as some other sections and outside material 
on graph theory and probability.  Below is a rough outline of what is to be covered week by week through the semester. Please check 
back regularly for precise details on what is covered, as well as postings of in-class 
worksheets and other activities.  Also note that the days of the week under Sections in
the table below provide links to class notes.


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Week              			Sections		   									In-class activities													Problems
------------------------ 	-------			   									--------------------	     										------------------------------------------
Sep 3 – Sep 7     		    Wednesday: introduction \\							[First day worksheet](first-day.pdf) \\								1.1: 4, 6, 26, 32, 38,40 \\
							[Friday](09072018.pdf): 1.1, 1.3, 1.4				[Introduction to sets](intro-to-sets.pdf)							1.3: 2, 4, 8, 1.4: 2, 6, 12, 14, 18

Sep 10 – Sep 14   		    [Monday](09102018.pdf): 1.2, 1.5 - 1.7 \\			[Set operations](sets-operations.pdf) \\ 							1.2: 2b, 4, 14, 1.5: 2afh, 4bef \\
							[Wednesday](09122018.pdf): chapter 1 wrap up \\		[Venn diagrams](venn-diagrams.pdf) \\								1.6: 2, 1.7: 6, 12 \\
							[Friday](09142018.pdf): 2.1 - 2.6					[Implications, truth tables](and-or-not-implication.pdf)			2.1: 2, 4, 6, 8, 10; 2.2: 6, 8, 9, 10; 2.3: 2, 4, 10, 12; 2.4: 2, 4

Sep 17 – Sep 21   			[Monday](09172018.pdf): 2.7 - 2.10 \\				[Quantifiers, negation](quantifiers-negation.pdf)					2.7: 4, 6, 8; 2.9: 4, 6; 2.10: 4, 6, 8, 10; \\
							[Wednesday](09192018.pdf): 3.1 - 3.2 \\					[(solutions)](quantifiers-negation-sol.pdf) \\					3.1: 2, 4, 6, 8, 10; 3.2: 8, 10 \\
							[Friday](09212018.pdf): 3.3							[Counting lists](counting-lists.pdf) \\								3.3: 2, 6, 10, 12
																				[Counting subsets](counting-subsets.pdf)
							  
Sep 24 – Sep 28   			[Monday](09242018.pdf): 3.4 \\						[Binomial theorem, combinatorial proof](binomial-theorem.pdf) 	 	3.4: 2, 6, 10 \\
							[Wednesday](09262018.pdf): 3.4, 3.5 \\					[(solutions)](binomial-theorem-sol.pdf) \\						\\  
							[Friday](09282018.pdf): 3.5							\\   																3.5: 1, 7, 8
																				[Inclusion-exclusion](inclusion-exclusion.pdf)																					 	 

Oct 1 – Oct 5     			Monday: counting overview \\  						[More counting problems](more-counting.pdf)  
							Wednesday: review \\									[(solutions)](more-counting-sol.pdf)  
							Friday: exam																																																							

Oct 8 – Oct 12    			Monday: break \\																										\\   \\  
							[Wednesday](10102018.pdf): Chapter 4 \\				[Direct proof](direct-proof.pdf) \\									Ch. 4: 4, 6, 10 \\
							[Friday](10122018.pdf): Chapter 4					[Euclidean algorithm](euclidean-algo.pdf)							Ch. 4: 11, 16

Oct 15 – Oct 19				[Monday](10152018.pdf): Chapter 5 \\  	  			[Contrapositive, congruence](contrapositive.pdf) \\					Ch. 5: 2, 10, 16, 23, 24 \\  
							[Wednesday](10172018.pdf): Chapter 6 \\				[Contradiction](contradiction.pdf), 								Ch. 6: 2, 10 \\										
							[Friday](10192018.pdf): Chapter 7						[Number theory fact sheet](number-theory.pdf) \\  				Ch. 7: 8, 10, 26
																				[More on proofs](iff.pdf) 	 

Oct 22 – Oct 26   			[Monday](10222018.pdf): Chapter 8 \\  				[Proofs involving sets](set-proofs.pdf) \\	  						Ch. 8: 8, 10, 12, 26 \\
							Wednesday: Chapter 8,9 \\  							\\       															Ch. 9: 16, 28 \\
							[Friday](10262018.pdf): Diophantine equations		[Diophantine equations](diophantine-equations.pdf)  				Ch. 6: 6, 8; Ch. 8: 2, 28

Oct 29 – Nov 2    			[Monday](10292018.pdf): [intro graph theory][] \\	[Graph theory](graph-theory.pdf) \\       							4.1: 1, 2, 8  \\
							[Wednesday](10312018.pdf): [planar graphs][] \\  	[Planar graphs](planar-graphs.pdf) \\  								4.2: 4ac, 7, 10 \\
							[Friday](11022018.pdf): [coloring][]				[Vertex coloring](vertex-coloring.pdf)  							4.3: 4b, 5

Nov 5 – Nov 9	    		[Monday](11052018.pdf): coloring \\  				[More coloring](more-coloring.pdf) \\
							[Wednesday](11072018.pdf): induction \\				[Induction](induction.pdf)  
							[Friday](11092018.pdf): trees, induction			   

Nov 12 – Nov 16   			Monday: review \\  									  \\    
							Wednesday: exam \\     								  \\  	     		
							[Friday](11162018.pdf): trees, induction  			[More induction, trees](more-induction.pdf)   
						
Nov 19 – Nov 22   	   	    [Monday](11192018.pdf): Chapter 12 \\				[Functions](functions.pdf)
							Wednesday: break \\
						    Friday: break     	  

Nov 26 – Nov 30    			[Monday](11262018.pdf): 12.3 \\						[Pigeonhole principle](pigeonhole.pdf) \\
							Wednesday: 12.3 \\									[More pigeonhole principle](pigeonhole-ii.pdf) \\
							[Friday](11302018.pdf): [5.1][5.3]					[Probability introduction](probability-intro.pdf)

Dec 3 – Dec 7     			[Monday](12032018.pdf): [5.3][] \\    			    [Conditional probability](conditional-prob.pdf)
							[Wednesday](12052018.pdf): [5.5][5.3] \\				[(solutions)](conditional-prob-sols.pdf) \\
							Friday: 5.5											[Bayes' rule](bayes.pdf)
																					[(solutions)](bayes-sol.pdf)

Dec 10 – Dec 14			    Monday: wrap up \\									[Extra review](extra-review3.pdf)
							Wednesday: reading day \\								[(solutions)](extra-review3s.pdf)
						    Friday: exam period	begins		 

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


## Getting help ##

Here's a few ways to get help:

-   **Office hours** (listed at the top of this page).  These are times I hold on my schedule 
	to be available for students.  I like to use these times as open drop in sessions 
	where there may be multiple students getting help simultaneously, but it's expected 
	that you come with specific questions about notes, homework problems, or the text that
	you have thought about already.  If you need to schedule a time to meet with me 
	one-on-one, either to discuss something private or because you can't make my office 
	hours, please send me an email with times that you are available to meet, and I will 
	find a time that works for both of us.
-	**Evening TA help**.  Student TA's will hold office hours for the class several nights a week in Clapp.
	These hours will be an opportunity for you to ask questions on homework and class material.  You should also
	see this as a place where you can find classmates to work with on homework as a group.  **Lu Yu** is the official TA for our section, 
	but the sections will all cover similar material, 
	so you should feel free to attend others' sessions as well.  The evening help schedule is as follows:
	+	Wednesdays, 7:00 – 9:00 pm in Clapp 422 with Khanh Ngo
	+	Thursdays, 7:00 – 9:00 pm in Clapp 422 with Olivia Fisher and **Lu Yu**
-	**Study groups**.  I strongly encourage you to form small groups and work together on problems.
	My advice though is to try problems by yourself before discussing them.  We have 
	several spaces on the fourth floor of Clapp where students can work together for 
	extended periods of time.
	
## Resources ##

-	**Other texts**.  Our textbook is a great, easy to read resource.  We might also refer to the following for a few miscellaneous topics.
	+	[Discrete Mathematics: an open introduction][]. A reference we'll use for graph theory material.
	+	[Building Blocks for Theoretical Computer Science][].  A really nice reference for what we cover, but this text has a few extra topics of particular interest to computer scientists (eg. analysis of algorithms).
-	**LaTeX** is a typesetting system that makes it easy to type math.  Here are some resources you might find useful.
	+ [Overleaf][]. An online LaTeX writing tool that lets you use LaTeX in the cloud.
	+ [MacTeX][]/[TeXLive][].  An installation package for getting LaTeX running on your 
	computer.  MacTeX is for macOS and TeXLive is for other operating systems.
	+ [quick reference][].  This file has a list of common commands.
	+ [LaTeX file template][].  This is a simple file that lets you get started making a full 
	  LaTeX document.  This is the [pdf][LaTeX file template pdf] file generated by the template.

	
[e-text]: https://www.people.vcu.edu/~rhammack/BookOfProof2/index.html
[syllabus]: ./syllabus
[guidelines]: http://mtholyoke.edu/~tchumley/assets/pdf/homework-guidelines.pdf
[installation guide]: http://guides.mtholyoke.edu/c.php?g=762365&p=5467440

[MacTeX]: http://tug.org/mactex/
[TeXLive]: https://www.tug.org/texlive/
[Overleaf]: https://www.overleaf.com
[quick reference]: https://reu.dimacs.rutgers.edu/Symbols.pdf
[LaTeX file template]: http://www.mtholyoke.edu/~tchumley/latex-presentation/hello-world.tex
[LaTeX file template pdf]: http://www.mtholyoke.edu/~tchumley/latex-presentation/hello-world.pdf

[Discrete Mathematics: an open introduction]: http://discrete.openmathbooks.org
[Building Blocks for Theoretical Computer Science]: http://mfleck.cs.illinois.edu/building-blocks/

[Google Group]: https://groups.google.com/a/mtholyoke.edu/forum/#!forum/math-232-01-fall-2018-g
[Monday, September 10]: https://goo.gl/forms/eXkuXwTfBe7l6sps2
[Monday, September 17]: https://goo.gl/forms/kbutdkK2SG4mLQpo1
[Monday, September 24]: https://goo.gl/forms/rHAxAhAZbQKXZmPj1

[intro graph theory]: http://discrete.openmathbooks.org/dmoi/sec_gt-intro.html
[planar graphs]: http://discrete.openmathbooks.org/dmoi/sec_planar.html
[coloring]: http://discrete.openmathbooks.org/dmoi/sec_coloring.html

[Discrete Mathematics]: http://www.people.vcu.edu/~rhammack/Discrete/index.html
[5.3]: http://www.people.vcu.edu/~rhammack/Discrete/Alpha.pdf#page=142

