#!/bin/bash
# This script performs like bib-include.sh but produces no links and does not include the reversed order html tag.  Produces markdown file
B2BFLAGS="--no-comment"
bib2bib $B2BFLAGS -c 'author : "Chumley"' --remove doi --remove eprint --remove url $1 |\
	 bibtex2html -nodoc -nokeys --no-keywords --style abbrv -q -d |\
	 pandoc -f html -t markdown | \
	 sed -n '/------------------/q;p' | \
	 awk ' /^$/ { print; } /./ { printf("%s ", $0); }' | \
	 tail -r | \
	 nl -bt -s '. ' -w 1
