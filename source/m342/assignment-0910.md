---
title: Math 342, Sep 10 Assignment
<#include nav-m342.yml>
---

Thanks for submitting great questions!  I'm writing responses to some of your questions below, and will respond in class to the others.  Please feel free to continue to ask questions!
I'll continue to send out similar assignments through the semester.

### Submit at least one question about the material from the first week of class. 

**Is it always possible to write an event as two disjoint sets? I see how this helps in the context of the proofs we looked at but sometimes have trouble finding how to write the values as two disjoint sets on my own.**

Yes, it's always possible to do this.  My approach is usually the following: I look for sets which are easy to describe in terms of intersections, unions, or complements and make those my disjoint sets in the decomposition.
It's hard to say too much in terms of general rules because the approach depends on the information given.  You have to keep in mind what is known as you break up events.

**I'm still a bit confused with the R code for 'sample(0:10, 2, replace = TRUE)'. If I don't specify the round function, would it mean R randomly picks 2 numbers from 0 to 10 even include decimal numbers such as 2.1?**

The '0:10' command is actually the set $\\{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10\\}$, so running the sample command with this set will always yield a whole number.  The 'runif' command is the one that gives real numbers in an interval.

**Do we always have to write the probability of an event as 4/16 instead of 1/4 to indicate how many outcomes are included?**

You should usually try to simplify your fractions, but having an intermediate step like writing 4/16 can definitely help indicate where your answer came from.
However, the best thing to do is to write in words how you got your answer.

**Where do DeMorgan's Laws come from? I'm not confused by them, but I would be interested to see a proof.**

This is a good question!  The proof is actually pretty intuitive.  Since I don't want to veer off too far into discrete math class territory, here is a [link](https://en.wikipedia.org/wiki/De_Morgan's_laws#Formal_proof).  I'd  be happy to talk one on one and go through it with you too.

**Can Venn diagram serve as a way of proving the formulas that we have?**

Venn diagrams are a good way to get intuition for what a formula should be, but for proofs, we should use just properties of probability functions.
Right now, we only have a small set of tools (like the addition rule for disjoint sets or inclusion-exclusion formulas), but we'll continue to build up on this set of tools.

**We laid out clearly how to find many different scenarios considering probabilities of events A and B. But, how exactly do I find P(AB)?**

It depends on specific context.  For example, if you're told what $P(A \\cup B), P(A)$, and $P(B)$ are, then you can use our inclusion-exclusion formula.  On the other hand, you might have to use counting to do this.
For example, suppose $A$ is the event that you get heads on the first toss of a coin and $B$ is the event that you get heads on the second toss of a coin.  Then $P(AB)$ is asking for the probability that you get heads
on both the first and second tosses.  There's two approaches to this.  One uses counting: count the number of outcomes in two coin tosses (4 outcomes: HH, HT, TH, TT) and thus P(AB) = 1/4.  The other approach uses the 
notion of independence which we'll discuss in chapter 3.

**What will R be used for later on in our course? How helpful will it be?**

We'll use R to simulate random experiments in order to estimate or approximate things like probabilities or average values.  The point of this is that it's often easy to describe how a random experiment is performed
(which means it's easy to write code to simulate the experiment), but difficult, or perhaps unintuitive at first, to analyze it with pen and paper.  Thus, simulation will be a good way to check or give intuition for our theory work.
I think it will be really helpful also for making sure we really understand how the random experiments we're interested in work.

**How can we work with probability in countably infinite sample spaces?**

We'll see examples of countably infinite sample spaces as we go through the semester and how to work with them.  For now, I'll just give one easy to describe example.  Consider the experiment of tossing a coin repeatedly until
the first time heads comes up.  The sample space for this experiment consists of all the sequences of coin tosses which end in heads: $\\{H, TH, TTH, TTTH, TTTTH, \ldots\\}.  You can see that the possible outcomes include
longer and longer sequences of coin tosses (but these are outcomes that become less and less likely as they get longer, at a geometric rate, so the probabilities do add up to 1).

