---
title: Math 339
footer: Last modified <#exec date>
<#include nav-m339.yml>
---

<img style="float: right;" src="../assets/img/m339.png" width=400px class="img-responsive">

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 538-2299  
**e-mail**: tchumley  
**Office Hours**: <#include officehours-s19.txt>  

Textbook: *Introduction to Stochastic Processes with R* by Robert P. Dobrow, ISBN: 9781118740651; \\
on library reserve under QC20.7.S8 D63 2016; \\
available as a free [e-text][] 

---------

## Announcements ##

Announcements will be posted here throughout the semester.

  **Apr 29**: You may turn in redos for Homework 10 together with your final exam, which is due May 7 at 12:00 pm.  I will distribute
your exam on Thursday morning and you can hand it in any time before the due date.  If I'm not in my office, you can slide
it under the door.

My office hours for reading week are as follows:

-	Monday: 4:00-5:00
-	Tuesday: 11:00-12:00
-	Wednesday: 9:00-10:00
-	Thursday: 11:00-12:00
-	Friday: 1:00-2:00





<details>
  <summary>Past announcements</summary>
  
  **Feb 25**: You may again redo any problems from Homework 4 for Friday this week (including both TA and professor homework).
Note that we have no homework due this week, due to the midterm, but redo's are still due on the usual day.
  
  **Feb 18**: You may redo any problems from Homework 3 for Friday this week (including both TA and professor homework).
  
  **Feb 13**: We'll have room 416 reserved as usual tonight from 7 to 9 pm for group study.
This week, our classmates Sudiksha and Olivia are the designated group study people who will be there.

**Feb 4**: This week our classmate Isabell will be our designated evening group study person on Wednesday 7-9 pm in Clapp 416.
This is a chance to meet up with classmates to work.  I'd like everyone to volunteer to be a designated
evening group study person twice this semester as part of your participation grade.  Knowing that a classmate is guaranteed to be
there will encourage others to attend I hope.

**Feb 4**: This week you may redo any professor problem from Homework 1.
Please follow the directions about submission in the Homework section below.
  
  **Dec 17**: Please check back in late January for updates on the course. 


</details>


## Syllabus ##

Check the [syllabus][] for all the important class policies (grades, attendance, etc.). 


## Homework ##

There will be weekly homework assignments throughout the semester to be turned in, as well
as some suggested problems to be considered as additional practice.

- **Written assignments**. A selection of problems taken from the textbook or other sources
	will be assigned to be due **Fridays in class**. 
	*	These assignments are given so that you can practice writing mathematics and 
		receive feedback on your progress.
	* 	If you would like feedback on a particular steps in a problem, then you can 
		indicate this on your assignment.  
	*	Please read these [guidelines][] for writing up your homework solutions.
- **Collaboration**. I want you to work together on the written homework; 
	the process of explaining your ideas to one another really helps in learning math. However, you must write up the problems on your own,
	listing the people with whom you worked on the problems. Please acknowledge any source of help, including classmates or outside texts,
	by writing  "help from ..." at the top of your assignment.  Also, please only write what you understand so that I know where to help.
- **Rewrites**. Homework is for practice, you are not expected to write perfect answers from the start! 
	If you would like to earn back lost credit on a homework assignment,
	you may resubmit it to me the Friday after it's returned to you.
	I will make note on your assignment about which problems you can redo.
	If you’d like to submit a redo, you must write your new answers on new paper,
	and attach your original assignment so that I can compare the two and see your improvement.
	
			

Assignment																			Due			
--------------------																-----		
[Homework 0](hw0.pdf)																Jan 25
[Homework 1](hw1.pdf) [(solutions)](hw1s.pdf)										Feb 1
[Homework 2](hw2.pdf) [(solutions)](hw2s.pdf)										Feb 8
[Homework 3](hw3.pdf) [(solutions)](hw3s.pdf)										Feb 15
[Homework 4](hw4.pdf) [(solutions)](hw4s.pdf)										Feb 22
[Homework 5](hw5.pdf) [(solutions)](hw5s.pdf)										Mar 8
[Homework 6](hw6.pdf) [(solutions)](hw6s.pdf)										Mar 22
[Homework 7](hw7.pdf) [(solutions)](hw7s.pdf)										Mar 29
[Homework 8](hw8.pdf) [(solutions)](hw8s.pdf)										Apr 5
[Homework 9](hw9.pdf) [(solutions)](hw9s.pdf)										Apr 19
[Homework 10](hw10.pdf)																Apr 26


  
## Quizzes ##
  
There will be some quizzes at the beginning of class on **Wednesdays** with problems similar to homework.
These are intended to break up the material between exams and get everyone studying.  
The quizzes will be announced below with some advance notice.

Quiz          	Date     	Material         		Solutions
---------     	-------- 	-------------    		----------
Quiz 1        	Feb 6	 	2.1 – 2.3				[(pdf)](quiz1s.pdf)
Quiz 2			Feb 13		3.1 – 3.2				[(pdf)](quiz2s.pdf)
Quiz 3			Feb 20		3.3						[(pdf)](quiz3s.pdf)
Quiz 4			Mar 20		4.1 – 4.4				[(pdf)](quiz4s.pdf)
Quiz 5			Apr 3		6.1 – 6.4				[(pdf)](quiz5s.pdf)
Quiz 6			Apr 24		7.1 – 7.4				[(pdf)](quiz6s.pdf)
  


## Exams ##

There will be two midterms and final.  The dates for the mid-terms are subject to change slightly:


Exam         	 			Date     	Time and Location       Material                   		  Study material             		Solutions						
------------ 				-------- 	-------------------- 	-----------------------    		  -------------------------- 		-----------
Exam I		      			Feb 27		in class, take-home     Chapters 2, 3					  [review guide](review1.pdf)		[in-class](exam1s-inclass.pdf), [take-home](exam1s-takehome.html)		  		  		  
Exam II						Apr 10  	in class, take-home		Chapter 4, 6					  [review guide](review2.pdf)		[in-class](exam2s-inclass.pdf), [take-home](exam2s-takehome.pdf)
Final exam		 			May 3 - 7	take-home				Chapter 5, 7





## Course plan ##

Our plan is to cover parts of the textbook chapters 1 – 7.  Below is 
a rough outline of what is to be covered week by week through the semester. Please check 
back regularly for precise details on what is covered, as well as postings of in-class 
worksheets and other activities.  Also note that the days of the week under Sections in
the table below provide links to class notes.

 
------------------------------------------------------------------------------------------------------------------------------------------------------------
Week              			Sections		   								In-class activities																
------------------------ 	-------			   								--------------------	     													
Jan 21 - Jan 25     		[Wednesday](01232019.pdf): 1.1, 1.2 \\    		[Introduction](intro.pdf) \\
							[Friday](01252019.pdf): 2.1, 2.3				[n-step transitions](n-step-transitions.pdf)

Jan 28 - Feb 1	   			[Monday](01282019.pdf): 2.2, 2.3, 2.4 \\  		[Random walks](random-walks.pdf), [R code](01-28-2019.R) \\
							[Wednesday](01302019.pdf): 2.2, 2.3, 2.5 \\  	[Joint distributions, simulation](simulation.pdf), [R code](01-30-2019.R) \\
							Friday: 2.2										[More models](more-models.pdf), [Rmd code](02-01-2019.Rmd),
																				[Rmd html](02-01-2019.html)

Feb 4 - Feb 8   			[Monday](02042019.pdf): 3.1, 3.2 \\				[Limiting, stationary distributions](limiting-stationary-dist.pdf) \\
							[Wednesday](02062019.pdf): 3.1, 3.2 \\			[Mixtures of Markov chains](mixtures.pdf) \\
							[Friday](02082019.pdf): 3.3						[Communication classes](communication.pdf)				
							 
Feb 11 - Feb 15   			[Monday](02112019.pdf): 3.3 \\					[Canonical decomposition](canonical-decomposition.pdf) \\
							[Wednesday](02132019.pdf): 3.3, 3.4 \\			[Excursions](excursions.pdf)
							[Friday](02152019.pdf): 3.3, 3.4

Feb 18 - Feb 22     		[Monday](02182019.pdf): 3.5, 3.6 \\				[Ergodicity](ergodicity.pdf) \\
							[Wednesday](02202019.pdf): 3.6, 3.8 \\			\\
							[Friday](02222019.pdf): 3.8						[Absorbing chains](absorbing-probs.pdf)
																																																							   
Feb 25 - Mar 1    			Monday: review \\								[Review guide solutions](review1s.html) \\
							Wednesday: exam \\								\\
							[Friday](03012019.pdf): 4.1, 4.2  				[Branching processes demo](branching.html)

Mar 4 - Mar 8   			Monday: no class \\								\\
							[Wednesday](03062019.pdf): 4.3 \\				[Probability generating functions](pgf.pdf) \\
							[Friday](03082019.pdf): 4.4						[Extinction probabilities](extinction.pdf), [R code](extinction.R)
					   
Mar 11 - Mar 15   			spring break

Mar 18 - Mar 22    			[Monday](03182019.pdf): 6.1 \\					[Poisson processes](poisson-intro.pdf) \\
							[Wednesday](03202019.pdf): 6.2 \\				[Arrival times](arrival-times.pdf)
							[Friday](03222019.pdf): 6.2, 6.4

Mar 25 - Mar 29    			[Monday](03252019.pdf): 6.4 \\					[Thinning and superposition](thinning-superposition.pdf) \\
							[Wednesday](03272019.pdf): 6.3, 7.1 \\			[Continuous time Markov chains](ctmc-intro.pdf) \\
							[Friday](03292019.pdf): 7.2						[Transition rates](transition-rates.pdf)

Apr 1 - Apr 5   			[Monday](04012019.pdf): 7.3 \\					[Infinitesimal generator](generator.pdf) \\
							[Wednesday](04032019.pdf): 7.4 \\				[Limiting distribution](ctmc-limiting.pdf) \\
							Friday: 7.4										[Stationary distributions](ctmc-stationary.pdf)

Apr 8 - Apr 12   	    	Monday: review \\								[Review guide solutions](review2s.pdf)
							Wednesday: exam \\
							Friday: senior symposium

Apr 15 - Apr 19    			[Monday](04152019.pdf): 7.4 \\					\\
							[Wednesday](04172019.pdf): 7.5 \\				[Detailed balance](detailed-balance.pdf) \\
							[Friday](04192019.pdf): 7.6						[Queueing theory](queues.pdf)

Apr 22 - Apr 26     		[Monday](04222019.pdf): 5.1 \\					\\
							Wednesday: 5.2 \\								[Knapsack problem](knapsack.html), [RMarkdown file](knapsack.rmd)
							[Friday](04262019.pdf): 5.2 

Apr 29 - May 3				[Monday](04292018.pdf): wrap up \\				[Cryptography](cryptography.pdf), [RMarkdown file](cryptography.Rmd)
							Wednesday: reading day \\
							Friday: exam period	begins	
							  
----------------------------------------------------------------------------------------------------------------




## Getting help ##

Here are a few ways to get help:

-   **Office Hours**: <#include officehours-s19.txt>  
-	**Study groups**: Other students in the class are a wonderful resource.
	I want our class to feel like a community of people working together.
	Even though our class won't have a TA or evening help, 
	I have reserved the following as a designated meeting spot each week:
	+	Tuesdays, 7:00 – 9:00 pm in Clapp 416 (room shared with other 300-level students)
	+	Wednesdays, 7:00 – 9:00 pm in Clapp 416 (room shared with other 300-level students)
	+	Thursdays, 7:00 – 9:00 pm in Clapp 416 (room shared with other 300-level students)
	
## Resources ##

-	Our textbook has a useful collection of [R scripts][] available; contained
there are all the R code snippets you'll notice interspersed in the text.
	
[e-text]: https://ebookcentral.proquest.com/lib/mtholyoke/detail.action?docID=4462510
[syllabus]: ./syllabus
[DataCamp]: https://www.datacamp.com
[guidelines]: http://mtholyoke.edu/~tchumley/assets/pdf/homework-guidelines.pdf
[R scripts]: http://www.people.carleton.edu/~rdobrow/stochbook/RScripts.html
[Presentation rubric]: http://mtholyoke.edu/~tchumley/assets/pdf/presentation-rubric.pdf

[MacTeX]: http://tug.org/mactex/
[TeXLive]: https://www.tug.org/texlive/
[LaTeXiT]: https://www.chachatelier.fr/latexit/
[quick reference]: https://reu.dimacs.rutgers.edu/Symbols.pdf
[LaTeX file template]: http://www.mtholyoke.edu/~tchumley/latex-presentation/hello-world.tex
[LaTeX file template pdf]: http://www.mtholyoke.edu/~tchumley/latex-presentation/hello-world.pdf
[Beamer template]: http://www.mtholyoke.edu/~tchumley/latex-presentation/BeamerTemplate.tex
[Beamer template pdf]: http://www.mtholyoke.edu/~tchumley/latex-presentation/BeamerTemplate.pdf



