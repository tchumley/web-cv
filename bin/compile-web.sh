#!/bin/bash

GIT_ROOT=$(git rev-parse --show-toplevel)
INCLUDE_PATH=$GIT_ROOT/include
METADATA=$INCLUDE_PATH/metadata.yaml
GPP=/opt/local/bin/gpp
PANDOC=/usr/local/bin/pandoc

files=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | grep -o '^source\/.*\.md' | \
        while read -r line; do echo "$line"; done)

if [ -n "$files" ]
then
        echo "Files for compilation:"
        echo "$files"

        while read -r f;
        do
                echo "Working with file $f"
                echo "Generating ${f%.md}.html"
                
                if [ "$f" == "source/index.md" ]; then
                	echo "Generating new bib-include.html file"
                	$GIT_ROOT/bin/bib-html.sh $INCLUDE_PATH/bibliography.bib > $INCLUDE_PATH/bib-include.html
                fi
                
                cat $f | gpp -x -H -I$INCLUDE_PATH | \
                cat - $METADATA | \
                pandoc -s -f markdown+link_attributes+smart+multiline_tables+escaped_line_breaks --toc -t html --template=$INCLUDE_PATH/bootstrap-template.html --mathjax -o ${f%.md}.html
        done <<< "$files"
else
		echo "No changed md files in source directory"
fi