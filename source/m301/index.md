---
title: Math 301
footer: Last modified <#exec date>
<#include nav-m301.yml>
---

<img style="float: right;" src="../assets/img/m301.png" width=400px class="img-responsive">
**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 538-2299  
**e-mail**: tchumley  
**Office Hours**: <#include officehours-f20m1.txt>  

Textbook: *Elementary Analysis: The Theory of Calculus* by Kenneth A. Ross, 
ISBN: 9781461462705; \\
available as a free [e-text][] when logged into the VPN.

---------

## Announcements ##

Announcements will be posted on the [Course Announcements Moodle forum][] throughout the semester.
Recordings of class lectures will also be posted on Moodle, 
but essentially all other materials will be posted on this page.


## Syllabus ##

Check the [syllabus][] for all the important class policies (grades, attendance, etc.). 


## Homework ##

There will be weekly homework assignments throughout the semester to be turned in.
Please read these [guidelines][] for writing mathematics.

- **Group work**. Some of your homework will be done in teams.
	*	These problems will be due on **Fridays**.
	*	You should type solutions in LaTeX and submit only one copy on behalf of the whole team.
- **Solo work**. A selection of problems will be assigned to be written up individually and turned in each week.
	*   These problems will also be due **Fridays**.
	*	You may work with others but the writing should be done on your own.
	*	Feel free to write solutions by hand or type them in LaTeX.
- **Gradescope**. Homework will be turned in through [Gradescope][].
	*	Please add yourself to the Gradescope course using Entry Code **86GRN4**.
	*	Gradescope has made a [short tutorial][] on submitting homework.
- **Collaboration**. I want you to work together on the written homework, of course on the
	group assignments but even on the preliminary and solo assignments. 
	The process of explaining your ideas to one another really helps in learning math. 
	However, you must write up the solo assignments on your own and avoid copying others' work directly. 
	Please acknowledge sources of help, including classmates or outside textbooks,
	by writing  "help from ..." at the top of your assignment.
	Also, please only write what you understand so that I know where to help, and
	please avoid using online forums like Math StackExchange, solutions manuals, or similar resources.
	A huge part of learning in this class comes from figuring out how to get unstuck
	by spending time thinking on your own or talking to me and classmates; these other
	resources can end up being counter-productive in the long term.
- **Rewrites**. Homework is for practice, and you are not expected to write perfect answers from the start! 
	*	Your revisions will be due on **Fridays**. Please resubmit (only the problems you're revising) on Gradescope.
	*	For some problems it's difficult to allow redo's fairly, 
		so on occasion I might not allow a problem to be redone, but this is rare.
	
			

Assignment							Due			
--------------------				-----		
[Homework 0](hw0.pdf)				Oct 27
[Homework 1](hw1.pdf)				Oct 30
[Homework 2](hw2.pdf)				Nov 6
[Homework 3](hw3.pdf)				Nov 13
[Homework 4](hw4.pdf)				Nov 20
[Homework 5](hw5.pdf)				Dec 4
[Homework 6](hw6.pdf)				Dec 11


## Quizzes ##

There will be (mostly) weekly quizzes that will be given on Tuesdays.
They will be posted on Gradescope and you'll have until Wednesday before class to complete them.
The purpose of these is to check in to see that you're comfortable with fundamental material
like definitions, theorems, and important examples and proofs.
Problems will always be related to the previous week's homework and class topics.

Quiz         	 			Date posted     	Material
------------ 				-------- 			------------
Quiz 1		      			Oct 27				Week 1
Quiz 2		      			Nov 3				Week 2
Quiz 3		      			Nov 10				Week 3
Quiz 4		      			Dec 1				Week 5
Quiz 5		      			Dec 8				Week 7

## Exams ##

There will be a midterm and a final.  The date for the mid-term is subject to change slightly.


Exam         	 			Date     	Format			        Material                 
------------ 				-------- 	-------------------- 	-----------------------  
Midterm		      			Nov 17		take-home		        Chapters 1-2			
Final						Dec 14  	take-home				Chapters 3-6			





## Course plan ##

Our plan is to cover parts of the textbook chapters 1 – 4. 
If there's time, we'll talk about parts of chapter 5 and 6 too.
Below is a rough outline of what is to be covered week by week through the semester. 
Please check back regularly for precise details on what is covered, 
as well as postings of class materials like lecture notes.


<#include open-accordion.txt>

<#define WEEK|1>
<#define DAYS|October 21 – October 23>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Introduction

-----------

##### Wednesday

* **Topic**: Sections 1, 2: The natural, rational, and real numbers. A hint at why we need real numbers and some review of induction.
* **Class materials**: [Lecture notes](day1.pdf), [Induction and inequalities](induction-review.pdf).
* **After class**: Read Theorems 3.2 and 3.4; read Definitions 4.1, 4.2, 4.3; Try Exercises 1.9, 3.6.


##### Thursday

* **Topic**: Section 4: The completeness axiom. A look at supremum, infimum.
* **Class materials**: [Lecture notes](day2.pdf), [Basic inequalities](basic-inequalities.pdf).
* **After class**: Read about the Completeness Axiom and the Archimedean Property in Section 4. Work on the [Suprema and infima](sup-inf.pdf) worksheet.
	Ask a question about the reading or worksheet on Piazza (or respond to someone's question).


##### Friday

* **Topic**: Section 4: The completeness axiom, continued. A look at the key defining property of the reals.
* **Class materials**: [Lecture notes](day3.pdf), [Suprema and infima](sup-inf.pdf).
* **After class**: Read the proof that the rationals are dense in the reals (in today's lecture notes). Read Examples 1 and 2 and Definition 7.1 in Section 7, and do Exercise 7.3.
	Ask a question about the reading or exercise on Piazza (or respond to someone's question).


><#include open-accordion-panel.txt>

<#define WEEK|2>
<#define DAYS|October 26 – October 30>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Sequences

-----------

##### Monday

* **Topic**: Sections 7, 8: Introduction to sequences and limits. A look at the formal definition of convergence and how we prove sequences converge.
* **Class materials**: [Lecture notes](day4.pdf)
* **After class**: Read Examples 3 and 4 in Section 8. Try Problem 1 in the [Convergence of sequences](seq-convergence.pdf) worksheet.
	Ask a question about the reading or exercise on Piazza (or respond to someone's question).

##### Tuesday

* **Topic**: Section 8: Discussion about proofs. We continue our look at introductory proof examples involving limits of sequences.
* **Class materials**: [Convergence of sequences](seq-convergence.pdf), [Lecture notes](day5.pdf)
* **After class**: Read the proof at the end of today's lectures. Read Examples 1, 2, 3, and 5; Definition 9.8; and the proofs of Theorem 9.2 and 9.3 in Section 9.


##### Wednesday

* **Topic**: Section 9: Limit theorems for sequences. A look at how to prove the theorems we used in calculus for computing limits.
* **Class materials**: [Lecture notes](day6.pdf)
* **After class**: Read Example 2 in Section 10 about proving a certain recursive sequence converges using the Monotone Convergence Theorem.
Try using this technique with the example at the end of today's lecture notes in the After Class section.

##### Thursday

* **Topic**: Section 10: The monotone convergence theorem. We show that sequences that are bounded and either increasing or decreasing must always converge.
* **Class materials**: [Monotone convergence theorem](mct.pdf), [Lecture notes](day7.pdf)
* **After class**: Read Definition 11.1, Theorem 11.4, and Examples 1 and 2 in Section 11.

##### Friday

* **Topic**: Section 11: Subsequences. We discuss the deeply important Bolzano-Weierstrass theorem, which says that every bounded sequence has a convergent subsequence.
* **Class materials**: [Lecture notes](day8.pdf)
* **After class**: Read Definition 10.8 and Lemma 10.9. Say hello to your homework group mates over the weekend.



><#include open-accordion-panel.txt>

<#define WEEK|3>
<#define DAYS|November 2 – November 6>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Sequences

-----------

##### Monday

* **Topic**: Section 10: Cauchy sequences. We introduce another useful class of sequences that always converge.
* **Class materials**: [Lecture notes](day9.pdf)
* **After class**: Read Section 14 up through Corollary 14.7. Try exercises 14.1 e,f and 14.3 c,e. Post a question or response on Piazza
	about the reading or exercises.


##### Tuesday

* **Topic**: Section 14: Series. We discuss infinite series, looking at and proving some familiar tests of convergence from calculus.
* **Class materials**: [Infinite series](infinite-series.pdf), [Lecture notes](day10.pdf)
* **After class**: Work on homework in preparation for homework group time tomorrow.


##### Wednesday

* **Topic**: Homework group time. We'll devote our class time to working in groups on [Homework 2](hw2.pdf).
* **Class materials**: [Lecture notes](day11.pdf)
* **After class**: Read Corollary 14.7 and the Ratio and Root Tests (replace liminf and limsup with lim and skip the proofs of these). Read Theorem 15.3.
	Try Exercises 14.1b,c and 15.6, and post questions and responses on Piazza.

##### Thursday

* **Topic**: Section 14: Series, continued. We continue our discussion of infinite series.
* **Class materials**: [More on infinite series](infinite-series-ii.pdf), [Lecture notes](day12.pdf)
* **After class**: Read pages 60-62 and 73-76 on limsup, liminf, and subsequential limits.

##### Friday

* **Topic**: Section 10: Limsup, liminf. We investigate new, more general notions of limits.
* **Class materials**: [Lecture notes](day13.pdf)
* **After class**: Read Theorems 11.2, 11.7, and 11.8.


><#include open-accordion-panel.txt>

<#define WEEK|4>
<#define DAYS|November 9 – November 13>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Continuity

-----------

##### Monday

* **Topic**: Section 11: Limsup, liminf, continued. We connect the notions of liminf and limsup with subsequences and accumulation points.
* **Class materials**: [Lecture notes](day14.pdf)
* **After class**: Read Definitions 20.1, 20.3, and 17.1, as well as Theorem 17.2 and Examples 1 and 2 in Section 17.

##### Tuesday

* **Topic**: Sections 17, 20: Limits of functions. We begin our discussion of limits of functions and continuity and get introduced to epsilon-delta proofs.
* **Class materials**: [Limits and continuity](continuity.pdf), [Lecture notes](day15.pdf)
* **After class**: Work on homework in preparation for homework group time tomorrow.

##### Wednesday

* **Topic**: Homework group time. We'll devote our class time to working in groups on [Homework 3](hw3.pdf).
* **Class materials**: No notes today.
* **After class**: Work on Problems 2 and 3 in the [Limits and continuity](continuity.pdf) worksheet.


##### Thursday

* **Topic**: Sections 17, 20: Continuous functions, continued. We continue our work with epsilon-delta proofs and discuss the definition of continuity.
* **Class materials**: [More continuity](advanced-continuity.pdf), [Lecture notes](day17.pdf)
* **After class**: Read Theorems 18.1 and 18.2 and review the Bolzano-Weierstrass Theorem.

##### Friday

* **Topic**: Section 18: Properties of continuous functions. We discuss the extreme value theorem.
* **Class materials**: [Lecture notes](day18.pdf)
* **After class**: Read Theorem 18.2 and its proof.



><#include open-accordion-panel.txt>

<#define WEEK|5>
<#define DAYS|November 16 – November 20>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Continuity

-----------

##### Monday

* **Topic**: Exam work day. I'll be available during class time for office hours, but there is no official class.

##### Tuesday

* **Topic**: Section 18: Properties of continuous functions, continued. We discuss the intermediate value theorem and its consequences.
* **Class materials**: [Intermediate value theorem](ivt.pdf), [Lecture notes](day19.pdf)
* **After class**: Read Section 19 through Theorem 19.2 and its proof. 

##### Wednesday

* **Topic**: Section 19: Uniform continuity. We review delta-epsilon proofs of continuity and introduce a definition stronger than continuity.
* **Class materials**: [Lecture notes](day20.pdf)
* **After class**: Read Theorem 19.2 and Discussion 19.3.

##### Thursday

* **Topic**: Section 19: Uniform continuity, continued. We discuss how to prove functions fail to be uniformly continuous.
* **Class materials**: [Uniform continuity](uniform-continuity.pdf), [Lecture notes](day21.pdf)
* **After class**: Read Definition 28.1 and try Exercises 28.1ac, 28.3.

##### Friday

* **Topic**: Section 28: Basic properties of the derivative. We introduce the definition of the derivative and look at some basic examples and facts.
* **Class materials**: [Lecture notes](day22.pdf)
* **After class**: Try Exercise 28.7, and try proving that $f(x) = |x|^3$ is differentiable at $x = 0$. Use epsilon-delta proofs.


><#include open-accordion-panel.txt>

<#define WEEK|6>
<#define DAYS|November 23 – November 27>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Differentiation

-----------

##### Monday

* **Topic**: Section 28: Basic properties of the derivative, continued. We consider the differentiability of some more sophisticated examples of functions.
* **Class materials**: [Lecture notes](day23.pdf)
* **After class**: Read the beginning of Section 29 and try to summarize how the first three theorems fit together.


##### Tuesday

* **Topic**: Section 29: The mean value theorem. We build up a proof for one of the most important results in the theory of calculus.
* **Class materials**: [Three core theorems](mvt.pdf),  [Lecture notes](day24.pdf)
* **After class**: Enjoy the break!

##### Wednesday - Friday

* **Topic**: Thanksgiving break, no class.


><#include open-accordion-panel.txt>

<#define WEEK|7>
<#define DAYS|November 30 – December 4>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Sequences of functions

-----------

##### Monday

* **Topic**: Section 34: The fundamental theorem of calculus. We review uniform continuity and the mean value theorem and prove the fundamental theorem of calculus.
* **Class materials**: [Lecture notes](day25.pdf)
* **After class**: Read Discussion 31.1, Definition 31.2, and Theorem 31.3 and ask a related question on Piazza or post a response.
	Think about group homework in preparation for group homework time on Tuesday and Wednesday.


##### Tuesday

* **Topic**: Section 31: Taylor's thoerem. We discuss the issue of when an infinitely differentiable function equals its power series. Also, we devote some time to group homework.
* **Class materials**: [Lecture notes](day26.pdf)
* **After class**: Continue working on homework in preparation for group homework time on Wednesday.

##### Wednesday

* **Topic**: Homework group time. We'll devote our class time to working in groups on [Homework 5](hw5.pdf).
* **Class materials**: No notes today.
* **After class**: Read my responses to the following Piazza questions: [first question](https://piazza.com/class/kbkxtlvqplc5l7?cid=53),
	[second question](https://piazza.com/class/kbkxtlvqplc5l7?cid=54). Continue reading Section 31 and ask follow up questions.


##### Thursday

* **Topic**: Section 31: Taylor's theorem, continued. We continue our discussion of Taylor's theorem and real analytic functions.
* **Class materials**: [Lecture notes](day27.pdf)
* **After class**: Read Definitions 24.1, 24.2, and Example 2 in Section 24.
	Work on the [Pointwise convergence of functions](pointwise-convergence.pdf) worksheet.

##### Friday

* **Topic**: Section 24: Uniform convergence. We talk about sequences of functions and introduce a simple, but in a sense flawed, notion of convergence for these sequences: pointwise convergence.
* **Class materials**: [Pointwise convergence of functions](pointwise-convergence.pdf), [Lecture notes](day28.pdf)
* **After class**: Read Theorem 24.3, Remark 24.4, and Example 7 in Section 24. Ask a question on Piazza.



><#include open-accordion-panel.txt>

<#define WEEK|8>
<#define DAYS|December 7 – December 11>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Sequences of functions

-----------

##### Monday

* **Topic**: Sections 24: Uniform convergence, continued. We work with a computational technique for determining uniform convergence on a domain.
* **Class materials**: [Uniform convergence of functions](uniform-convergence.pdf), [Lecture notes](day29.pdf)
* **After class**: Read Discussion 25.1, Theorem 25.2, and try the [Uniform convergence and integration](uniform-conv-integration.pdf) worksheet.
	Think about group homework in preparation for group homework time on Tuesday and Wednesday.

##### Tuesday

* **Topic**: Sections 25: More on uniform convergence. We discuss some consequences of uniform convergence related to continuity and integration.
* **Class materials**: [Uniform convergence and integration](uniform-conv-integration.pdf), [Lecture notes](day30.pdf)
* **After class**: Continue working on homework in preparation for group homework time on Wednesday.

##### Wednesday

* **Topic**: Homework group time. We'll devote our class time to working in groups on [Homework 6](hw6.pdf).
* **Class materials**: No notes today.
* **After class**: Read Definitions 25.3 and Theorems 25.4-24.7.

##### Thursday

* **Topic**: Sections 25: More on uniform convergence, continued. We discuss Cauchy sequences of functions and series of functions.
* **Class materials**: [Lecture notes](day31.pdf)
* **After class**: Finish up homework.

##### Friday

* **Topic**: Wrap up, look ahead. We discuss metric spaces, the space of continuous functions on a closed interval, and analogies with ideas from earlier in the course.
* **Class materials**: [Lecture notes](day32.pdf) 
* **After class**: Work on exam, redo's.

><#include open-accordion-panel.txt><#include div-close.txt>



## Getting help ##

Here are a few ways to get help:

-   **Office hours**: <#include officehours-f20m1.txt>  
-	**Class TA**: Our class TA will be available for one-on-one appointments each week.
	Her contact information will be posted on Moodle.
-	**Study groups**: Other students in the class are a wonderful resource.
	I want our class to feel like a community of people working together.
-	**Piazza**: I've set up an [online forum][] for asking and answering questions about
	class material, including homework problems. My plan is to check the forum every day
	and answer questions as they come up, but I hope everyone in the class will pitch in 
	and answer others' questions when possible.
	
## Resources ##

-	When away from campus, a [college VPN][] is available to access resources normally only
	available when on the campus network (eg. free e-books through LITS and Springer Link).
	However, I understand there might be some accessibility issues even with the VPN, 
	so I will do my best to help everyone access 
	necessary materials through other methods as well.
-	I've collected some resources to help you when writing homework solutions with [LaTeX][].
	* Here is a [LaTeX template][] file for writing homework solutions and its [pdf output][].
	* A [quick reference][] is available for commonly used symbols.
	* [Overleaf][] is a cloud service that lets you edit and compile LaTeX through a web
		browser so that no local installation is needed. The free version will be sufficient
		for our class.
	* To install LaTeX locally on your personal computer, I recommend
	installing [MacTex][] if you use a Mac, [MikTeX][] for Windows, or [TeXLive][] for Linux.
-	There are many real analysis books out there. Reading our required textbook will be enough
	to get a strong understanding of the material in class, but if you're curious, here
	are some other good books. The first two are available for free as e-books when
	signed into the campus network or VPN.
	*	[*Understanding Analysis*][] by Stephen Abbott. Similar to our textbook, but with
		some interesting chapter introductions and projects.
	*	[*Real Mathematical Analysis*][] by Charles C. Pugh. Nicely written, but with more
		emphasis on topology and metric spaces than our textbook.
	*	*Principles of Mathematical Analysis* by Walter Rudin. A classic textbook that
		is a bit too terse for a first analysis course but worth reading if you're
		really excited about analysis. It's filled with great, challenging exercises.
-	Here is an archive of the [Fall 2020, Module 1, class web page](./f20m1/).
		



	
[e-text]: http://proxy.mtholyoke.edu:2048/login?url=http://dx.doi.org/10.1007/978-1-4614-6271-2
[syllabus]: ./syllabus
[guidelines]: https://math.hmc.edu/su/wp-content/uploads/sites/10/2019/11/good-math-writing.pdf
[Gradescope]: https://www.gradescope.com
[short tutorial]: https://gradescope-static-assets.s3.amazonaws.com/help/submitting_hw_guide.pdf
[online forum]: https://piazza.com/mtholyoke/fall2020/math301/home
[Course Announcements Moodle forum]: https://moodle.mtholyoke.edu/mod/forum/view.php?id=548975

[college VPN]: https://lits.mtholyoke.edu/tech-support/access-and-internet-connectivity/connect-campus/using-virtual-private-network-vpn

[LaTeX]: https://en.wikipedia.org/wiki/LaTeX
[Overleaf]: https://www.overleaf.com
[MacTeX]: http://tug.org/mactex/
[MikTeX]: https://miktex.org/download
[TeXLive]: https://www.tug.org/texlive/
[quick reference]: https://www.caam.rice.edu/~heinken/latex/symbols.pdf
[LaTeX template]: ../assets/tex/homework-template.tex
[pdf output]: ../assets/pdf/homework-template.pdf

[*Understanding Analysis*]: https://link.springer.com/book/10.1007%2F978-1-4939-2712-8
[*Real Mathematical Analysis*]: https://link.springer.com/book/10.1007%2F978-3-319-17771-7


