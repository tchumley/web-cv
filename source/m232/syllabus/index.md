---
title: Math 232 Syllabus
<#include nav-m232.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley      

---------

**Course**: Math 232, Discrete Mathematics   

**Prerequisites**: Math 102 (Calculus II) or CS 101

**Textbook**: *Book of Proof, 2nd edition*, by Richard Hammack, ISBN: 978-0989472104

**Learning goals**: During the semester our goal is to:

-	Strengthen or develop your ability to construct a logical argument
-	Write proofs using direct methods, contradiction, induction
-	Get exposure and experience in the following topics:
	*	foundations of logic
	*	set theory: equality, intersection, union, complement
	*	functions and equivalence relations 
	*	number theory 
	*	counting
	*	probability 
	*	graph theory

**Office hours**: <#include officehours-f18.txt>      
	
These are times I hold on my schedule to be available for students.  I like to use these times as open drop in sessions where there may be multiple students getting help simultaneously, but it's expected that you come with specific questions about notes, homework problems, or the text that you have thought about already.  If you need to schedule a time to meet with me one-on-one, either to discuss something private or because you can't make my office hours, please send me an email with times that you are available to meet, and I will find a time that works for both of us.

**Homework**: There will be weekly homework assignments comprising both computational and theoretical, writing oriented problems.  Everyone will revise and submit a collection of homework problems from the semester in a final portfolio.  We'll discuss the portfolio more in class later in the semester.

**Self-check quizzes**: There will be some short (5-10 minute) quizzes with straightforward questions (for example, stating a definition).  They are written to make sure you're comfortable with the basics and in turn make homework easier.  These will be counted toward the participation part of your grade.

**Quizzes**: There will be a few longer (15-20 minute) quizzes during the semester to keep everyone up to date and studying the material between exams.  These will involve questions similar to homework problems.  The dates for these will be announced in advance.

**Exams**: There will be two midterm exams and a self-scheduled final.

**Attendance and Participation**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class please prepare by doing any assigned reading and suggested problems. Please expect to talk about math in small groups as well as in class discussions. Other classroom activities may involve worksheets, computer explorations, and informal presentations at the board.

**Late work, makeups**:  In general unexcused late work is not accepted for full credit and exams must be taken on time.  In special circumstances (eg. bad illness, unexpected family issues) I will accept late work or allow for a make-up exam up to a few days late.  I won't be able to accept late work after solutions are posted.  Please get in touch as soon as possible if something comes up.

**Grading**: Grades will be assigned based on homework and exams according to the following weighting:

- Homework (including portfolio): 15%
- Quizzes: 15%
- Participation (including reading questions, mini-presentations, self-check quizzes): 5%
- Exam I: 20%
- Exam II: 20%
- Final: 25%

Overall letter grades will be based on a scale no stricter than the usual:

- 93-100:			A
- 90-93:			A-
- 88-90				B+
- 83-88:			B
- 80-83:			B-
- 78-80:			C+
- 73-78:			C
- 70-73:			C-
- 68-70:			D+
- 63-67:			D
- 60-63:			D-
- 0-60:				F

**Academic integrity**: It is very important for you to follow the Honor Code in all of your work for this course. Collaboration on homework assignments is encouraged. However, it is important that you only write what you understand, and that it is in your own words. If you have any questions about what constitutes an Honor Code violation in this class please ask your instructor. Honor Code violations will be brought to the dean. The penalty for violating the Honor Code on any assignment, quiz or exam will be a score of 0 for it. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please contact AccessAbility Services, located in Wilder Hall B4, at (413) 538-2634 or accessability-services@mtholyoke.edu. If you are eligible, they will give you an accommodation letter which you should bring to me as soon as possible.
