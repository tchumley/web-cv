---
title: Math 102
footer: Last modified <#exec date>
<#include nav-m102.yml>
---

- 	I grew up in a northwest suburb of Chicago.  Here's a practically-impossible-if-not-for-Google piece of trivia about my home town: Sandra Bullock's character in the film Gravity is from this town.

- 	It's too hard to pick my favorite cookie flavor.  I guess my favorite kind of cookie would have chocolate in it.

- 	I like all kinds of music!  I dont't spend much time actively looking for music, but two things I've listened to recently are Nina Nastasia and The Microphones.

- 	Before teaching at Mount Holyoke I was a postdoc at Iowa State University in the probability group.  I worked on (and continue to work on) research problems involving stochastic processes, which are models that incorporate some element of randomness.  I've worked a lot on problems involving billiards (sort of like the game of pool, but a mathematical version).  Aside from research, I also taught classes like calculus, linear algebra, and real analysis.  Before Iowa State, I was a graduate student at Washington University in St. Louis, an amazing place!

- 	I'm a math professor for a few reasons.  One is that I really enjoy working on math.  My favorite moments in working on math come when you solve problems using a totally unexpected, seemingly unrelated idea from an area of math you thought had nothing to do with the problem you were working on.  This is probably why I love probability.  You use tools from calculus, differential equations, and other areas to solve problems.  It's also why I like working on problems that overlap between geometry and probability.

	The other big reason I'm a math professor is that I really enjoy working with students.  I guess it's not a strict requirement for being a math professor, but enjoying the process of teaching makes my job a lot easier.  By the way, you should know that MHC is a special place in the way students buy into the process of learning.  You, the students, are all my priority, so the effort that you put in makes my job all the more fulfilling.  Also, I enjoy working with students outside of classroom teaching.  Working on research projects is exciting and energizing, all the more so with students.

- 	I realized I loved math enough to make it my main focus some time near the end of college.  I was initially a computer science major, but I enjoyed math classes enough that I kept taking them until I realized I liked math a little more.  Math was challenging (not that computer science wasn't) in a way that was fulfilling.  My professors pushed us, but I felt like they also had our best interest in mind.  The community around math in grad school really solidified my decision to pursue the subject.  I had a very special group of friends in the math department; it felt like a community and everyone was very supportive.  Professors in the department treated us like future colleagues.

	I realized I wanted to teach math some time in grad school.  I realized that I felt a lot of excitement in telling students about ideas I found intriguing and interesting. I try to find moments when I'm teaching now that will inspire that same sense of excitement.  I want students in my class to hopefully find something exciting enough that they want to show it off to interested friends.  Maybe I realized I wanted to teach even before grad school.  I spent a lot of time in grade school trying to explain everything I learned to my little sister.  She's four years younger than me and maybe had no idea what I was talking about most of the time but maybe that's besides the point.  In any case, the best moments I've had in teaching and learning come when finding something simple to describe but that has an unexpected result or relationship with another area of math.

- 	Thank you to the person who said they like my flannel shirts.

- 	Someone asked if I'm Team Pi or Team Tau.  The question was in reference to two related mathematical constants.  Pi is the ratio of a circle's circumference to its diameter, while tau is the ratio of its circumference to its radius.  The two are related by the equation tau equals 2 times pi.  There are some who argue that tau is more natural to use than pi.  Here are two videos here that go into the matter more:

	[Vi Hart - Pi is still wrong](https://www.khanacademy.org/math/recreational-math/vi-hart/pi-tau/v/pi-is--still--wrong)
	
	[Numberphile](https://youtu.be/ZPv1UV0rD8U)

	Admittedly, I wasn't aware of tau until I received the anonymous note.  It might be too late for me to switch teams.

- The first link above is done by someone named Vi Hart.  Someone asked if I was familiar with her.  I wasn't, but I've now read her Wikipedia article

	[Vi Hart on Wikipedia](https://en.wikipedia.org/wiki/Vi_Hart)

	and am going to look for more of her work!
	
