---
title: Math 301 Syllabus
<#include nav-m301.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley   

---------

**Course**: Math 301, Real analysis

**Prerequisites**: Math 211 (Linear Algebra) and Math 232 (Discrete Math), or equivalent experience studying abstract math and writing proofs.

**Textbook**: *Elementary Analysis: The Theory of Calculus* by Kenneth A. Ross

**About the course**: Our course is an introduction to analysis through the lens of trying to understand the underpinnings of calculus. This is a nice way to frame the subject the first time you it, but in a way, the course and subject is much more about the proof techniques that we'll learn and the nitty gritty details of the synonymous notions of limits, approximation, and convergence. It's through this wider lens that it becomes clear why analysis is at the core of many mathematical subjects (eg. probability, mathematical statistics, differential equations, scientific computing, differential geometry), as well as outside disciplines like engineering and economics.

**About the course during the pandemic**: Here are some things about the course
that are bit more specific to the Fall 2020 semester.

-	We'll have synchronous meetings on Zoom every day in time slot 2.
	I expect everyone to attend each meeting, but I'll be understanding of
	occasional absences and will post recordings of class on Moodle.
	I'll do my best to insure your privacy with these recordings,
	and I'll ask you not to share them outside our class.
-	There will be a little pre- and post-class work, involving some combination
	of reading, short videos, and problems. My goal will be to give you some
	structure to help you through the compressed schedule, 
	but not overwhelm you. Nevertheless, you should expect to spend about 20-24
	hours per week on the class.
-	You're all my top priority this fall!
	Let's do our best to support each other.

**Learning goals**: During the semester our goal will be to understand the theory of the real numbers and functions at a deep, rigorous level. Central to this will be developing our ability to communicate, particularly through writing proofs. In terms of topics specific to our course, the aim is to cover:

- field axioms, total orderings, suprema and infima of sets, completeness of the real numbers
- limits, sequences, and series of real numbers, Cauchy sequences, the Bolzano-Weierstrass theorem
- continuity, compactness, connectedness, the extreme value theorem, the intermediate value theorem
- uniform convergence, interchanging the order of limits, and power series
- (time permitting) the mean value theorem, Taylor's theorem, the fundamental theorem of calculus.


**Attendance**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class please prepare by doing any assigned reading and suggested problems. Please expect to talk about math in small groups as well as in class discussions. Activities may involve worksheets and informal presentations that will be designed to contribute significantly to our learning.

**Participation**: Part of your grade will depend on participation and there will be a number of opportunities to participate (eg. asking and answering questions, coming to office hours, responding to feedback surveys, and doing informal presentations).

**Homework**: There will be weekly homework assignments.

**Quiz**: There be (mostly) weekly quizzes.

**Exams**: There will be a midterm exam and a final exam.

**Late work, makeups**:  In general, unexcused late work will not be accepted for full credit (typically at most 50% credit if it's more than a week late) and exams must be taken on time.  In special circumstances (eg. illness) I will accept late work for full credit or allow for a make-up exam.  Please get in touch as soon as possible if something comes up.

**Getting help**: Here are some of the resources that will be available:

-	**Office hours**: These times (which will be posted near the top of the  class web page) are open drop in sessions where there may be multiple students getting help.  It's expected that you come with specific questions about notes, homework problems, or the text that you have thought about already.  If you need to schedule a time to meet with me one-on-one, either to discuss something private or because you can't make my office hours, please send me an email with times that you are available to meet, and I will find a time that works for both of us.
-	**Study groups**: Other students in the class are a wonderful resource.  I want our class to feel like a community of people working together.  Even though our class won't have a TA or evening help, I will work to find ways for you to feel supported both by me and each other.

**Grades**: Grades will be assigned according to the following weighting:

- Participation: 10%
- Quizzes: 10%
- Homework: 30%
- Midterm: 25%
- Final: 25%


Overall letter grades will be based on a scale no stricter than the usual:

- 93-100:			A
- 90-93:			A-
- 88-90				B+
- 83-88:			B
- 80-83:			B-
- 78-80:			C+
- 73-78:			C
- 70-73:			C-
- 68-70:			D+
- 63-67:			D
- 60-63:			D-
- 0-60:				F


**Academic integrity**: It is very important for you to follow the Honor Code in all of your work for this course. Collaboration on homework assignments is encouraged. However, it is important that you only write what you understand, and that it is in your own words. My first instinct is always to trust you, but I cannot give credit for plagiarized work and might have to refer such issues to the deans. If you have any questions about whether something is an Honor Code violation, please ask me. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please get in touch with me. We'll work together with AccessAbility Services to make sure the class is accessible and equitable.


