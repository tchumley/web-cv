---
title: Math 339 Syllabus
<#include nav-m339.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley   

---------

**Course**: Math 339, Stochastic Processes 

**Prerequisites**: Math 211 (Linear Algebra) and Math 342 (Probability), or equivalent

**Textbook**: *Introduction to Stochastic Processes with R* by Robert P. Dobrow, ISBN: 9781118740651

**Learning goals**: During the semester you will be learning how to

- understand the basics of stochastic modeling of real-world systems related to the phyiscal sciences, computer science, and (possibly) finance
- analyze long-term behavior of Markov chains using tools from linear algebra
- apply general results on Markov chains to understand Markov chain Monte Carlo and its use in analyzing real world problems
- use the exponential distribution to model arrival times, the Poisson process, and understand its application to continuous time Markov chains
- use simulation to gain intuition for and analyze complicated stochastic processes

**Homework**: There will be weekly homework assignments comprising both computational and theoretical, writing oriented problems.

**Quizzes**: There will be weekly 10-15 minutes quizzes.  The problems on quizzes will be similar to homework assignments and are meant to keep everyone up to date and studying between exams.

**Exams**: There will be two midterm exams and a final.

**Attendance**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class please prepare by doing any assigned reading and suggested problems. Please expect to talk about math in small groups as well as in class discussions. Other classroom activities may involve worksheets, computer explorations, and informal presentations at the board.

**Late work, makeups**:  In general unexcused late work is not accepted for full credit (typically at most 50% credit if it's more than a week late) and quizzes and exams must be taken on time.  In special circumstances (eg. bad illness) I will accept late work for full credit or allow for a make-up exam up to a few days late.  Please get in touch as soon as possible if something comes up.

**Getting help**: Here are some of the resources that will be available:

-	**Office hours**: These times (which will be posted near the top of the  class web page) are open drop in sessions where there may be multiple students getting help.  It's expected that you come with specific questions about notes, homework problems, or the text that you have thought about already.  If you need to schedule a time to meet with me one-on-one, either to discuss something private or because you can't make my office hours, please send me an email with times that you are available to meet, and I will find a time that works for both of us.
-	**Study groups**: Other students in the class are a wonderful resource.  I want our class to feel like a community of people working together.  Even though our class won't have a TA or evening help, I will plan to reserve a room so that classmates have a designated meeting spot each week.

**Grades**: Grades will be assigned based on homework and exams according to the following weighting:

- Participation: 5%
- Homework: 15%
- Quizzes: 15%
- Exam I: 20%
- Exam II: 20%
- Final: 25%


Overall letter grades will be based on a scale no stricter than the usual:

- 93-100:			A
- 90-93:			A-
- 88-90				B+
- 83-88:			B
- 80-83:			B-
- 78-80:			C+
- 73-78:			C
- 70-73:			C-
- 68-70:			D+
- 63-67:			D
- 60-63:			D-
- 0-60:				F


**Academic integrity**: It is very important for you to follow the Honor Code in all of your work for this course. Collaboration on homework assignments is encouraged. However, it is important that you only write what you understand, and that it is in your own words. If you have any questions about what constitutes an Honor Code violation in this class please ask your instructor. Honor Code violations will be brought to the dean. The penalty for violating the Honor Code on any assignment, quiz or exam will be a score of 0 for it. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please contact AccessAbility Services, located in Wilder Hall B4, at (413) 538-2634 or accessability-services@mtholyoke.edu. If you are eligible, they will give you an accommodation letter which you should bring to me as soon as possible.


