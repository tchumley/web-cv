#!/bin/bash

# The second command in the task below comprises a series of pipes,
# which do the following respectively:
# extract bibtex entries from BIBFILE according to rules in B2BFLAGS,
# convert bibtex entries to html,
# convert html to markdown,
# strip text in markdown after ---- string,
# strip blank lines,
# reverse the order of the lines,
# number the lines,
# convert to html,
# add reversed attribute ordered list tag 

B2BFLAGS="--no-comment"
bib2bib $B2BFLAGS -c 'author : "Chumley"' $1 |\
	 bibtex2html -nodoc -nokeys --no-keywords --style abbrv -q -d |\
	 pandoc -f html -t markdown | \
	 sed -n '/------------------/q;p' | \
	 awk ' /^$/ { print; } /./ { printf("%s ", $0); }' | \
	 tail -r | \
	 nl -bt -s '. ' -w 1 | \
	 pandoc -f markdown -t html | \
	 sed 's/<ol/& reversed/'
