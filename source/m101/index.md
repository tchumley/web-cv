---
title: Math 101
footer: Last modified <#exec date>
<#include nav-m101.yml>
---

<img style="float: right;" src="../assets/img/m101.png" width=300px class="img-responsive">


**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 538-2299  
**e-mail**: tchumley  
**Office Hours**: <#include officehours-s19.txt>  

Textbook: *Calculus: Single Variable, 7th Edition* by Deborah Hughes-Hallett et al, ISBN: 9781119139317 \\
(there are multiple ISBN numbers for the book but any print or e-text of the 7th edition is fine); \\
on library reserve under QA303.2 .H845 2017

---------

## Announcements ##

Announcements will be posted here throughout the semester.

  **Apr 29**: My office hours for reading week are as follows:

-	Monday: 4:00-5:00
-	Tuesday: 11:00-12:00
-	Wednesday: 9:00-10:00
-	Thursday: 11:00-12:00
-	Friday: 1:00-2:00


<details>
  <summary>Past announcements</summary>
  
  **Apr 24**: Alissa will hold evening help on Sunday, April 28, and her usual tutoring hours that afternoon.  There will be no evening help on Tuesday, April 30.

  
  **Feb 20**: The functions skills quiz went well overall, but if you scored below 21/30, I'm requiring that
you schedule a time to take a redo.  The kinds of of calculations on the quiz are important for moving forward
in the class, and I want everyone to have a chance to be successful, as well as the chance to improve your grade
up to a 21/30.
  
  **Feb 8**: One-on-one help with Amelia will be on Mondays, 6:00 – 7:00 pm, in Clapp 420.
Use [Amelia's sign-up page][] to reserve a slot.  Her evening help is still on Tuesdays with no sign up required.

**Feb 4**: Some instructions for submitting **homework rewrites** have been added to the Homework section below.
Also, I've adjusted the Skills Quiz 1 date to Friday, February 15 so that you can get some feedback on Homework 2 first.
  
  **Jan 25**: Evening help information is posted below in the Getting Help section.  Note that it starts this Sunday!
  
  **Dec 17**: Please check back in late January for updates on the course. 

</details>


## Syllabus ##

Check the [syllabus][] for all the important class policies (grades, attendance, etc.).


## Homework ##

There will be weekly homework assignments throughout the semester to be turned in, as well
as some suggested problems to be considered as additional practice.

- **Written assignments**. A selection of problems taken from the textbook or other sources
	will be assigned to be due **Wednesdays in class**. 
	*	These assignments are given so that you can practice writing mathematics and 
		receive feedback on your progress.
	* 	If you would like feedback on a particular steps in a problem, then you can 
		indicate this on your assignment.  
	*	Please read these [guidelines][] for writing up your homework solutions.
- **Collaboration**. I want you to work together on the written homework; 
	the process of explaining your ideas to one another really helps in learning math. However, you must write up the problems on your own,
	listing the people with whom you worked on the problems. Please acknowledge any source of help, including classmates or outside texts,
	by writing  "help from ..." at the top of your assignment.  Also, please only write what you understand so that I know where to help.
- **Rewrites**. Homework is for practice, you are not expected to write perfect answers right from the start! 
	If you would like to earn back lost credit on a homework assignment,
	you may resubmit it to me within one week of the day you get it back in class.
	If you’d like to do this, you must write your new answers on new paper,
	and attach your original assignment so that I can compare the two and see your improvement.

	
			

Assignment																			Due			
--------------------																-----		
[Homework 0](hw0.pdf)																Jan 25
[Homework 1](hw1.pdf)																Jan 30
[Homework 2](hw2.pdf)																Feb 6
[Homework 3](hw3.pdf)																Feb 13
[Homework 4](hw4.pdf)																Feb 20
[Homework 5](hw5.pdf)																Feb 27
[Homework 6](hw6.pdf)																Mar 6
[Homework 7](hw7.pdf)																Mar 20
[Homework 8](hw8.pdf)																Mar 27
[Homework 9](hw9.pdf)																Apr 3
[Homework 10](hw10.pdf)																Apr 10
[Homework 11](hw11.pdf)																Apr 17
[Homework 12](hw12.pdf)																Apr 29
  

## Quizzes #


#### Weekly quizzes

There will be weekly quizzes during the semester, intended to break up the material 
between exams. You should expect these to be 10-15 minutes long at the start of class
on **Fridays** with material from the previous homework assignment.


Quiz        Date     Material         			Solutions
---------   -------- -------------    			----------
Quiz 1      Feb 1	 1.1, 1.2, 1.4				[(pdf)](quiz1s.pdf)
Quiz 2		Feb 8	 1.3, 1.5, 1.6				[(pdf)](quiz2s.pdf)
Quiz 3		Feb 22	 1.7, 1.8, 1.9, 2.1			[(pdf)](quiz3s.pdf)
Quiz 4		Mar 1	 2.2, 2.3, 2.4, 2.5			[(pdf)](quiz4s.pdf)
Quiz 5		Mar 22	 3.1, 3.2, 3.3				[(pdf)](quiz5s.pdf)
Quiz 6		Apr 6	 3.7						[(pdf)](quiz6s.pdf)
Quiz 7		Apr 12	 4.1						[(pdf)](quiz7s.pdf)


#### Skills quizzes

There will be two skills proficiency quizzes, which are longer than weekly quizzes.  They will focus on algorithmic computations.

Quiz             Date       Material         			
---------        --------   -------------    		
Skills quiz 1    Feb 15		functions (1.1 – 1.6)
Skills quiz 2    Mar 29		derivatives (3.1 – 3.6), [review problems](derivatives-review.pdf)



## Exams ##

There will be three exams.  The dates for the two mid-terms are subject to change slightly:


Exam         	 			Date     	Time and Location       Material                   		  Study material             							
------------ 				-------- 	-------------------- 	-----------------------    		  -------------------------- 
Exam 1       				Mar 8		in class     			1.7, 1.8, Chapter 2		 		  [review guide](review1.pdf) 				  		  		  
Exam 2       				Apr 19	    in class			    3.7, 4.1 – 4.3, 4.6				  [review guide](review2.pdf)
final			 			May 3 - 7	self-scheduled			cumulative						  [review guide](review3.pdf)






## Course plan ##

Our plan is to cover parts of the textbook chapters 1 – 5.  Below is 
a rough outline of what is to be covered week by week through the semester. Please check 
back regularly for updates.

 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Week              			Sections		   								In-class activities																Extra problems
------------------------ 	-------			   								--------------------	     													--------------------------------------------
Jan 21 - Jan 25     		Wednesday: 1.1 \\								[Functions, lines](1-1.pdf) \\													1.1: 1, 7, 9, 17, 23, 43, 51 \\
							Friday: 1.2										[Exponential functions](1-2.pdf)												1.2: 5, 7, 27, 29

Jan 28 - Feb 1	   			Monday: 1.4 \\									[Logarithmic functions](1-4.pdf) \\												1.4: 1, 3, 5, 7, 9, 15, 17, 45, 47 \\
							Wednesday: 1.3 \\								[Shifts, inverses](1-3.pdf)	\\													1.3: 3, 9, 11, 13, 17, 27, 33, 43, 45 \\
							Friday: 1.5										[Trigonometric functions](1-5.pdf)												1.5: 3, 7, 9, 13, 31

Feb 4 - Feb 8   			Monday: 1.6 \\									[Polynomials, rational functions](1-6.pdf) \\									1.6: 3, 5, 9, 19, 21, 23 \\
							Wednesday: 1.7 \\								[Limits, continuity](1-7.pdf) \\  												1.7: 3, 5, 11, 15, 17, 25, 27, 33 \\
							Friday: 1.7, 1.8								[More on limits](1-8.pdf)														1.8: 1, 3, 7, 9, 57, 59
							  
Feb 11 - Feb 15   			Monday: limits practice \\						[Limits, continuity worksheet](limits-continuity.pdf) \\  						1.9: 9, 11, 13, 17, 19 \\
							Wednesday: 2.1, review \\						[Instantaneous velocity](2-1.pdf) \\  											2.1: 1, 3, 5, 7, 21, 23 \\
							Friday: skills quiz								[Function skills quiz practice](functions-skill-quiz-practice.pdf)				2.2: 11, 19, 21, 39, 51, 53, 55
																				[(solutions)](functions-skill-quiz-practice-sol.pdf)

Feb 18 - Feb 22     		Monday: 2.2 \\									[Definition of derivative](2-2.pdf), [worksheet](2-2-worksheet.pdf) \\			2.3: 7, 9, 11, 13, 29, 35, 41 \\
							Wednesday: 2.3 \\								[The derivative function](2-3.pdf) \\											2.4: 3, 7, 21 \\
							Friday: 2.4, 2.5								[The second derivative](2-4-5.pdf) \\											2.5: 3, 9, 11, 13, 25
																																																								
Feb 25 - Mar 1    			Monday: 2.6 \\									[Differentiability](2-6.pdf) \\													2.6: 3, 5, 7, 11, 23, 29 \\
							Wednesday: 3.1 \\								[Power, constant multiple, sum rules](3-1.pdf) \\								3.1: 9, 11, 13, 15, 17, 19, 31, 33, 35, 37 \\
							Friday: 3.2, 3.3								[Exponential functions](3-2.pdf), [Product rule](3-3-part-i.pdf)				3.2: 1, 5, 9, 11, 17, 29, 33

Mar 4 - Mar 8   			Monday: no class \\								\\ 
							Wednesday: review \\  							[Review guide solutions](review1s.pdf)
							Friday: exam
						
Mar 11 - Mar 15   			spring break

Mar 18 - Mar 22    			Monday: 3.3 \\									[Quotient rule](3-3-part-ii.pdf) \\												3.3: 3, 5, 7, 9, 11, 13, 15, 17, 19, 23, 31 \\
							Wednesday: 3.4 \\  								[Chain rule](3-4.pdf) \\  														3.4: 1-57 odd, 59, 61, 63, 65, 75 \\  
							Friday: 3.5 									[Trig derivatives](3-5.pdf)	    												3.5: 1-49 odd  

Mar 25 - Mar 29    			Monday: 3.6, 3.7 \\  							[Implicit differentiation](3-6-7.pdf) \\  										3.6: 1, 3, 7, 9, 11, 13, 17, 19, 21 \\
							Wednesday: review \\  							[Derivatives review solutions](derivatives-review-sol.pdf)						3.7: 1, 5, 7, 15, 23, 25, 27, 31
							Friday: skills quiz  

Apr 1 - Apr 5   			Monday: 4.1 \\									[First and second derivative tests](4-1.pdf) \\									4.1: 5, 7, 9, 11, 13, 15, 17, 19 \\
							Wednesday: 4.1 \\								[Inflection points](4-1-ii.pdf)	\\												\\
							Friday: 4.2										[Global extrema](4-2.pdf)														4.2: 1, 5, 7, 9, 25, 27

Apr 8 - Apr 12   	    	Monday: 4.3 \\									[Optimization](4-3.pdf) \\														4.3: 1, 3, 5, 7, 11, 17, 19 \\
							Wednesday: 4.3 \\								\\																				\\
							Friday: 4.6										[Related rates](4-6.pdf)														4.6: 3, 15, 21, 23, 25, 33

Apr 15 - Apr 19    			Monday: 4.6 \\    								\\
							Wednesday: review \\   							[Review guide solutions](review2s.pdf),
							Friday: exam										[Extra problem solutions](review2-extra-sol.pdf) \\

Apr 22 - Apr 26     		Monday: 5.1, 5.2 \\								[Areas, Riemann sums](5-1.pdf), [(filled in version)](5-1s.pdf)	\\				5.1: 1, 3, 7, 9, 11, 17, 19 \\
							Wednesday: 5.2 \\								[Geometry of definite integrals](5-2.pdf), [(filled in version)](5-2s.pdf) \\	5.2: 1, 3, 5, 35, 37, 39, 46, 51 \\
							Friday: 5.3, 6.2 								[Fundamental theorem of calculus](5-3.pdf), [(filled in version)](5-3s.pdf)		5.3: 9, 11, 15; 6.2: 7-21 odd, 57-65 odd

Apr 29 - May 3				Monday: wrap up \\								[Review guide solutions](review3s.pdf)
							Wednesday: reading day \\
							Friday: exam period	begins	
							  
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


## Getting help ##


Here are a few ways to get help:     

-   **Office Hours**: <#include officehours-s19.txt>  
-	**Evening TA help**.  Student TA's will hold office hours for the class several nights a week in Clapp.  **Alissa Smith** 
	and **Amelia Tran** are the TAs for our section, but the sections will all cover similar material, 
	so you should feel free to attend others' sessions as well.  The evening help schedule is as follows:
	+	**Sundays, 7:00 – 9:00 pm** in Clapp 407 with Di Guo
	+	**Mondays, 7:00 – 9:00 pm** in Clapp 407 with Alissa Smith and Sachie Tran
	+	**Tuesdays, 7:00 – 9:00 pm** in Clapp 407 with Amelia Tran
	+	**Wednesdays, 7:00 – 9:00 pm** in Clapp 407 with Nicole Andrews
	+	**Thursdays, 7:00 – 9:00 pm** in Clapp 407 with Maya Brody and Daphne Gauthier
-	**Individual tutoring**: There will be some opportunities to meet individually with TAs by appointment (link below).
	+	Alissa will hold time **Sundays, 4:00 – 5:00 pm** on the Clapp 4th floor landing near the sloth fossil.
		Use [Alissa's sign-up page][] to reserve an appointment.
	+	Amelia will hold time **Mondays, 6:00 – 7:00 pm** in Clapp 420.
		Use [Amelia's sign-up page][] to reserve an appointment.
-	**Study groups**: Other students in the class are a wonderful resource.
	I want our class to feel like a community of people working together.  Please get in touch if you'd like me to help you find
	study partners, and please reach out if you'd like others to join your group.
	
## Resources ##

-	[Wolfram Alpha][]: a useful way to check your answers on computations.
	It can do algebra and calculus, among other things, and it understands a mix of English and symbols.
-	[Desmos][]: a nice website for graphing functions.

	
[syllabus]: ./syllabus
[guidelines]: http://mtholyoke.edu/~tchumley/assets/pdf/homework-guidelines.pdf
[Wolfram Alpha]: https://www.wolframalpha.com
[Desmos]: https://www.desmos.com
[Alissa's sign-up page]: https://calendar.google.com/calendar/selfsched?sstoken=UUg3RFN2MnkzYXlTfGRlZmF1bHR8ZDg0Mzg3NmM5ZGQxODZmNDljZDg5NWVkYWYzNzQ2ZDg
[Amelia's sign-up page]: https://calendar.google.com/calendar/selfsched?sstoken=UU8ydUF2MjJhV3V0fGRlZmF1bHR8NWJkYjFkMmVlNjE1MDZmYzU5YTkyNTljMWY0ODQzYTk


