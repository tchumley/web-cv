---
title: Math 232, Sep 10 Assignment
footer: Last modified <#exec date>
<#include nav-m232.md>
---

Thanks for submitting great questions!  I'm writing responses to some of your questions below, and will respond in class to the others.  Please feel free to continue to ask questions!
I'll continue to send out similar assignments through the semester.

### Submit at least one question about the material from the first week of class.

**Does the Cartesian Product only apply to elements with ordered pairs / triple pairs / etc, or does it only work within the context of the coordinate system?**

The Cartesian product is a way for us to build or denote the elements of a coordinate system.  For example, the $xy$-plane is built up from coordinates (or points or elements or pairs) $(x,y)$,
which are elements of the Cartesian product $\\mathbb R \\times \\mathbb R$.  However, we don't need to think of a coordinate system when thinking about Cartesian products; we can just think about pairs, triples, etc of numbers
without thinking of them in space.  For example, the pairs might consist of two pieces of data (like height and weight) for subjects in a statistical study.


**Can I think of sets like arrays?**

Yes and no.  Arrays (in computer science) and sets (in math) are similar in that they're both a way of collecting or grouping a bunch of elements (like numbers).
However, a set doesn't impose any order on the elements, and repeated elements are not allowed.  Arrays do impose an order (you access an element by specifying its index) and do allow repeated elements.


**The Cartesian product makes me think of matrix. Is there any relationship?**

Kind of.  A nice way of listing out the elements of a Cartesian product of two sets is to make a table whose entries are pairs (a,b) where a is an element of the first
set in the Cartesian product and b is an element of the second set.  We'll do this in class on Monday.  Also, while a matrix is kind of a table, its elements are single numbers, not pairs.


**Does an empty set have a subset?**

Yes!  But only one: the empty set.  (Rememeber that $A \\subseteq A$ for any set $A$)


**Is 0 a natural number?**

No, although there's not really a good reason for this.  Natural numbers start at 1.


**In the book, at the end of section 1.3:**

> For instance, consider the unit circle $C = \\{(x, y) \\in \\mathbb R^2: x^2 + y^2 = 1\\}$.  This is a subset $C \\subseteq \\mathbb R^2$.
Likewise the graph of a function $y = f(x)$ is a set of points $G = \\{(x, f (x)) : x \\in \\mathbb R\\}$, and $G \\subseteq \\mathbb R^2$.

**I don't understand why $C \\subseteq \\mathbb R^2$ and $G \\subseteq \\mathbb R^2$.**

Remember that $A \\subseteq B$ means that every element of $A$ is also an element of $B$.
Note that $\\mathbb R^2$ consists of all points $(x,y)$ where $x,y$ are real numbers. 
The circle $C$ consists of a particular set of points $(x,y)$. Therefore, every element of $C$ is also an element of $\\mathbb R^2$.
The same holds with $G$, which consists of pairs $(x,y)$ that make up the graph of a function.


**If $A=\\{4,\\{5,7,9\\},10\\}$, is $\\{5\\}$ a subset of $A$? Does $\\{5\\}$ belong to $A$?**

Note that $A$ consists of 3 elements: the numbers $4$ and $10$ and the set $\\{5,7,9\\}$.   $\\{5\\}$ is not one of the 3 elements of $A$, so $\\{5\\}$ does not belong to $A$.
Also $\\{5\\}$ is not a subset of $A$.  We would need $5$ to be an element of $A$ for $\\{5\\}$ to be a subset.


**The notation makes the problems confusing for me. Is there anything I can do to make the problems easier for me to understand or should I just make a reference sheet?**

Especially when starting out, the notation and language of math can be confusing.  The good thing is that well written math tries very hard to avoid being ambiguous or imprecise.
I think making a reference sheet is a great idea.  I'll also make an effort to emphasize in class when something is potentially confusing or easy to forget.  Otherwise,
make sure to ask about anything you're unsure about as early as possible.


**For the problem 1 in the first class, why does $D-L+R$ always equal 2**

This is a great question that we'll prove in class once we talk about two important concepts: proofs by induction and graph theory.  For now, we'll have to leave it in the dark
a little bit, but here's a link you might take a look at about [Euler characteristic](https://en.wikipedia.org/wiki/Euler_characteristic).


**This question wasn't mentioned in the class but I saw it when I was reading the textbook. For the Cartesian product, I don't understand how to calculate the order triple. 
For the material from the first week of class, I am a little bit confused about the empty set.**

We'll talk about Cartesian products in class on Monday.  As a short answer to your question about ordered triples, they're elements of the set $$ A \\times B \\times C = \\{(a,b,c) : a \\in A, b \\in B, c \\in C\\}.$$
You can think of listing out all the elements by making a 3-dimensional table.  About empty sets, we'll continue to discuss these in the context of examples.  For now, you can think of it as a very simple set; it's just $\\{\\}$.
If you think of a set as a box with elements inside, then it's just an empty box.


**If there is a set as a subset in a set, e.g. $\\{\\{a,b\\},c,d\\}$, does the subset $\\{a\\}$ in the set $\\{a,b\\}$ count as an element of the original set?**

No, it does not count as element of the original set.  This is a lot like one of the questions above; you're all thinking of great questions!


**What is $\\mathcal P\\left( \\mathcal P\\left( \\mathbb R^2 \\right)\\right)$?**

This is a really big set!  To start to get an idea of how big it is, try finding $\\mathcal P\\left( \\mathcal P\\left( \\{a,b,c\\} \\right)\\right)$.  (I'll warn you that just this set has cardinality $2^8 = 256$)


**In what cases do people use $\\emptyset$ and $\\{\\emptyset\\}$? I am not confused about the difference between them, but I am just curious about the meaning and why do we have to have different things even though they're all empty.**

Something like $\\{\\emptyset\\}$ is a little esoteric and doesn't pop up much in practice.  However, $\\{\\emptyset\\}$ is not the same as $\emptyset$, because $\\{\\emptyset\\}$ is a set with 1 element.  


**How do we know that 40 is the smallest prime number?**

I think you mean, how do we know $40$ is the smallest natural number $n$ for which $f(n) = n^2 + n + 41$ is prime.  For now, we can check this is true by brute force (plugging in all the numbers from 1 to 39 and checking by hand if the result is prime).  When we get to number theory, we'll talk more about factoring numbers into primes and we might be able to give a more systematic or clever way of looking at this problem.


**Why do we use set builder notation? In other words how does it help us? I am a little confused about its purpose.**

We use set builder notation in order to precisely describe all the elements in a set.  For example, we might represent an infinite set like $\\{1, 2, 4, 8, 16, 32, \ldots\\}$ and it's relatively easy to see that the pattern is that
we have a set which consists of $2^n$ for whole numbers $n\geq 0$.  Set builder notation allows us erase ambiguity in a situation like this and explicitly tell what the pattern for elements in the set is.  This is especially useful
if the pattern is not immediately obvious.


**Is it different if there's an empty set vs an empty set in empty brackets?**

I'm a little unsure about what you mean by "empty set in empty brackets," but I'll write out a little bit about empty sets.  $\\emptyset$ and $\\{\\}$ are the same thing; they both denote the empty set and we can think of them as an empty box.  Likewise, $\\{\\emptyset\\}$ and $\\{\\{\\}\\}$ are the same thing (but different from $\\emptyset$).  They both denote a set which contains one element: the empty set.  



**How would you describe a "range", like in definition form.**

In order to talk about range, we have to introduce functions.  A function $f$ is a rule which sends elements from one set, say $A$, into another set, say $B$.  We denote this by $f : A \\to B$.  The range of $f$
is the set of all elements of the form $f(x)$ where $x$ is an element of $A$.  In set builder notation, the range is $\\{f(x) \\in B : x \\in A\\}$.  We'll talk about this stuff later in the semester.


### Read sections 1.6 and 1.7 and submit at least one question on this material.


**Does cardinality have distributive properties?**

Kind of.  For example, with the Cartesian product, $|A \\times B| = |A|\\cdot |B|$.  Also, if $A$ and $B$ are disjoint sets, then $|A \\cup B| = |A| + |B|$.  However, this last formula doesn't hold if $A$ and $B$ have some overlap.  (Why?) Since subtle cases like this often pop up, we don't usually explicitly talk about distributive properties of the Cartesian product.

**Suppose $A = \\{(x, x^2) : x \\in \\mathbb R\\}$. Why do we define the universal set as $\\mathbb R^2$ but not $\\mathbb R$?**

The universal set should be a "superset" of $A$.  Or in other words $A$ should be a subset of its universal set.  However, for your example, $A$ contains pairs, so it's a subset of the Cartesian product $\\mathbb R \\times \\mathbb R = \\mathbb R^2$.

**I don't quite understand the idea of universal set. If $A=\\{2,4\\}, B=\\{1,3\\}$ is $U=\\{1,2,3,4\\}$?**

That's one example of a universal set for this example.  The universal set could also be $\\mathbb N$ or $\\mathbb Z$ or $\\mathbb R$ since $A$ and $B$ are subsets of all of these.  The universal set is usually explicitly stated or clear from the context of a problem.  On homeworks, quizzes, and exams, I'll be sure to make it unambiguous what the universal set is.

**Can we say $B-A = A - (A \\cap B)$?**

Not quite.  If you draw a Venn diagram, you can see that $B-A = B - (A\\cap B)$.
