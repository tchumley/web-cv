---
title: Math 301
footer: Last modified <#exec date>
<#include nav-m301.yml>
---

<img style="float: right;" src="/~tchumley/assets/img/m301.png" width=400px class="img-responsive">
**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 538-2299  
**e-mail**: tchumley  
**Office Hours**: <#include officehours-f20m1.txt>  

Textbook: *Elementary Analysis: The Theory of Calculus* by Kenneth A. Ross, 
ISBN: 9781461462705; \\
available as a free [e-text][] when logged into the VPN.

---------

## Announcements ##

Announcements will be posted on the [Course Announcements Moodle forum][] throughout the semester.
Recordings of class lectures will also be posted on Moodle, 
but essentially all other materials will be posted on this page.


## Syllabus ##

Check the [syllabus][] for all the important class policies (grades, attendance, etc.). 


## Homework ##

There will be weekly homework assignments throughout the semester to be turned in.
Please read these [guidelines][] for writing mathematics.

- **Group work**. Some of your homework will be done in teams.
	*	These problems will be due on **Fridays**.
	*	You should type solutions in LaTeX and submit only one copy on behalf of the whole team.
- **Solo work**. A selection of problems will be assigned to be written up individually and turned in each week.
	*   These problems will also be due **Fridays**.
	*	You may work with others but the writing should be done on your own.
	*	Feel free to write solutions by hand or type them in LaTeX.
- **Gradescope**. Homework will be turned in through [Gradescope][].
	*	Please add yourself to the Gradescope course using Entry Code **xxxx**.
	*	Gradescope has made a [short tutorial][] on submitting homework.
- **Collaboration**. I want you to work together on the written homework, of course on the
	group assignments but even on the preliminary and solo assignments. 
	The process of explaining your ideas to one another really helps in learning math. 
	However, you must write up the solo assignments on your own and avoid copying others' work directly. 
	Please acknowledge sources of help, including classmates or outside textbooks,
	by writing  "help from ..." at the top of your assignment.
	Also, please only write what you understand so that I know where to help, and
	please avoid using online forums like Math StackExchange, solutions manuals, or similar resources.
	A huge part of learning in this class comes from figuring out how to get unstuck
	by spending time thinking on your own or talking to me and classmates; these other
	resources can end up being counter-productive in the long term.
- **Rewrites**. Homework is for practice, 
	and you are not expected to write perfect answers from the start! 
	If you would like to earn back lost credit on a homework assignment,
	you may resubmit it to me a week after it's returned to you.
	For some problems it's difficult to allow redo's fairly, 
	so on occasion I might not allow a problem to be redone.
	
			

Assignment							Due			
--------------------				-----		
[Homework 0](hw0.pdf)				Aug 26
[Homework 1](hw1.pdf) 				Aug 28
[Homework 2](hw2.pdf)				Sep 4
[Homework 3](hw3.pdf)				Sep 11
[Homework 4](hw4.pdf)				Sep 18
[Homework 5](hw5.pdf)				Sep 25
[Homework 6](hw6.pdf)				Oct 2
[Homework 7](hw7.pdf)				Oct 9


## Quizzes ##

There will be (mostly) weekly quizzes that will be given on Mondays.
They will be posted on Gradescope and you'll have until Tuesday before class to complete them.
The purpose of these is to check in to see that you're comfortable with fundamental material
like definitions, theorems, and important examples and proofs.
Problems will always be related to the previous week's homework and class topics.

Quiz         	 			Date     	Material
------------ 				-------- 	------------
Quiz 1		      			Aug 31		Sections 1-4		
Quiz 2						Sep 8		Sections 7-9 
Quiz 3						Sep 14		Sections 9-10
Quiz 4						Sep 28		Sections 17-18
Quiz 5						Oct 5		Sections 19, 28

## Exams ##

There will be a midterm and a final.  The date for the mid-term is subject to change slightly.


Exam         	 			Date     	Format			        Material                 
------------ 				-------- 	-------------------- 	-----------------------  
Midterm		      			Sep 18		take-home		        Chapters 1-2			
Final						Oct 15  	take-home				Chapters 3-5			





## Course plan ##

Our plan is to cover parts of the textbook chapters 1 – 4. 
If there's time, we'll talk about parts of chapter 5 and 6 too.
Below is a rough outline of what is to be covered week by week through the semester. 
Please check back regularly for precise details on what is covered, 
as well as postings of class materials like lecture notes.


<#include open-accordion.txt>

<#define WEEK|1>
<#define DAYS|August 24 – August 28>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Introduction

-----------

##### Monday

* **Topic**: Sections 1, 2: The natural, rational, and real numbers. A hint at why we need real numbers and some review of induction.
* **Class materials**: [Blank lecture notes](w1-d1-blank.pdf), [Lecture notes filled](w1-d1-filled.pdf), [Induction review](induction-review.pdf).
* **After class**: Please finish the induction review worksheet.

##### Tuesday

* **Topic**: Section 3: The real numbers. A look at the ordered field axioms, the absolute value function, and some useful inequalities.
* **Class materials**: [Blank lecture notes](w1-d2-blank.pdf), [Lecture notes filled](w1-d2-filled.pdf), [Basic inequalities](basic-inequalities.pdf).
* **After class**: Please finish the basic inequalities worksheet, read Section 4, up to and including Example 4.

##### Wednesday

* **Topic**: Section 4: The completeness axiom. A look at supremum, infimum.
* **Class materials**: [Blank lecture notes](w1-d3-blank.pdf), [Lecture notes filled](w1-d3-filled.pdf), [Suprema and infima](sup-inf.pdf).
* **After class**: Please finish the suprema and infima worksheet, and read the rest of Section 4 (but you may skip the proof Corollary 4.5).


##### Thursday

* **Topic**: Section 4: The completeness axiom. A look at the key defining property of the reals.
* **Class materials**: [Blank lecture notes](w1-d4-blank.pdf), [Lecture notes filled](w1-d4-filled.pdf), [Archimedean property](archimedean-property.pdf).
* **After class**: Please read the proof of the density of Q in R in today's filled in notes, and read Section 7 up through Example 2.

##### Friday

* **Topic**: Sections 7, 8: Introduction to sequences and limits. A look at the formal definition of convergence and how we prove sequences converge.
* **Class materials**: [Blank lecture notes](w1-d5-blank.pdf), [Lecture notes filled](w1-d5-filled.pdf)
* **After class**: Please meet with your group mates and start on Homework 2. For Monday, read examples 3 and 4 in Section 8.

><#include open-accordion-panel.txt>

<#define WEEK|2>
<#define DAYS|August 31 – September 4>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Sequences

-----------

##### Monday

* **Topic**: Section 8: Discussion about proofs. We continue our look at introductory proof examples involving limits of sequences.
* **Class materials**: [Blank lecture notes](w2-d1-blank.pdf), [Lecture notes filled](w2-d1-filled.pdf)
* **After class**: Read Theorems 9.1 to 9.4 and their proofs.


##### Tuesday

* **Topic**: Section 9: Limit theorems for sequences. A look at how to prove the theorems we used in calculus for computing limits.
* **Class materials**: [Blank lecture notes](w2-d2-blank.pdf), [Lecture notes filled](w2-d2-filled.pdf)
* **After class**: Read Section 10 up through Example 2. 

##### Wednesday

* **Topic**: Section 10: The monotone convergence theorem. We show that sequences that are bounded and either increasing or decreasing must always converge.
* **Class materials**: [Blank lecture notes](w2-d3-blank.pdf), [Lecture notes filled](w2-d3-filled.pdf)
* **After class**: Read Section 10, from Corollary 10.5 through to Theorem 10.7 and its proof.

##### Thursday

* **Topic**: Section 10: Cauchy sequences. We introduce another useful class of sequences that always converge. Along the way, we learn about the limit supremum and limit infimum.
* **Class materials**: [Blank lecture notes](w2-d4-blank.pdf), [Lecture notes filled](w2-d4-filled.pdf), [Liminf, limsup theorems](limsup-liminf-proofs.pdf)
* **After class**: Finish up today's worksheet and read the proof of Theorem 10.11 in the textbook.

##### Friday

* **Topic**: Section 10: Cauchy sequences, continued. We get into the heart of the matter with Cauchy sequences, showing that they always converge.
* **Class materials**: [Blank lecture notes](w2-d5-blank.pdf), [Lecture notes filled](w2-d5-filled.pdf)
* **After class**: Read Section 11 on subsequences and prepare for Tuesday's quiz on Homework 2 material.



><#include open-accordion-panel.txt>

<#define WEEK|3>
<#define DAYS|September 7 – September 11>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Sequences

-----------

##### Monday

* **Topic**: Labor Day, no class


##### Tuesday

* **Topic**: Section 11: Subsequences. We discuss the Bolzano-Weierstrass theorem, which says that every bounded sequence has a convergent subsequence.
* **Class materials**: [Blank lecture notes](w3-d1-blank.pdf), [Lecture notes filled](w3-d1-filled.pdf), [Subsequences](subsequences.pdf)
* **After class**: Finish up the subsequences worksheet and read Section 14 through the comparison test.

##### Wednesday

* **Topic**: Section 14: Series. We discuss infinite series, looking at and proving some familiar tests of convergence from calculus.
* **Class materials**: [Blank lecture notes](w3-d2-blank.pdf), [Lecture notes filled](w3-d2-filled.pdf)
* **After class**: Read about the comparison, root, and ratio tests in Section 14. Try Exercise 14.6.

##### Thursday

* **Topic**: Section 14: Series, continued. We continue our discussion of infinite series, diving into a powerful test of convergence: the root test.
* **Class materials**: [Blank lecture notes](w3-d3-blank.pdf), [Lecture notes filled](w3-d3-filled.pdf), [Series](infinite-series.pdf)
* **After class**: Read Theorem 12.2 and try Exercise 14.2.

##### Friday

* **Topic**: Section 15: Alternating series. We wrap up or discussion on infinite series (for now) with a look at alternating series.
* **Class materials**: [Blank lecture notes](w3-d4-blank.pdf), [Lecture notes filled](w3-d4-filled.pdf)
* **After class**: Get started on homework 4 and read the first three pages of Section 17.


><#include open-accordion-panel.txt>

<#define WEEK|4>
<#define DAYS|September 14 – September 18>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Continuity

-----------

##### Monday

* **Topic**: Section 17: Continuous functions. We begin our discussion of continuous functions and get introduced to delta-epsilon proofs of continuity.
* **Class materials**: [Blank lecture notes](w4-d1-blank.pdf), [Lecture notes filled](w4-d1-filled.pdf), [Continuity](continuity.pdf)
* **After class**: Work on the remaining worksheet problems and read the proof of Theorem 17.2


##### Tuesday

* **Topic**: Section 17: Continuous functions, continued. We continue our work with delta-epsilon proofs.
* **Class materials**: [Blank lecture notes](w4-d2-blank.pdf), [Lecture notes filled](w4-d2-filled.pdf)
* **After class**: Try the extra problems suggested in lecture and review the proof of Theorem 17.2 again.

##### Wednesday

* **Topic**: Section 17: Continuous functions, concluded. We prove how the delta-epsilon formulation of continuity is equivalent to the sequential formulation.
* **Class materials**: [Blank lecture notes](w4-d3-blank.pdf), [Lecture notes filled](w4-d3-filled.pdf)
* **After class**: Review the Bolzano-Weierstrass theorem and read Theorem 18.1 and its proof.

##### Thursday

* **Topic**: Section 18: Properties of continuous functions. We discuss the extreme value theorem.
* **Class materials**: [Blank lecture notes](w4-d4-blank.pdf), [Lecture notes filled](w4-d4-filled.pdf)
* **After class**: Read Theorem 18.2 and its proof.

##### Friday

* **Topic**: Section 18: Properties of continuous functions, continued. We discuss the intermediate value theorem and its consequences.
* **Class materials**: [Blank lecture notes](w4-d5-blank.pdf), [Lecture notes filled](w4-d5-filled.pdf)
* **After class**: Read Definition 19.1 and Examples 1 and 2 in Section 19.


><#include open-accordion-panel.txt>

<#define WEEK|5>
<#define DAYS|September 21 – September 25>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Continuity

-----------

##### Monday

* **Topic**: Section 19: Uniform continuity. We review delta-epsilon proofs of continuity and introduce a definition stronger than continuity.
* **Class materials**: [Blank lecture notes](w5-d1-blank.pdf), [Lecture notes filled](w5-d1-filled.pdf)
* **After class**: Finish up your exam.


##### Tuesday

* **Topic**: Section 19: Uniform continuity, continued. We discuss how to prove functions fail to be uniformly continuous.
* **Class materials**: [Blank lecture notes](w5-d2-blank.pdf), [Lecture notes filled](w5-d2-filled.pdf)
* **After class**: Read the introduction to Section 20 and Theorems 20.4 and 20.6.

##### Wednesday

* **Topic**: Section 20: Limits of functions. We wrap up our discussion on uniform continuity and formalize the notion of limits of functions.
* **Class materials**: [Blank lecture notes](w5-d3-blank.pdf), [Lecture notes filled](w5-d3-filled.pdf)
* **After class**: Read the introduction to Section 28.

##### Thursday

* **Topic**: Section 28: Basic properties of the derivative. We introduce the definition of the derivative and look at some basic examples and facts.
* **Class materials**: [Blank lecture notes](w5-d4-blank.pdf), [Lecture notes filled](w5-d4-filled.pdf)
* **After class**: Try Exercise 28.8.

##### Friday

* **Topic**: Section 28: Basic properties of the derivative, continued. We consider the differentiability of some more sophisticated examples of functions.
* **Class materials**: [Blank lecture notes](w5-d5-blank.pdf), [Lecture notes filled](w5-d5-filled.pdf)
* **After class**: Read the beginning of Section 29 try to summarize how the first three theorems fit together.


><#include open-accordion-panel.txt>

<#define WEEK|6>
<#define DAYS|September 28 – October 2>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Sequences of functions

-----------

##### Monday

* **Topic**: Section 29: The mean value theorem. We build up a proof for one of the most important results in the theory of calculus.
* **Class materials**: [Three core theorems](mvt.pdf), [Lecture notes filled](w6-d1-filled.pdf)
* **After class**: Read the corollaries that follow the mean value theorem in Section 29.


##### Tuesday

* **Topic**: Section 29: The mean value theorem, continued. We wrap up the proof of the mean value theorem and look at some corollaries.
* **Class materials**: [Three core theorems](mvt.pdf), [Lecture notes filled](w6-d2-filled.pdf)
* **After class**: Read Section 24 up through Definition 24.2, think about the proof of the Fundamental Theorem of Calculus, part I, and work on the [Pointwise convergence of functions](pointwise-convergence.pdf) worksheet.

##### Wednesday

* **Topic**: Section 24: Uniform convergence. We talk about sequences of functions and introduce a simple, but in a sense flawed, notion of convergence for these sequences: pointwise convergence.
* **Class materials**: [Pointwise convergence of functions](pointwise-convergence.pdf), [Lecture notes filled](w6-d3-filled.pdf)
* **After class**: Work on Problem 2 in today's worksheet.

##### Thursday

* **Topic**: Section 24: Uniform convergence, continued. We discuss uniform convergence of sequences of functions and talk about its relationship with continuity.
* **Class materials**: [Lecture notes filled](w6-d4-filled.pdf)
* **After class**: Read the proofs of Theorem 24.3 and 25.2.

##### Friday

* **Topic**: LEAP symposium, no class


><#include open-accordion-panel.txt>

<#define WEEK|7>
<#define DAYS|October 5 – October 9>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Sequences of functions

-----------

##### Monday

* **Topic**: Sections 24: Uniform convergence, continued. We work with a computational technique for determining uniform convergence on a domain.
* **Class materials**: [Uniform convergence of functions](uniform-convergence.pdf), [Lecture notes filled](w7-d1-filled.pdf)
* **After class**: Work on the remaining worksheet problems.


##### Tuesday

* **Topic**: Sections 25: More on uniform convergence. We discuss some consequences of uniform convergence related to continuity and integration.
* **Class materials**: [Uniform convergence and continuity](uniform-conv-continuity.pdf), [Uniform convergence and integration](uniform-conv-integration.pdf), [Lecture notes filled](w7-d2-filled.pdf)
* **After class**: Continue working on the worksheets and review the Fundamental Theorem of Calculus, parts I and II, from Tuesday last week.

##### Wednesday

* **Topic**: Sections 25: More on uniform convergence, continued. We continue our work with integration and uniform convergence.
* **Class materials**: [Uniform convergence and integration](uniform-conv-integration.pdf)
* **After class**: Be ready to discuss Problems 2 and 3 on the worksheet at the start of class tomorrow.

##### Thursday

* **Topic**: Sections 25: More on uniform convergence, continued. We discuss the relationship between uniform convergence and differentiation.
* **Class materials**: [Uniform convergence and derivatives](uniform-conv-diff.pdf), [Lecture notes filled](w7-d4-filled.pdf)
* **After class**: Wrap up Problem 2 on the derivatives worksheet.

##### Friday

* **Topic**: Wrap up. We discuss the derivatives worksheet and future study.
* **Class materials**: [Uniform convergence and derivatives](uniform-conv-diff.pdf), [Lecture notes filled](w7-d5-filled.pdf)
* **After class**: Begin working on study guide and sample exam problems.


><#include open-accordion-panel.txt>

<#define WEEK|8>
<#define DAYS|October 12 – October 15>
<#define COLLAPSED|true>
<#define PANEL_CONTENTS |

#### Wrap up

-----------

##### Monday

* **Topic**: Office hours, no class


##### Tuesday

* **Topic**: reading day

##### Wednesday

* **Topic**: reading day

##### Thursday

* **Topic**: final exam due at 11:59 pm


><#include open-accordion-panel.txt><#include div-close.txt>



## Getting help ##

Here are a few ways to get help:

-   **Office hours**: <#include officehours-f20m1.txt>  
-	**Study groups**: Other students in the class are a wonderful resource.
	I want our class to feel like a community of people working together.
	Even though our class won't have a TA or evening help, I'm planning to set up
	groups for people to work together.
-	**Piazza**: I've set up an [online forum][] for asking and answering questions about
	class material, including homework problems. My plan is to check the forum every day
	and answer questions as they come up, but I hope everyone in the class will pitch in 
	and answer others' questions when possible.
	
## Resources ##

-	When away from campus, a [college VPN][] is available to access resources normally only
	available when on the campus network (eg. free e-books through LITS and Springer Link).
	However, I understand there might be some accessibility issues even with the VPN, 
	so I will do my best to help everyone access 
	necessary materials through other methods as well.
-	I've collected some resources to help you when writing homework solutions with [LaTeX][].
	* Here is a [LaTeX template][] file for writing homework solutions and its [pdf output][].
	* A [quick reference][] is available for commonly used symbols.
	* [Overleaf][] is a cloud service that lets you edit and compile LaTeX through a web
		browser so that no local installation is needed. The free version will be sufficient
		for our class.
	* To install LaTeX locally on your personal computer, I recommend
	installing [MacTex][] if you use a Mac, [MikTeX][] for Windows, or [TeXLive][] for Linux.
-	There are many real analysis books out there. Reading our required textbook will be enough
	to get a strong understanding of the material in class, but if you're curious, here
	are some other good books. The first two are available for free as e-books when
	signed into the campus network or VPN.
	*	[*Understanding Analysis*][] by Stephen Abbott. Similar to our textbook, but with
		some interesting chapter introductions and projects.
	*	[*Real Mathematical Analysis*][] by Charles C. Pugh. Nicely written, but with more
		emphasis on topology and metric spaces than our textbook.
	*	*Principles of Mathematical Analysis* by Walter Rudin. A classic textbook that
		is a bit too terse for a first analysis course but worth reading if you're
		really excited about analysis. It's filled with great, challenging exercises.


	
[e-text]: http://proxy.mtholyoke.edu:2048/login?url=http://dx.doi.org/10.1007/978-1-4614-6271-2
[syllabus]: ./syllabus
[guidelines]: https://math.hmc.edu/su/wp-content/uploads/sites/10/2019/11/good-math-writing.pdf
[Gradescope]: https://www.gradescope.com
[short tutorial]: https://gradescope-static-assets.s3.amazonaws.com/help/submitting_hw_guide.pdf
[online forum]: https://piazza.com/mtholyoke/fall2020/math301/home
[Course Announcements Moodle forum]: https://moodle.mtholyoke.edu/mod/forum/view.php?id=548975

[college VPN]: https://lits.mtholyoke.edu/tech-support/access-and-internet-connectivity/connect-campus/using-virtual-private-network-vpn

[LaTeX]: https://en.wikipedia.org/wiki/LaTeX
[Overleaf]: https://www.overleaf.com
[MacTeX]: http://tug.org/mactex/
[MikTeX]: https://miktex.org/download
[TeXLive]: https://www.tug.org/texlive/
[quick reference]: https://www.caam.rice.edu/~heinken/latex/symbols.pdf
[LaTeX template]: /~tchumley/assets/tex/homework-template.tex
[pdf output]: /~tchumley/assets/pdf/homework-template.pdf

[*Understanding Analysis*]: https://link.springer.com/book/10.1007%2F978-1-4939-2712-8
[*Real Mathematical Analysis*]: https://link.springer.com/book/10.1007%2F978-3-319-17771-7


