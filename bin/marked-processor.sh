#!/bin/bash

cat | tee | /opt/local/bin/gpp -x -H -I/Users/tchumley/web-cv/include | cat - /Users/tchumley/web-cv/include/metadata.yaml | pandoc -s -f markdown+link_attributes+smart+multiline_tables+escaped_line_breaks --toc -t html --template=/Users/tchumley/web-cv/include/bootstrap-template.html --mathjax

