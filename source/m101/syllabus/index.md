---
title: Math 101 Syllabus
<#include nav-m101.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley      

---------

**Course**: Math 101, Calculus I

**Textbook**: *Calculus: Single Variable, 7th Edition* by Deborah Hughes-Hallett et al, ISBN: 9781119139317; \\
(any print or e-text of the 7th edition is fine); \\
on library reserve under QA303.2 .H845 2017

**Learning goals**: A big part of first-semester calculus is trying to understand real-world phenomena which can be described as rates of change (eg. velocity, population growth, fluid flow).  Calculus, and mathematics in general, is deep and interesting because of the back and forth between trying to understand specific real-world problems and trying to understand how these problems can be described and answered with a general theory.  During the semester our goal is to:

-	Get exposure and experience in the following topics:
	*	a review of important functions (polynomials, exponential functions, 
		trigonometric functions, exponential functions, and logarithms)
	*	the derivative of a function
	*	applications of differentiation
	*	an introduction to integration
-	Use functions, derivatives, and limits to model, predict, and understand
	real world phenomena.
-	Practice working with others to improve your learning. The ability to work
	successfully with others to achieve a common goal is an important skill and
	will help you in all that you do both in college and in your future career. 
-	Practice reading, writing, and communicating technical ideas.


**Homework**: There will be weekly written homework assignments due at the beginning of class on Wednesdays.  You're encouraged to start early on the assignments; they're not meant to be done in one sitting.

**Quizzes**: There will be weekly 10-15 minutes quizzes at the beginning of class on Fridays.  The problems on quizzes will be similar to homework assignments and are meant to keep everyone up to date and studying between exams.

**Skills quizzes**: There will be two skills proficiency quizzes, which are longer than weekly quizzes.  They will focus on algorithmic computations.

**Exams**: There will be two midterm exams and a self-scheduled final.  Exams will have algorithmic computations to do, but they will generally be more conceptual.

**Technology**: Here are some general remarks on the use of calculators, software, and phones:

-	For exams and quizzes, you may use a scientific calculator, but it is not necessary or required (I generally write questions that can be done by hand or left unsimplified).
-	Software or other tools like Wolfram Alpha can be used on homework as a secondary source but should be cited.  You're expected to show your work on problems.
-	It's ok to take photos of the board for note taking, but please don't post these online.
-	Please keep your phones on Do Not Disturb mode in class, especially during quizzes and exams so as not to disturb others.
-	If you're unsure whether something is ok to use, please feel free to ask.

**Attendance and Participation**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class please prepare by doing any assigned reading and suggested problems. Please expect to talk about math in small groups as well as in class discussions. Other classroom activities may involve worksheets, computer explorations, and informal presentations at the board.

**Late work, makeups**:  In general unexcused late work is not accepted for full credit (typically at most 50% credit if it's more than a week late) and exams must be taken on time.  In special circumstances (eg. bad illness) I will accept late work for full credit or allow for a make-up exam up to a few days late.  Please get in touch as soon as possible if something comes up.

**Getting help**: Here are some of the resources that will be available:

-	**Evening help**:  There will be nightly two hour sessions for you to work in groups with your classmates with TAs who can give your group hints if needed.  Times and location will be announced.
-	**Individual tutoring**: There will be some opportunities to meet individually with TAs.  More information will be announced.
-	**Office hours**: These times (which will be posted near the top of the  class web page) are open drop in sessions where there may be multiple students getting help.  It's expected that you come with specific questions about notes, homework problems, or the text that you have thought about already.  If you need to schedule a time to meet with me one-on-one, either to discuss something private or because you can't make my office hours, please send me an email with times that you are available to meet, and I will find a time that works for both of us.
-	**Study groups**: Other students in the class are a wonderful resource.  I want our class to feel like a community of people working together.

**Grading**: Grades will be assigned based on homework and exams according to the following weighting:

- Participation (including online questions, in-class work): 5%
- Homework: 10%
- Quizzes: 10%
- Skills quiz I: 10%
- Skills quiz II: 10%
- Exam I: 15%
- Exam II: 15%
- Final: 25%

Overall letter grades will be based on a scale no stricter than the usual:

- 93-100:			A
- 90-93:			A-
- 88-90				B+
- 83-88:			B
- 80-83:			B-
- 78-80:			C+
- 73-78:			C
- 70-73:			C-
- 68-70:			D+
- 63-67:			D
- 60-63:			D-
- 0-60:				F

**Academic integrity**: It is very important for you to follow the Honor Code in all of your work for this course. Collaboration on homework assignments is encouraged. However, it is important that you only write what you understand, and that it is in your own words. If you have any questions about what constitutes an Honor Code violation in this class please ask your instructor. Honor Code violations will be brought to the dean. The penalty for violating the Honor Code on any assignment, quiz or exam will be a score of 0 for it. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please contact AccessAbility Services, located in Wilder Hall B4, at (413) 538-2634 or accessability-services@mtholyoke.edu. If you are eligible, they will give you an accommodation letter which you should bring to me as soon as possible.

