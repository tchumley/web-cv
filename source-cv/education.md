Education {.unnumbered}
=========

-   Ph.D. Mathematics, Washington University, 2013
-   M.S. Mathematics, Marquette University, 2007
-   B.S. Computer Science, Mathematics, Marquette University, 2005
