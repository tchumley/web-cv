---
title: Math 333
footer: Last modified <#exec date>
<#include nav-m333.yml>
---

<img style="float: right;" src="/~tchumley/assets/img/m333.png" width=400px class="img-responsive">

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 538-2299  
**e-mail**: tchumley  
**Office Hours**: <#include officehours-s18.txt>   

**Textbook**: *Differential Equations, 4th edition*, by Paul Blanchard,‎ Robert L. Devaney,‎ Glen R. Hall, ISBN: 978-1133109037; \\
on library reserve under QA371 .B63 2011

---------

## Announcements ##

Announcements will be posted here throughout the semester.

**April 29**: Here are office hours for the coming week:

-	Monday, April 30: 10:00 – 11:00
-	Tuesday, May 1: 11:00 – 12:00, 4:00 – 5:00
-	Wednesday, May 2: 2:00 – 3:00

and by appointment.  Please note that I'll be away Thursday and Friday, but I'll be able to reply to email, albeit with some delay.

**April 28**: The final exam was handed out in class and is posted [here](exam3.pdf).

**Apr 17**: [Here][ge] is a nice resource for notes on generalized eigenvectors.  It summarizes 
and includes some proofs of things we discussed.

**Apr 16**: Information on quiz 3 is posted [here](quiz3info.pdf) and below.

**Feb 5**: We'll have to stop using Gradescope and go back to handing in paper copies.  
Starting with this week's problem set 2, I'm hoping you'll turn in your assignments in a 
folder which I'll have on the board next to my office door, Clapp 404b.  The due date and 
time will continue to be 7pm on Wednesdays.  I'm sorry to make the change if you found 
Gradescope convenient.

**Feb 2**: Here is another modeling competition opportunity taking place at Amherst College
April 21 called [SCUDEM][] (in addition to the one I wrote to you about taking place next week).

**Jan 24**: Chapter 1 of our textbook is posted on Moodle in case your copy hasn't arrived yet.



## Syllabus ##

Check the [syllabus][] for all the important class policies (grades, attendance, etc.).


## Homework ##

There will be weekly homework assignments throughout the semester to be turned in, as well
as suggested problems to be considered as additional practice.

- **Written assignments**. A selection of problems taken from the textbook or other sources
	will be assigned to be due **Wednesdays at 7pm**. 
	*	These assignments are given so that you can practice writing mathematics and 
		receive feedback on your progress.
	* 	If you would like feedback on a particular step in a problem, then you can 
		indicate this on your assignment.  
	* 	Each problem will be given a score out of 5. A score of 1 will be given to indicate 
		that to stay on track you should get help from your instructor.
	*	Please refer to these [guidelines][] when writing your homework.
- **Submitting to [Gradescope][]**.  We'll be using an online submission tool called Gradescope 
	to collect, grade, and return homework.  Here are some notes:
	*	Begin by signing up as a student on the Gradescope home page with **Entry Code MD5GVZ**.
	*	I've posted a [short tutorial][] to help with Gradescope if you haven't used it before.
	*	Gradescope has also made a [guide][] to help with scanning and submitting.
	*	More information is available in this [general help guide][].
	*	Please let me know of any issues that come up!
- **Suggested problems**. The book has a good variety of problems, both in terms of content
	and difficulty level.  As you study throughout the semester, you might consider browsing 
	and trying the problems at the end of the chapter for extra practice.  I'll always be happy
	to help over email or in office hours if you're stuck.
	
	
Assignment																			Due			
--------------------																-----		
[Problem set 0](hw0.pdf) [(solutions)](hw0-labs.pdf)								Jan 29
[Problem set 1](hw1.pdf) [(solutions)](hw1s.pdf)									Jan 31
[Problem set 2](hw2.pdf) [(solutions)](hw2s.pdf)									Feb 7
[Problem set 3](hw3.pdf) [(solutions)](hw3s.pdf)									Feb 14
[Problem set 4](hw4.pdf) [(solutions)](hw4s.pdf)									Feb 21
[Problem set 5](hw5.pdf) [(solutions)](hw5s.pdf)									Mar 2
[Problem set 6](hw6.pdf) [(solutions)](hw6s.pdf)									Mar 7
[Problem set 7](hw7.pdf) [(solutions)](hw7s.pdf)									Mar 21
[Problem set 8](hw8.pdf) [(solutions)](hw8s.pdf)									Mar 28
[Problem set 9](hw9.pdf) [(solutions)](hw9s.pdf)									Apr 6
[Problem set 10](hw10.pdf) (updated April 6) [(solutions)](hw10s.pdf)				Apr 11
1-2 page project topic summary														Apr 16
[Problem set 11](hw11.pdf) [(solutions)](hw11s.pdf)									Apr 18
[Problem set 12](hw12.pdf) [(solutions)](hw12s.pdf)									Apr 25



## Quizzes ##

There will be a few quizzes during the semester---roughly 3 total, one midway between each exam. 
These are intended to break up the material between exams and get everyone studying. 
The precise sections covered will be listed below and the types of problems will be 
discussed in class. 


  Quiz          						Date     Material         		Solutions
  ---------    						 	-------- -------------    		----------
  Quiz 1        						Feb 12	 1.1 – 1.3				[(pdf)](quiz1s.pdf)
  Quiz 2								Mar 23   2.1 – 2.4, 2.6			[(pdf)](quiz2s.pdf)
  Quiz 3 [(info)](quiz3info.pdf)		Apr 20	 3.5, 3.6, 3.8			[(pdf)](quiz3s.pdf)

  

## Exams ##

There will be three exams.  The dates for the two mid-terms are subject to change slightly:


  Exam         			Date     	Time and Location       Material                  				Study material             							
  ------------ 			-------- 	-------------------- 	-----------------------   				-------------------------------------------------	
  Exam 1       			Feb 23	    in class, take home		Chapter 1	  							[study guide](review1.pdf)
  Exam 2       			Mar 30	    in class, take home		Chapter 2, 3.1 – 3.3			  		[study guide](review2.pdf)
  [Final](exam3.pdf)	May 3-7		take home				3.4 – 3.8, 5.1, 5.2, 5.5											




## Course plan ##

Our plan is to cover the textbook chapters 1-3, and parts of 4 and 5.  Below is 
a rough outline of what is to be covered week by week through the semester. Please check 
back regularly for precise details on what is covered, as well as postings of in-class 
worksheets and other activities.  Also note that the days of the week under Sections in
the table below provide links to class notes.

 
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Week              		Sections		   								In-class activities																Problems
  ------------------------ 	-----------			   							--------------------	     													------------------------------------------
  Jan 22 - Jan 26     		[Wednesday](01242018.pdf): 1.1 \\				[First day info](first-day.pdf), [Big picture](big-picture.pdf), 				1.1: 1, 2, 3, 6, 7, 11	\\
  							[Friday](01262018.pdf): 1.1						    [Exponential growth](exponential-growth.pdf) \\								1.1: 17,  18 \\
  																			[Logistic model](logistic-model.pdf), [Worksheet1.m](Worksheet1.m)				1.2: 1
  																																							
  Jan 29 - Feb 2	   		[Monday](01292018.pdf): 1.2 \\					[Separation of variables](separation.pdf), 										1.2: 6, 8, 9, 20, 30, 32, 39, 40 \\
  							[Wednesday](01312018.pdf): 1.3, 1.4 \\			    [(solutions)](separations.pdf) \\											1.3: 1, 5, 6, 11, 12, 13, 14, 15, 16 \\
  							[Friday](02022018.pdf): 1.4						[Slope fields](slope-fields.pdf), [Slopefield.m](Slopefield.m) \\ 				1.4: 2, 4, 6, 8, 11
  																			[Euler's method](euler.pdf), [euler.m](euler.m),								
  																				[example](euler-example.pdf) 

  Feb 5 - Feb 9   			[Monday](02052018.pdf): 1.5 \\					[Existence and uniqueness](existence-uniqueness.pdf) \\							1.5: 2, 5, 7, 9, 11 \\
  							[Wednesday](02072018.pdf): 1.6 \\				[Phase lines](phase-lines.pdf)													1.6: 1,5, 8, 13, 17, 20, 30, 34, 39 \\
  							[Friday](02092018.pdf): 1.7 																									1.7: 3, 5, 7, 17, 19	
								  
  Feb 12 - Feb 16   		[Monday](02122018.pdf): 1.7 \\					[Bifurcations](bifurcations.pdf) [(part 3)](bifurcations-worksheet.pdf) \\		\\
  							[Wednesday](02142018.pdf): 1.8 \\				[Linear equations](linear-equations.pdf)										1.8: 3, 5, 19, 20, 21 \\
  							[Friday](02162018.pdf): 1.9																										1.9: 10, 23
  
  Feb 19 - Feb 23     		[Monday](02192018.pdf): 2.1 \\					[Predator-prey systems](predator-prey-systems.pdf),								2.1: 1, 2, 4, 6, 7a, 8, 9, 10, 16a
  							Wednesday: review \\								[Matlab file 1](PhasePortraitParameterizedSolutions.m),
  							Friday: exam										[Matlab file 2](ExtendedPhaseSpace.m)
  																																																									
  Feb 26 - Mar 2    		[Monday](02262018.pdf): 2.2 \\					[Lotka-Volterra systems](lotka-volterra-systems.pdf),							2.2: 8, 11, 21 \\
  							[Wednesday](02282018.pdf): 2.3, 2.4 \\				[PhasePortrait2.m](PhasePortrait2.m) \\										2.3: 3, 7; 2.4: 1, 3, 7, 8, 11 \\
  							[Friday](03022018.pdf): 2.6						\\																				2.6: 2, 3, 4, 5
  																			[Existence and uniqueness for systems](eu-systems.pdf),							
  																				[NonautonomousExtended.m](NonautonomousExtended.m)
  
  Mar 5 - Mar 9   			[Monday](03052018.pdf): 2.7 \\					[SIR Model](sir-model.pdf) \\													2.7: 2, 7, 8 \\
  							Wednesday: 2.7 \\								[SIR part II](sir-part-ii.pdf), [SIRPhasePortrait.m](SIRPhasePortrait.m),		\\ 
  							[Friday](03092018.pdf): 2.8							[SIRparameterized.m](SIRparameterized.m) \\									2.8: 1, 5
  																			[LorenzMovie.m](LorenzMovie.m), [Chaotic water wheel][],
  																				[Steven Strogatz water wheel][],
  																				[Chapter 9 of Strogatz on Lorenz equations][] (posted if you're curious)
  
  Mar 12 - Mar 16   		spring break
  
  Mar 19 - Mar 23    		[Monday](03192018.pdf): 3.1 \\					[Linear algebra review](linear-algebra-review.pdf)								3.1: 6, 8, 10, 11, 17, 19, 26, 29 \\
  							[Wednesday](03212018.pdf): 3.2 \\					[(solutions)](linear-algebra-review-solutions.pdf) \\						3.2: 1, 2, 9, 14ab, 23 \\
  							[Friday](03232018.pdf): 3.3						[Eigenstuff review](eigenstuff-review.pdf)										3.3: 1, 2, 7, 15
  																				[(solutions)](eigenstuff-review-solutions.pdf)
  
  Mar 26 - Mar 30    		[Monday](03262018.pdf): 3.4 \\					[Complex eigenvalues](complex-eigenvalues.pdf) 									3.4: 2, 3, 4, 5, 9, 10, 11, 15, 23
  							Wednesday: review \\  								[(solutions)](complex-eigenvalues-sols.pdf) \\  
  							Friday: exam									[Real eigenvalues, review questions](real-eigenvalues.pdf)
  																				[(solutions)](real-eigenvalues-sols.pdf)
  
  Apr 2 - Apr 6   			[Monday](04022018.pdf): 3.5, 3.6 \\				[Second-order linear equations](second-order-eqs.pdf) \\  						3.5: 1, 2, 17, 18 \\
  							[Wednesday](04042018.pdf): 3.6, 3.8 \\			[Harmonic oscillators](harmonic-oscillators.pdf),								3.6: 13, 15, 16, 21, 23, 24 \\
  							[Friday](04062018.pdf): matrix exponential			[HarmonicOscillatorParameterized.m](HarmonicOscillatorParameterized.m)		3.5: 5, 6

  Apr 9 - Apr 13   	    	[Monday](04092018.pdf): generalized eigenvectors \\  [Generalized eigenvectors](generalized-eigenvectors.pdf) \\  				3.8: 7, 10 \\
  							[Wednesday][ge]: generalized eigenvectors \\  	[More on generalized eigenvectors](generalized-eigenvectors-ii.pdf)	  			\\
  							[Friday](04132018.pdf): 5.1 																									5.1: 1, 2, 3
  
  Apr 16 - Apr 20    		[Monday](04162018.pdf): 5.1 \\					[Linearization](linearization.pdf) \\											5.1: 5, 7
  							[Wednesday](04182018.pdf): 5.2 \\				[Nullclines](nullclines.pdf)
  							Friday: 5.2										[More nullclines practice](more-nullclines.pdf)
  
  Apr 23 - Apr 27     		[Monday](04232018.pdf): 5.5 \\					[3d linearization](3d-linearization.pdf), 
  							Wednesday: presentations \\							[PhasePortrait3DLinear.m](PhasePortrait3DLinear.m),
  							Friday: presentations								[PhasePortrait3D.m](PhasePortrait3D.m)
  
  Apr 30 - May 4			Monday: presentations	  
  
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Projects ##

We'll devote the last week of the semester to a mini-symposium of short group 
presentations.  Since the field of differential equations is rich with interesting 
examples and topics, more than we could cover in a single semester, each group of 3-4 
students will choose a topic/application that we might otherwise not have time for in 
class.  Here are some details:

-	[Presentation information and suggested topics](presentation-topics.pdf)
-	Upcoming dates:
	+	**Friday, April 6**: select two potential topics and turn in 1-2 paragraphs about 
		each.  Give a synopsis of each topic and why you might like to present on it.
	+	**Monday, April 9**: discuss groups/topics in class, finalize groups by Wednesday.
	+	**Monday, April 16**: submit 1-2 page (written or typed) summary of the topic (one 
		submission per group).
	+	**April 16 to 23**: meet with me with a draft of slides and/or plan for the talk.
	+	**April 25, 27, 30**: presentation days in class.
-	[Presentation rubric][]

---------------------------------------------------------------------------------------------------
Group members                             Topic                               Presentation day           Slides
------------------                        ---------------------------------   -----------------------    ------------
Ashley, Xiaoxue, Young, Ziyan             Hamiltonian systems				  Wednesday, April 25        [pdf](Hamiltonian-Systems.pdf)

Areeb, Faryal, Mirha                      Richardson arms race model		  Wednesday, April 25	     [pdf](Richardson-Arms-Race.pdf)


Alina, Audrey, Ching Ching, Nikkole       Discrete logistic model			  Friday, April 27			 [pdf](Discrete-Logistic-Model.pdf)

Claire H., Claire X., Celia               Improving Euler's method			  Friday, April 27           [pdf](Improvement-of-Euler.pdf)


Alexis, Helen, Isahelen, Yi               SIRE model						  Monday, April 30		     [pdf](SIRE.pdf), [Matlab files](SIRE-Matlab.zip)

Caledonia, Gargi, Michelle, Serena        Population models, PDEs             Monday, April 30           [pdf](Population-PDE.pdf)

Shirley, Xingtong, Yu                     SIR model with vital dynamics		  Monday, April 30			 [pdf](SIR-Vital-Dynamics.pdf)

---------------------------------------------------------------------------------------------------


#### LaTeX resources

LaTeX is a typesetting system that makes it easy to type math.  Here are some resources you might find useful
as you make presentation slides.

- [MacTeX][]/[TeXLive][].  An installation package for getting LaTeX running on your 
computer.  MacTeX is for macOS and TeXLive is for other operating systems.

- [LaTeXiT][].  This small program for macOS lets you quickly make a mathematical 
expression using LaTeX commands and export it as an image or pdf file that can be 
inserted into a PowerPoint presentation.  This is an alternative to making a full 
LaTeX document; it doesn't require much time investment aside from first installing 
MacTeX or TeXLive.  (LaTeXiT comes with a full MacTeX/TeXLive installation.)

- [quick reference][].  This file has a list of common commands.

- [LaTeX file template][].  This is a simple file that lets you get started making a full 
LaTeX document.  This is the [pdf][LaTeX file template pdf] file generated by the template.

- [Beamer template][].  If you'd like to use LaTeX to make presentation slides, there is 
a package called Beamer that is an alternative to systems like PowerPoint.  Here is a 
template for a Beamer presentation.  This is the [pdf][Beamer template pdf] file 
generated by the template.


## Getting help ##

Here's a few ways to get help:

-   **Office hours** (listed at the top of this page).  These are times I hold on my schedule 
	to be available for students.  I like to use these times as open drop in sessions 
	where there may be multiple students getting help simultaneously, but it's expected 
	that you come with specific questions about notes, homework problems, or the text that
	you have thought about already.  If you need to schedule a time to meet with me 
	one-on-one, either to discuss something private or because you can't make my office 
	hours, please send me an email with times that you are available to meet, and I will 
	find a time that works for both of us.
-	**Study groups**.  I strongly encourage you to form small groups and work together on problems.
	My advice though is to try problems by yourself before discussing them.  We have 
	several spaces on the fourth floor of Clapp where students can work together for 
	extended periods of time. 
	
## Resources ##

-	We will use Matlab to do computations that complement the analytical theory in our course.  Mount Holyoke provides
free licenses to install Matlab on your personal computers.  An [installation guide][] is posted, along with a beginner's tutorial.
	
[e-text]: http://www.mtholyoke.eblib.com/EBLWeb/patron/?target=patron&extendedid=P_1449975_0
[syllabus]: ./syllabus
[guidelines]: http://mtholyoke.edu/~tchumley/assets/pdf/homework-guidelines.pdf
[Gradescope]: https://gradescope.com
[short tutorial]: http://mtholyoke.edu/~tchumley/assets/pdf/gradescope.pdf
[guide]: http://gradescope-static-assets.s3-us-west-2.amazonaws.com/help/submitting_hw_guide.pdf
[general help guide]: https://gradescope.com/help#help-center-section-student-workflow
[installation guide]: http://guides.mtholyoke.edu/c.php?g=762365&p=5467440
[SCUDEM]: https://www.simiode.org/scudem/2018
[Chaotic water wheel]: https://www.youtube.com/watch?v=7A_rl-DAmUE
[Steven Strogatz water wheel]: https://www.youtube.com/watch?v=7iNCfNBEJHo
[Chapter 9 of Strogatz on Lorenz equations]: https://moodle.mtholyoke.edu/mod/resource/view.php?id=371346
[ge]: http://faculty.sfasu.edu/judsontw/ode/html/linear09.html
[Presentation rubric]: http://mtholyoke.edu/~tchumley/assets/pdf/presentation-rubric.pdf


[MacTeX]: http://tug.org/mactex/
[TeXLive]: https://www.tug.org/texlive/
[LaTeXiT]: https://www.chachatelier.fr/latexit/
[quick reference]: https://reu.dimacs.rutgers.edu/Symbols.pdf
[LaTeX file template]: http://www.mtholyoke.edu/~tchumley/latex-presentation/hello-world.tex
[LaTeX file template pdf]: http://www.mtholyoke.edu/~tchumley/latex-presentation/hello-world.pdf
[Beamer template]: http://www.mtholyoke.edu/~tchumley/latex-presentation/BeamerTemplate.tex
[Beamer template pdf]: http://www.mtholyoke.edu/~tchumley/latex-presentation/BeamerTemplate.pdf

