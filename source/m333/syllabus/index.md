---
title: Math 333 Syllabus
<#include nav-m333.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley      

---------

**Course**: Math 333, Differential Equations   

**Prerequisites**: Math 211, Linear algebra, or equivalent

**Textbook**: *Differential Equations, 4th edition*, by Paul Blanchard, Robert L. Devaney,‎ Glen R. Hall, ISBN: 9781133109037;

**Learning goals**: This course introduces differential equations and analytical, numerical, and graphical techniques for the analysis of their solutions. First and second order differential equations and linear systems are studied. Applications are selected from areas such as biology, chemistry, economics, ecology, and physics. Computers will be used extensively to calculate and visualize results.  The core of the course is chapters 1-3. We will also cover parts of Chapters 4 and 5.

**Office hours**: <#include officehours-s18.txt>      
	
These are times I hold on my schedule to be available for students.  I like to use these times as open drop in sessions where there may be multiple students getting help simultaneously, but it's expected that you come with specific questions about notes, homework problems, or the text that you have thought about already.  If you need to schedule a time to meet with me one-on-one, either to discuss something private or because you can't make my office hours, please send me an email with times that you are available to meet, and I will find a time that works for both of us.

**Homework**: There will be weekly homework assignments comprising both computational and theoretical, writing oriented problems.

**Quizzes**: There will be a few in-class quizzes during the semester to keep everyone up to date and studying the material between exams.  The dates for these will be announced in advance.

**Exams**: There will be two midterm exams and a self-scheduled final. The midterms are tentatively scheduled for February 23 and March 30.

**Project**: We'll devote the last week of the semester to a mini-symposium of short  group presentations.  Since the field of differential equations is rich with interesting examples and topics, more than we could cover in a single semester, each group of 2-3 students will choose a topic/application that we might otherwise not have time for in class.  Topics might be taken from a section of the textbook or a scientific paper; I'll have a list of suggestions, but we won't have to restrict ourselves to my suggestions.  I envision presentations might consist of a short introduction, a discussion of what makes the topic interesting or useful, and a glimpse at some technical details (perhaps via simulation).  However, this is just a suggestion and the overall format is up to the group.    Presentations might be a blackboard chalk talk, or with slides; a numerical computation component could very useful in a lot of the presentations.  More details will be discussed in class later in the semester.

**Attendance and Participation**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class please prepare by doing any assigned reading and suggested problems. Please expect to talk about math in small groups as well as in class discussions. Other classroom activities may involve worksheets, computer explorations, and informal presentations at the board.

**Late work, makeups**:  In general unexcused late work is not accepted for full credit and exams must be taken on time.  In special circumstances (eg. bad illness, unexpected family issues) I will accept late work or allow for a make-up exam up to a few days late.  I won't be able to accept late work after solutions are posted.  Please get in touch as soon as possible if something comes up.

**Grading**: Grades will be assigned based on homework and exams according to the following weighting:

- Homework: 15%
- Project: 10%
- Quizzes: 10%
- Exam I: 20%
- Exam II: 20%
- Final: 25%

Overall letter grades will be based on a scale no stricter than the usual:

- 92-100:			A
- 90-92:			A-
- 87-90				B+
- 82-87:			B
- 80-82:			B-
- 77-80:			C+
- 72-77:			C
- 70-72:			C-
- 67-70:			D+
- 62-67:			D
- 60-62:			D-
- 0-60:				F

**Academic integrity**: It is very important for you to follow the Honor Code in all of your work for this course. Collaboration on homework assignments is encouraged. However, it is important that you only write what you understand, and that it is in your own words. If you have any questions about what constitutes an Honor Code violation in this class please ask your instructor. Honor Code violations will be brought to the dean. The penalty for violating the Honor Code on any assignment, quiz or exam will be a score of 0 for it. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please contact AccessAbility Services, located in Wilder Hall B4, at (413) 538-2634 or accessability-services@mtholyoke.edu. If you are eligible, they will give you an accommodation letter which you should bring to me as soon as possible.

