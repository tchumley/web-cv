---
title: Math 211 Syllabus
<#include nav-m211.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley   

---------

**Course**: Math 211, Linear Algebra

**Prerequisites**: Math 102, Calculus II, or equivalent

**Textbook**: *Linear Algebra with Applications*, 5th Edition by Otto Bretscher, ISBN: 9780321796974

**Learning goals**: During the semester you will be learning how to

-	solve systems of linear equations algorithmically
- 	describe the solution set of a system of linear equations using the geometry of Euclidean space and theoretical results
-	recognize abstract vector spaces in natural contexts and apply the theory of linear algebra to analyze these spaces
-	use geometry and algebra to understand linear transformations and connect the analysis of these transformations to the analysis of a system of linear equations
-	understand uses of orthonormal bases, eigenvalues, and eigenvectors and perform and explain algorithms for computing them
-	communicate complex mathematical ideas precisely (read and write rigorous
mathematical proofs, develop strong mathematical and logical arguments, and articulate clear questions)

**Office hours**: Mondays 4:15-5:15, Tuesdays 2:30-5:00, Wednesdays 1:00-2:00 & 4:15-5:15, Thursdays 2:30-3:30
	
These are times I hold on my schedule to be available for students.  I like to use these 
times as open drop in sessions where there may be multiple students getting help 
simultaneously, but it's expected that you come with specific questions about notes, 
homework problems, or the text that you have thought about already.  If you need to 
schedule a time to meet with me one-on-one, either to discuss something private or because
you can't make my office hours, please send me an email with times that you are available 
to meet, and I will find a time that works for both of us.

**Homework**: There will be weekly homework assignments (except on exam weeks) comprising both computational and theoretical, writing-oriented problems.  Your lowest homework score will not be counted toward your grade.

**Quizzes**: There will be weekly 15 minute quizzes (except on exam weeks) with questions similar to suggested textbook problems or which ask you to state a definition.  Your two lowest quiz scores will not be counted toward your grade.

**Exams**: There will be two midterm exams and a self-scheduled final. The midterms are tentatively scheduled for February 23rd and April 6th. 

**Attendance**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class please prepare by doing any assigned reading and suggested problems. Please expect to talk about math in small groups as well as in class discussions. Other classroom activities may involve worksheets, computer explorations, and informal presentations at the board.

**Late work, makeups**:  No late homework due to unexcused absence will be accepted.  Excused absences are granted only in exceptional circumstances and may require written documentation.

**Grading**: Grades will be assigned based on homework, quizzes, and exams according to the following weighting:

- Homework: 20%
- Quizzes: 15%
- Exam I: 20%
- Exam II: 20%
- Final: 25%

Overall letter grades will be based on a scale no stricter than:

- 90-100:			A
- 80-89:			B
- 70-79:			C
- 60-69:			D
- 0-59:				F

**Academic integrity**: I expect everyone involved in this course (students, graders, myself) to respect the letter and spirit of the Honor Code. You are encouraged to work collaboratively on homework, but you should each write up your own solutions in your own words, and you should always acknowledge your sources if the ideas you are presenting are not wholly your own. It’s fine to cite a classmate, an evening helper, a website, or a book as your source of inspiration, but it’s important that you give credit where it is due and never pass off someone else’s insight as your own. For this class, the citation and acknowledgement can be quite informal—a simple note on the top page of your homework citing any students you worked closely with or other sources you consulted is fine. If you’re not sure how this applies in a particular instance, ask me. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please contact AccessAbility Services, located in Wilder Hall B4, at (413) 538-2634 or accessability-services@mtholyoke.edu. If you are eligible, they will give you an accommodation letter which you should bring to me as soon as possible.


