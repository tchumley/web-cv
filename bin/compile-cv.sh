#!/bin/bash

GIT_ROOT=$(git rev-parse --show-toplevel)
INCLUDE_PATH=$GIT_ROOT/include
METADATA=$INCLUDE_PATH/metadata.yaml

changes=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | grep -e source-cv/ -e bibliography.bib)

if [ -n "$changes" ]
then
		$GIT_ROOT/bin/bib-html.sh $INCLUDE_PATH/bibliography.bib > $INCLUDE_PATH/bib-include.html
		$GIT_ROOT/bin/bib-markdown.sh $INCLUDE_PATH/bibliography.bib > $INCLUDE_PATH/bib-include-no-links.md

		gpp -x -H -I$INCLUDE_PATH $GIT_ROOT/source-cv/cv.md | \
		pandoc -f markdown -t latex | \
		cat $GIT_ROOT/source-cv/before.tex - $GIT_ROOT/source-cv/after.tex > $GIT_ROOT/source-cv/cv.tex
		latexmk -pdf -output-directory=$GIT_ROOT/source/assets/pdf -quiet $GIT_ROOT/source-cv/cv.tex
		latexmk -pdf -output-directory=$GIT_ROOT/source/assets/pdf -c $GIT_ROOT/source-cv/cv.tex 

		cat $GIT_ROOT/source/index.md | gpp -x -H -I$INCLUDE_PATH | \
		cat - $METADATA | \
		pandoc -s -f markdown+link_attributes+smart+multiline_tables+escaped_line_breaks --toc -t html \
		--template=$INCLUDE_PATH/bootstrap-template.html --mathjax -o $GIT_ROOT/source/index.html 
else
		echo "No changed cv files"
fi