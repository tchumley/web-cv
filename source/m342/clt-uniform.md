---
title: Math 342
<#include nav-m342.yml>
---

#### Independent sums of uniform random variables


![](./clt-images/uniform_1.png){width=400px class="img-responsive"}

![](./clt-images/uniform_2.png){width=400px class="img-responsive"}

![](./clt-images/uniform_3.png){width=400px class="img-responsive"}

![](./clt-images/uniform_4.png){width=400px class="img-responsive"}

![](./clt-images/uniform_5.png){width=400px class="img-responsive"}

![](./clt-images/uniform_6.png){width=400px class="img-responsive"}

![](./clt-images/uniform_7.png){width=400px class="img-responsive"}

![](./clt-images/uniform_8.png){width=400px class="img-responsive"}

![](./clt-images/uniform_9.png){width=400px class="img-responsive"}

![](./clt-images/uniform_10.png){width=400px class="img-responsive"}


