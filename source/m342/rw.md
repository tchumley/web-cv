---
title: Math 342
<#include nav-m342.yml>
---

#### Random walks on the integer lattice 

The plots below each simulate a sample path of a random walk.  A random walk is a random process
that works as follows.  You have a particle that starts at $0$ on the real line and at each time
step, the particle moves forward or backwards to one of the two neighboring integers.  At time $n = 1$
the particle is thus either at $1$ (ie. it moved forward) or $-1$ (ie. it moved backward) with probability
$p$ and $1-p$ repsectively.

We can express the movement at each time step $k$ via a random variable given by

$$ X_k = \\left\\{\\begin{array}{rl} 1 & \\mbox{with probability  } p \\\\ 
-1 & \\mbox{with probability } 1-p \\end{array}\\right. $$

and the location of the walker at time $n$ is then given by

$$ S_n = \\sum_{k=1}^n X_k $$

It follows by the **Strong Law of Large Numbers** that

$$ \\frac{S_n}{n} \\to \\mu = E[X_k] = 2p-1$$

as $n \\to \\infty$ with probability $1$.  Interpreted another way, it says

$$ S_n \\approx n\\mu $$

for large values of $n$, which means $S_n$ grows linearly at rate $\\mu$.  The **Central Limit Theorem**
offers a refinement of this idea.  It tells us that

$$ \\frac{S_n-n\\mu}{\\sqrt{n}} \\approx Y $$

where $Y$ is a standard normal.  Interpreted like we did with SLLN, this says

$$ S_n \\approx n\\mu + \\sqrt{n}Y, $$

which we can take to mean that $S_n$ behaves like $n\\mu$ but there are some random fluctuations of order $\\sqrt{n}$, and
they are normally distributed.

The pictures below show three simulations for three different values of $p$, which is what
we called the forward step probability of the random walk.  In the first picture, where $p = 1/2$
the random walk moves forward and backward with equal probability at each time step and so its
path is linear with slope $0$.  In the second and third pictures, the walk has a bias (forward in the second picture,
backward in the second picture) and the paths fluctuate around the line $y = \\mu x$.

Code for the pictures is posted [here](Simulation-of-RW.R).


![](./rw-symmetric.png){width=400px class="img-responsive"}

![](./rw-forward.png){width=400px class="img-responsive"}

![](./rw-backward.png){width=400px class="img-responsive"}



