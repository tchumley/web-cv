---
title: Math 333
footer: Last modified <#exec date>
<#include nav-m333.yml>
---

### Groups as of Monday, April 9

In class on Monday, we began forming groups.

-------------------------------------------------------------------------------
Group members                             Topic                               
-------------------------                 ---------------------------------   
Alexis, Helen, Isahelen, Yi               SIRE model

Caledonia, Gargi, Michelle, Serena        Population models, PDEs                        

Areeb, Faryal, Mirha                      Richardson arms race model

Ashley, Xiaoxue, Young, Ziyan             Hamiltonian systems

Shirley, Xingtong, Yu                     SIR model with vital dynamics

Claire H., Claire X., Celia               Improvements to Euler's method

Alina, Audrey, Ching Ching, Nikkole       Discrete logistic model

--------------------------------------------------------------------------------


There are time slots available for 8 presentations, so 2 more topics can be chosen.  Also, if a group has fewer than 4 members you can still join.

Not yet in a group:

-	Julie


### Initial interests

The following topics were written about as part of the 2 synopses writing assignment.

#### Differential equations in neuroscience

-	Nikkole Spencer

#### Differential equations and sound waves

-	Nikkole Spencer

#### Discrete logistic model

-	Mirha Khan
-	Alina Smithe

#### Error in Euler's method

-	Celia Xu

#### Forced harmonic oscillators

-	Ashley Cavanagh
-	Xiaoxue Gao
-	Claire Han
-	Areeb Khichi
-	Ziyan Pei
-	Alina Smithe
-	Young Yang

#### Hamiltonian systems

-	Ashley Cavanagh
-	Xiaoxue Gao
-	Ziyan Pei
-	Helen Wang
-	Young Yang

#### Improvements to Euler's method

-	Claire Xie
-	Celia Xu

#### Mathematical model of cancer and response to therapy

-	Isahelen Chen
-	Audrey Fahey
-	Xingtong Shen
-	Helen Wang
-	Yi Zhou

#### Nicholson Bailey model

-	Faryal Usman

#### Population dynamics with spatial dependence; PDEs

-	Yu Hu
-	Gargi Mishra
-	Serena Wang
-	Caledonia Wilson

#### Richardson arms race model

-	Faryal Usman

#### SIR model with vital dynamics

-	Yu Hu
-	Serena Wang
-	Shirley Xu

#### SIRE model and HIV epidemic

-	Isahelen Chen
-	Audrey Fahey
-	Alexis Helgeson
-	Ching Ching Huang
-	Shirley Xu
-	Yi Zhou

#### Solving differential equations with power series

-	Claire Han

#### Tacoma Narrows Bridge

-	Alexis Helgeson
-	Ching Ching Huang
-	Mirha Khan
-	Areeb Khichi
-	Gargi Mishra
-	Xingtong Shen
-	Caledonia Wilson
-	Claire Xie

