---
title: Math 342 Syllabus
<#include nav-m342.yml>
---

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 413-538-2299  
**e-mail**: tchumley      

---------

**Course**: Math 342, Probability   

**Prerequisites**: Math 203, Multivariable Calculus, or equivalent 

**Textbook**: *Probability with Applications and R*, by Robert P. Dobrow

**About the course**: Our course is a first introduction to probability theory. I think most everyone has had a little experience with counting and probability, either through a past statistics or science class, or in high school, or both. Our class tries to take that little bit of past intuition, and build a full theory out of it in order to understand models of real world phenomena that have an element of randomness. What this means, at its core, is that we'll study a class of mathematical objects called random variables. Our class is a little bit about modeling (ie. trying to understand what is the correct random variable that captures a given real world scenario), but a lot more about trying to analyze given models. That means using tools from discrete math, algebra, and calculus to pick apart how random variables work. 


**About the course during the pandemic**: Here are some things about the course
that are bit more specific to the Spring 2021 semester.

-	We'll have synchronous meetings on Zoom every day.
	I expect everyone to attend each meeting, but I'll be understanding of
	occasional absences and will post recordings of class on Moodle.
	I'll do my best to insure your privacy with these recordings,
	and I'll ask you not to share them outside our class.
-	There will be a little pre- and post-class work, involving some combination
	of reading and problems. My goal will be to give you some
	structure to help you through the compressed schedule, 
	but not overwhelm you. Nevertheless, you should expect to spend about 20-24
	hours per week on the class.
-	You're all my top priority this spring!
	Let's do our best to support each other.

**Learning goals**: During the semester our goal is to: 

- understand and use axioms of probability to develop probabilistic models for real world phenomena, as well and derive and prove probabilistic theorems and formulas.
- translate models and problems described in words to into mathematically precise statements.
- become familiar classical discrete and continuous probability distributions and the phenomena they model.
- learn how to use conditioning to simplify the analysis of complicated models.
- have facility manipulating probability mass functions, densities, expectations, and the relationships among them.
- understand the power of probabilistic limit theorems in inference and approximation, and be able to use them when appropriate.
- become familiar with the use of R and RStudio to estimate probabilistic quantities and verify theoretical results

**Attendance**: You are expected to come to every class ready to do mathematics. This means that you should bring paper, pens, pencils, and other equipment that you may need. Before each class please prepare by doing any assigned reading and suggested problems. Please expect to talk about math in small groups as well as in class discussions. Activities may involve worksheets and informal discussions.

**Participation**: Part of your grade will depend on participation and there will be a number of opportunities to participate (eg. asking and answering questions in class, coming to office hours, and participating on Piazza).

**Homework**: There will be weekly homework assignments.

**Quiz**: There be (mostly) weekly quizzes.

**Exams**: There will be two midterm exams and a final exam.

**Late work, makeups**:  In general, unexcused late work will not be accepted for full credit (typically at most 50% credit if it's more than a week late) and exams must be taken on time.  However, I try to be generous about excusing lateness.  If you get in touch as soon as possible when something comes up, ask for an extension, and let me know how much extra time you need, then I can generally accept things for full credit.

**Getting help**: Here are some of the resources that will be available:

-	**Office hours**: These times (which will be posted near the top of the  class web page) are open drop in sessions where there may be multiple students getting help.  It's expected that you come with specific questions about notes, homework problems, or the text that you have thought about already.  If you need to schedule a time to meet with me one-on-one, either to discuss something private or because you can't make my office hours, please send me an email with times that you are available to meet, and I will find a time that works for both of us.
-	**Study groups**: Other students in the class are a wonderful resource.  I want our class to feel like a community of people working together.  Even though our class won't have a TA or evening help, I will work to find ways for you to feel supported both by me and each other.

**Grading**: Grades will be assigned based on homework and exams according to the following weighting:

- Participation: 10%
- Quizzes: 15%
- Homework: 15%
- Midterm 1: 20%
- Midterm 2: 20%
- Final: 20%

Overall letter grades will be based on a scale no stricter than the usual:

- 93-100:			A
- 90-93:			A-
- 88-90				B+
- 83-88:			B
- 80-83:			B-
- 78-80:			C+
- 73-78:			C
- 70-73:			C-
- 68-70:			D+
- 63-67:			D
- 60-63:			D-
- 0-60:				F

**Academic integrity**: It is very important for you to follow the Honor Code in all of your work for this course. Collaboration on homework assignments is encouraged. However, it is important that you only write what you understand, and that it is in your own words. My first instinct is always to trust you, but I cannot give credit for plagiarized work and might have to refer such issues to the deans. If you have any questions about whether something is an Honor Code violation, please ask me. 

**Students with disabilities**: If you have a disability and would like to request accommodations, please get in touch with me. We'll work together with AccessAbility Services to make sure the class is accessible and equitable.

