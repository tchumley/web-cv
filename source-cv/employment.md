Employment {.unnumbered}
==========

-	Assistant Professor, Mount Holyoke College, Department of Mathematics \& Statistics, 2016 – present
-   NSF Alliance Postdoctoral Research Fellow, Iowa State University, Department of Mathematics, 2013 – 2016
-	Graduate teaching assistant and instructor, Washington University, Department of Mathematics, 2008 – 2013