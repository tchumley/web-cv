---
title: Math 342
footer: Last modified <#exec date>
<#include nav-m342.yml>
---

<img style="float: right;" src="/~tchumley/assets/img/m342.png" width=350px class="img-responsive">

**Instructor**: [Tim Chumley](/~tchumley)  
**Office**: Clapp 404b  
**Phone**: 538-2299  
**e-mail**: tchumley  
**Office Hours**: <#include officehours-s21m2.txt> 

**Textbook**: *Probability with Applications and R*, by Robert P. Dobrow, ISBN: 978-1118241257; \\
available as a free [e-text][] and
available on library reserve under QA276.45.R3 D63 2014 



---------

## Announcements ##

Announcements will be posted on the [Course Announcements Moodle forum][] throughout the semester.
Recordings of class lectures will also be posted on Moodle, 
but essentially all other materials will be posted on this page.


## Syllabus ##

Check the [syllabus][] for all the important class policies (grades, attendance, etc.).


## Homework ##

There will be weekly homework assignments throughout the semester to be turned in.
Please read these [guidelines][] for writing in our class.

- **Group work**. Some of your homework will be done in teams.
	*	These problems will be due on **Fridays**.
	*	You should type solutions in LaTeX and submit only one copy on behalf of the whole team.
- **Solo work**. A selection of problems will be assigned to be written up individually and turned in each week.
	*   These problems will also be due **Fridays**.
	*	You may work with others but the writing should be done on your own.
	*	Feel free to write solutions by hand or type them in LaTeX.
- **Gradescope**. Homework will be turned in through [Gradescope][].
	*	Please add yourself to the Gradescope course using Entry Code **JB42GD**.
	*	Gradescope has made a [short tutorial][] on submitting homework.
- **Collaboration**. I want you to work together on the written homework, of course on the
	group assignments but even on the solo assignments. 
	The process of explaining your ideas to one another really helps in learning math. 
	However, you must write up the solo assignments on your own and avoid copying others' work directly. 
	Also, please only write what you understand so that I know where to help, and
	please avoid using online forums like Math StackExchange, solutions manuals, or similar resources.
	A huge part of learning in this class comes from figuring out how to get unstuck
	by spending time thinking on your own or talking to me and classmates; these other
	resources can end up being counter-productive in the long term.
- **Rewrites**. Homework is for practice, and you are not expected to write perfect answers from the start! 
	*	Your revisions will be due on **Fridays**. Please resubmit (only the problems you're revising) on Gradescope.
	*	For some problems it's difficult to allow redo's fairly, 
		so on occasion I might not allow a problem to be redone.
	
	
  
Assignment							Due			
--------------------				-----		
[Homework 0](hw0.pdf)				March 22
[Homework 1](hw1.pdf)				March 26
[Homework 2](hw2.pdf)				April 2
[Homework 3](hw3.pdf)				April 9
[Homework 4](hw4.pdf)				April 16
[Homework 5](hw5.pdf)				April 23
[Homework 6](hw6.pdf)				April 30
[Homework 7](hw7.pdf)				May 7


## Quizzes ##

There will be (mostly) weekly quizzes that will be given on Tuesdays.
They will be posted on Gradescope and you'll have until Wednesday before class to complete them.
The purpose of these is to check in to see that you're comfortable with fundamental material
like definitions, theorems, and important examples and proofs.
Problems will always be related to the previous week's homework and class topics.

Quiz         	 			Date posted     	Material
------------ 				-------- 			------------
[Quiz 1](quiz1.pdf)			March 30			Homework 1
[Quiz 2](quiz2.pdf)			April 6				Homework 2
[Quiz 3](quiz3.pdf)			April 21			Homeworks 3, 4
[Quiz 4](quiz4.pdf)			May 4				Homeworks 5, 6
  

## Exams ##

There will be two midterms and a final.  The dates for the mid-terms are subject to change slightly.


Exam         	 				Due Date    Format			        Material                 
------------ 					-------- 	-------------------- 	-----------------------  
[Exam 1](exam1.pdf)				April 13	take-home		        Homeworks 1, 2
[Exam 2](exam2.pdf)				April 27	take-home				Homeworks 3, 4			
[Exam 3](exam3.pdf)				May 11		take-home				Homeworks 5, 6, 7			



## Course plan ##

Our plan is to cover most of chapters 1-4, 6, and parts of chapters 5, 7, 8, and 9 in the textbook.
Below is a rough outline of what is to be covered week by week through the semester. 
Please check back regularly for precise details on what is covered, 
as well as postings of class materials like lecture notes.

<#include open-accordion.txt>

<#define WEEK|1>
<#define DAYS|March 15 – March 19>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Introduction

-----------

##### Thursday

* **Topic**: Sections 1.1 - 1.4: Introduction, set theory, and probability functions. 
	We discuss some introductory definitions.
* **Class materials**: [Lecture notes](day01.pdf), [Basics of sets and probability functions](intro.pdf)
* **After class**: Read Section 1.6 up through Example 1.17.

##### Friday

* **Topic**: Sections 1.5, 1.6: Counting. 
	We begin our discussion of counting and computing probabilities involving equally likely outcomes.
* **Class materials**: [Lecture notes](day02.pdf)
* **After class**: In Section 1.4, read the derivation of Property 3. In Section 1.7, read up through the proof of Proposition 1.2. Ask a question
	on Piazza about the reading so far.


><#include open-accordion-panel.txt>

<#define WEEK|2>
<#define DAYS|March 22 – March 26>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Conditional probability, independence

-----------

##### Monday

* **Topic**: Sections 1.7 - 1.8: Problem solving techniques and random variables. 
	We talk about using complements versus the inclusion-exclusion principle
	and introduce random variables.
* **Class materials**: [Lecture notes](day03.pdf)
* **After class**: Read Section 1.10, but skip the divisibility example. Download and skim through the [Intro to R markdown](intro-to-R.Rmd) file.

##### Tuesday

* **Topic**: Community day, no class.

##### Wednesday

* **Topic**: Section 1.10: Introduction to R and simulation. We talk about using R to do simulations and verify answers.
* **Class materials**: [Lecture notes](day04.pdf), [Intro to R markdown](intro-to-R.Rmd), [Intro to R pdf](intro-to-R.pdf)
* **After class**: Read Examples 2.1, 2.4, 2.7, and 2.8 in Chapter 2.

#### Thursday

* **Topic**: Sections 2.1, 2.3: Conditional probability. We introduce the notion of conditional probability and use it to study the probability of sequential events.
* **Class materials**: [Lecture notes](day05.pdf), [Conditional probability](conditional-probability.pdf)
* **After class**: Read Section 2.4 up through Example 2.12. Read Examples 2.16 and 2.17 in Section 2.5.

##### Friday

* **Topic**: Sections 2.4, 2.5: Law of total probability and Bayes' rule. We discuss finding probabilities by decomposing the sample space, as well as finding inverted conditional probabilities.
* **Class materials**: [Lecture notes](day06.pdf), [Law of total probability, Bayes' rule](bayes-rule.pdf)
* **After class**: Read the introductory discussion of Section 3.4 and Examples 3.12, 3.15, 3.18, and 3.19.






><#include open-accordion-panel.txt>

<#define WEEK|3>
<#define DAYS|March 29 – April 2>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Discrete random variables

-----------


##### Monday

* **Topic**: Section 3.4: Counting subsets. We expand our repertoire of counting examples and consider problems where order does not play a role.
* **Class materials**: [Lecture notes](day07.pdf), [Counting subsets](counting-subsets.pdf)
* **After class**: Read the start of Section 3.1 (the definition of independence and the first example),
	Section 3.3, and Section 3.5 up through Example 3.21.

##### Tuesday

* **Topic**: Section 3.5: Binomial distribution. We discuss examples that consist of a fixed number of independent trials of a success/failure experiment.
* **Class materials**: [Lecture notes](day08.pdf), [Binomial distribution](binomial.pdf)
* **After class**: Work on [Homework 2](hw2.pdf) in preparation for homework group time tomorrow.

##### Wednesday

* **Topic**: Homework group time.
* **After class**: Read Section 3.7 up through Example 3.30.

##### Thursday

* **Topic**: Section 3.7: Poisson distribution. We discuss a deeply important distribution used for counting occurrences over a fixed time interval.
* **Class materials**: [Lecture notes](day09.pdf), [Poisson distribution](poisson.pdf)
* **After class**: Read Section 5.1 up through Example 5.3 and subsection 5.3.1. Skip the examples that discuss expected value and variance for now.

##### Friday

* **Topic**: Section 5.1: Geometric distribution. We discuss another important distribution that counts trials until first success.
* **Class materials**: [Lecture notes](day10.pdf)
* **After class**: Read Section 4.1 and Examples 4.6 and 4.7 in Section 4.2.



><#include open-accordion-panel.txt>

<#define WEEK|4>
<#define DAYS|April 5 – April 9>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Discrete random variables

-----------

##### Monday

* **Topic**: Sections 4.1, 4.2: Expected value. We introduce the idea of the mean (or average or expected value) of a random variable.
* **Class materials**: [Lecture notes](day11.pdf)
* **After class**: Read Section 4.3, focusing on Examples 4.10, 4.11, and 4.12 in particular.


##### Tuesday

* **Topic**: Sections 4.3: Joint distributions. We begin to study multivariate distributions, 
	which allow us to describe the behavior of multiple random variables at once.
* **Class materials**: [Lecture notes](day12.pdf), [Joint distributions](joint-pmf.pdf)
* **After class**: Work on [Homework 3](hw3.pdf) in preparation for homework group time tomorrow.


##### Wednesday

* **Topic**: Homework group time.
* **After class**: Read Section 4.4, focusing particularly on Examples 4.14, 4.15, and 4.17.

##### Thursday

* **Topic**: Section 4.4: Independence. We discuss sums of random variables, independence, and their relationship.
* **Class materials**: [Lecture notes](day13.pdf), [Expectation, sums, and independence](expectation-sums.pdf)
* **After class**: Read Section 4.6.

##### Friday

* **Topic**: Section 4.6: Variance. We derive useful algebraic properties involving variance and sums of random variables.
* **Class materials**: [Lecture notes](day14.pdf)
* **After class**: Work on exam!



><#include open-accordion-panel.txt>

<#define WEEK|5>
<#define DAYS|April 12 – April 16>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Continuous random variables

-----------

##### Monday

* **Topic**: Exam work day, no class.
* **After class**: Read Section 4.8, focusing on Examples 4.35, 4.37, 4.41, and 4.42.

##### Tuesday

* **Topic**: Section 4.8: Conditional distribution. When two random variables are jointly distributed, 
we consider the conditional distribution of one given the other in many modeling situations. We discuss this, as well as conditional expectation.
* **Class materials**: [Lecture notes](day15.pdf), [Conditional distributions and expectation](conditional-expectation-discrete.pdf)
* **After class**: Read the introduction to Chapter 6 and read Section 6.1 up through Example 6.2.

##### Wednesday

* **Topic**: Section 6.1: Probability density function. We begin our study of a class random variables whose range is uncountably infinite.
We refer to these as continuous random variables and discuss how to compute probabilities by integrating functions we call the probability density functions.
* **Class materials**: [Lecture notes](day16.pdf), [Basic integration review](integration.pdf), [More review problems](https://web.williams.edu/Mathematics/sjmiller/public_html/238/handouts/CalcReviewProblems.pdf)
* **After class**: Read Section 6.2.
	Also, take a look at the basic integration review linked above. 
	If you're comfortable with those, that's great, and the integrals won't get any harder.
	There is a more in depth review in the second link, provided by someone at Williams College, with some more practice if you need.


##### Thursday

* **Topic**: Section 6.2: Cumulative distribution function. We discuss the cdf of a (continuous) random variable, which is closely related to the probability density function
	by way of the Fundamental Theorem of Calculus.
* **Class materials**: [Lecture notes](day17.pdf), [PDF and CDF](pdf-cdf.pdf)
* **After class**: Read about expectation in Section 6.4, 
	paying closest attention to similarity with the definition in the discrete case and the three examples.


##### Friday

* **Topic**: Section 6.4: Expectation and variance. We discuss how to compute the expectation and variance of a continuous random variable.
	These values are very similar to their discrete counterparts, but we use integrals instead of sums.
* **Class materials**: [Lecture notes](day18.pdf)
* **After class**: Read Section 6.5, and think about how the exponential distribution might be related to the Poisson and geometric distributions.
	Focus your reading on Examples 6.12, 6.13, and the memoryless property discussion.



><#include open-accordion-panel.txt>

<#define WEEK|6>
<#define DAYS|April 19 – April 23>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Continuous random variables

-----------

##### Monday

* **Topic**: Midterm break, no class.


##### Tuesday

* **Topic**: Section 6.5: Exponential distribution. 
	We discuss a deeply important distribution that models the time between occurrences in a random process called a Poisson process.
	We can think of it as the continuous version of the geometric distribution.
* **Class materials**: [Lecture notes](day19.pdf), [Exponential distribution](exp-dist.pdf)
* **After class**: Work on [Homework 5](hw5.pdf) in preparation for homework group time tomorrow.

##### Wednesday

* **Topic**: Homework group time.
* **After class**: Begin reading Section 6.7 and refresh yourself on double integrals by reading these [refresher notes](./f18/10262018.pdf).

##### Thursday

* **Topic**: Section 6.7: Joint densities. We review double integrals from multivariable calculus in preparation for 
	discussing joint distributions of continuous random variables.
* **Class materials**: [Lecture notes](day20.pdf), [Refresher notes](./f18/10262018.pdf), [Iterated integrals](iterated-integrals.pdf)
* **After class**: Read Examples 6.19, 6.21, and 6.22 from Section 6.7.

##### Friday

* **Topic**: Section 6.7: Joint densities. We continue our work on joint distributions of continuous random variables.
* **Class materials**: [Lecture notes](day21.pdf)
* **After class**: Read Examples 6.23, 6.24, and 6.25.



><#include open-accordion-panel.txt>

<#define WEEK|7>
<#define DAYS|April 26 – April 30>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Conditional distributions

-----------

##### Monday

* **Topic**: Exam work day, no class.

##### Tuesday

* **Topic**: Sections 6.7, 6.8: Joint densities, independence. We continue our work on joint distributions of continuous random variables
	by focusing on marginal densities and independence.
* **Class materials**: [Lecture notes](day22.pdf), [Joint densities](joint-densities.pdf)
* **After class**: Read section 8.1, focusing on Examples 8.1, 8.4, and 8.5


##### Wednesday

* **Topic**: Section 8.1: Conditional densities. We discuss finding conditional probabilities involving two continuous random variables.
* **Class materials**: [Lecture notes](day23.pdf)
* **After class**: Read Section 8.2, focusing on Examples 8.6 and 8.7.

##### Thursday

* **Topic**: Section 8.1: Conditional densities, continued. We discuss some more examples involving conditional probabilities with continuous random variables.
* **Class materials**: [Lecture notes](day24.pdf), [Conditional densities](conditional-density.pdf)
* **After class**: Work on your homework for tomorrow.

##### Friday

* **Topic**: Section 8.2: Mixed conditionals. We discuss finding conditional probabilities involving one continuous and one discrete random variable.
* **Class materials**: [Lecture notes](day25.pdf)
* **After class**: Read Section 8.3 up through Example 8.11.



><#include open-accordion-panel.txt>

<#define WEEK|8>
<#define DAYS|May 3 – May 7>
<#define COLLAPSED|false>
<#define PANEL_CONTENTS |

#### Wrap up

-----------

##### Monday

* **Topic**: Section 8.3: Conditional expectation. We continue our discussion from Section 4.8 on conditional expectation, now considering the continuous case.
* **Class materials**: [Lecture notes](day26.pdf)
* **After class**: Read Examples 8.11 and 8.12 in Section 8.3, and Subsection 8.3.2.

##### Tuesday

* **Topic**: Section 8.3: Conditional expectation, continued. We finish our discussion on conditional expectation by discussion the random
	variable version of conditional expectation and discussing how the law of total expectation leads to a continuous version of the law of total probability.
* **Class materials**: [Lectures notes](day27.pdf), [Conditional expectation](conditional-expectation.pdf)
* **After class**: Work on [Homework 7](hw7.pdf) in preparation for homework group time tomorrow.

##### Wednesday

* **Topic**: Homework group time.
* **After class**: Read Section 9.5 up through Example 9.15.

##### Thursday

* **Topic**: Section 9.5: Moment generating functions. We discuss a method for computing moments of random variables.
* **Class materials**: [Lecture notes](day28.pdf)
* **After class**: Finish [Homework 7](hw7.pdf).

##### Friday

* **Topic**: Wrap up. We discuss the central limit theorem and what you might explore after a first probability class.
* **Class materials**: [Lecture notes](day29.pdf)
* **After class**: Work on [Exam 3](exam3.pdf).



><#include open-accordion-panel.txt><#include div-close.txt>

## Getting help ##

Here are a few ways to get help:

-   **Office hours**: <#include officehours-s21m2.txt>   
-	**Class TA**: Information posted on Moodle.
-	**Study groups**: Other students in the class are a wonderful resource.
	I want our class to feel like a community of people working together.
-	**Piazza**: I've set up an [online forum][] for asking and answering questions about
	class material, including homework problems. My plan is to check the forum every day
	and answer questions as they come up, but I hope everyone in the class will pitch in 
	and answer others' questions when possible.
	
## Resources ##

-	When away from campus, a [college VPN][] is available to access resources normally only
	available when on the campus network (eg. free e-books through LITS and Springer Link).
	However, I understand there might be some accessibility issues even with the VPN, 
	so I will do my best to help everyone access 
	necessary materials through other methods as well.
-	I've collected some resources to help you when writing homework solutions with [LaTeX][] and [RMarkdown][].
	* Here is a [RMarkdown/LaTeX template][] file for writing homework solutions and its [pdf output][].
	* A [LaTeX quick reference][] is available for commonly used symbols.
	* [RStudio Server][] is a cloud service that lets you edit and compile R and RMarkdown files through a web
		browser so that no local installation is needed. The server is hosted on the MHC network and you need
		to be on the VPN to access it if you're away from campus.
	* You can also install [R][] and [RStudio][] locally on your personal computer 
	(you must install R before RStudio), or you can also use [RStudio Cloud][], 
	which is a commercial RStudio cloud service with a free tier.
-	There are many probability books out there. Reading our required textbook will be enough
	to get a strong understanding of the material in class, but if you're curious, here
	are some other good books. 
	* *A First Course in Probability* by Sheldon Ross. Fundamentally similar in content as our textbook,
	but without the nuanced view toward real world applications and simulation.
	A big positive is that it has an immense number of exercises.
	* *Introduction to Probability* by Dimitri Bertsekas and John Tsitsiklis. Similar to our textbook, but more barebones.
	Covers the important topics well, but it seems to miss the interesting detours and exercises that make probability come alive.
	It's nice as a quick reference for the basics.
	There is also a great [MIT open course][] based on the textbook given by one of its authors which has video lectures and notes.
-	Our textbook has a useful collection of [R scripts][] available; contained
	there are all the R code snippets you'll notice interspersed in the text.
-	Here is an archive of the [Spring 2021, Module 1, class web page](./s21m1/).


[e-text]: http://www.mtholyoke.eblib.com/EBLWeb/patron/?target=patron&extendedid=P_1449975_0
[syllabus]: ./syllabus
[guidelines]: /~tchumley/assets/pdf/probability_writing.pdf
[Gradescope]: https://www.gradescope.com
[short tutorial]: https://gradescope-static-assets.s3.amazonaws.com/help/submitting_hw_guide.pdf
[online forum]: https://piazza.com/mtholyoke/spring2021/math34202/home
[Course Announcements Moodle forum]: https://moodle.mtholyoke.edu/mod/forum/view.php?id=620160


[LaTeX]: https://en.wikipedia.org/wiki/LaTeX
[RMarkdown]: https://en.wikipedia.org/wiki/Knitr
[college VPN]: https://lits.mtholyoke.edu/tech-support/access-and-internet-connectivity/connect-campus/using-virtual-private-network-vpn
[RMarkdown/LaTeX template]: /~tchumley/assets/r/homework_template.Rmd
[pdf output]: /~tchumley/assets/pdf/homework_template.pdf
[LaTeX quick reference]: https://www.caam.rice.edu/~heinken/latex/symbols.pdf
[RStudio Server]: http://rstudio.mtholyoke.edu/
[R]: https://cran.rstudio.com
[RStudio]: https://rstudio.com/products/rstudio/download/#download
[RStudio cloud]: https://rstudio.cloud
[MIT open course]: https://ocw.mit.edu/resources/res-6-012-introduction-to-probability-spring-2018/index.htm
[R scripts]: http://www.people.carleton.edu/~rdobrow/Probability/RScripts.html



