---
title: Problem Solving Club
footer: Last modified <#exec date>
<#include nav-prob-solving.md>
header-includes:
    <meta name="robots" content="noindex" />
---

### About

The Problem Solving Club at MHC meets (roughly) every week in the Fall and Spring semesters to work on fun, challenging math problems.  The problems are usually easy to state but tricky to solve, and sometimes they'll introduce you to math you've never seen before!  In the fall semester, we focus on problems similar to those on the [Putnam Competition][] exam that takes place on the first Saturday of December every year.

If you'd like to be added to the club's mailing list, send a message to [Tim Chumley][] or [Jessica Sidman][].

### Meetings

Problem Solving Club will meet again this Spring 2017 on **Fridays at 12pm** in the Math/Stat Lounge, **Clapp 416**.  Lunch will be served!

### Problem of the week

This semester the Problem Solving Club will have a problem of the week.  At each Friday meeting, a new problem will be presented, with solutions to be discussed the next week.  

Please feel free to share the problem of the week.  Anyone that wishes to write up solutions and turn them in by the following Friday will be invited to join us for an end of the semester lunch/problem solving party!  It's not necessary to turn something in every week, but we want to encourage everyone to practice their writing.  Please **submit solutions by Friday at 12pm** under the office door at Clapp 404b, or at the Friday meetings.


  Week            Problem                   Solution      
  ---------       --------          		-----------
  Feb 13 - Feb 17 [(pdf)](week1.pdf)		[(pdf)](week1s.pdf)
  Feb 20 - Feb 24 [(pdf)](week2.pdf)		.
  Feb 27 - Mar 3  [(pdf)](week3.pdf)		.
  Mar 6 - Mar 10  [(pdf)](week4.pdf)		.




[Putnam Competition]: http://math.scu.edu/putnam/
[Tim Chumley]: tchumley@mtholyoke.edu
[Jessica Sidman]: jsidman@mtholyoke.edu
