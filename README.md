# web-cv

A static website generator used to maintain a collection of course web pages and
an academic CV. The web pages and CV are written in a mixture of Markdown and 
LaTeX.  The site and CV files are converted to html and pdf using a 
collection of open source tools.  A Gitlab CI runner yml file is included for 
automating builds and deploying to a web server.

## Getting Started

Clone this repository and modify any markdown files in the source or source-cv directory.  Run compile-cv.sh and compile-web.sh to generate html and pdf files.

### Prerequisites

The build scripts require the following software

-   pandoc
-   GPP
-   bib2bib
-   bib2html
-   latexmk
-   pdflatex
