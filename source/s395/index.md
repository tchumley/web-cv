---
title: Stat 395 
footer: Last modified <#exec date>
<#include nav-s395.yml>
---

<img style="float: right;" src="/~tchumley/assets/img/m339.png" width=350px class="img-responsive">

**Course**: Statistical analysis of network data  
**Participants**:

-	Tim Chumley
-	Areeb Khichi
-	Shirley Xu
-	Young Yang

**Textbook**: *Statistical analysis of network data with R*, by Eric D. Kolaczyk and Gábor Csárdi, ISBN: 978-1-4939-0982-7

## Description ##

Networks and network analysis are arguably one of the largest recent growth areas in the 
quantitative sciences.  With roots in mathematics and statistics (graph theory), computer 
science, and the social sciences, its widely interdisciplinary nature makes it a particularly
interesting subject.  In this independent study reading course, we take the approach of 
studying networks and network data through statistical analyses aided by the use of computational
tools such as R.

Our main goals are the following:

-	learn some fundamental concepts graph theory and random graph models
-	learn statistical techniques to study real world network data examples
-	use the igraph R package to do visualization and and quantitative analyses of models
-	more to be updated


## Resources ##

-	**Texts**:
	+	*Statistical analysis of network data with R*, by Eric D. Kolaczyk and Gábor Csárdi
	+	*Statistical Analysis of Network Data: Methods and Models* by Eric D. Kolaczyk
	+	*Networks: An Introduction* by Mark Newman

-	[36-720, Statistical Network Models][] course at CMU run by Cosma Shalizi

## Calendar ##

We'll meet once a week on Fridays at 4:00pm.  Topics discussed are noted below, together
with corresponding R code and Markdown files that accompany the week's reading.

 
  -------------------------------------------------------------------------------------------------------------------------------------------------
  Week              		Topic			   								Activities																						
  ------------------------ 	-------			   								--------------------	     											
  Sep 4 - Sep 8     		Organizational meeting							
  
  Sep 11 - Sep 15   		Manipulating network data						[Chapter 2 notes](Networks_Ch2.html)

  Sep 18 - Sep 22   		Visualizing network data						[Chapter 3 notes](Networks_Ch3.html)																								
								  
  Sep 25 - Sep 29   		Descriptive analysis of network properties		
  
  Oct 2 - Oct 6     		Counting $k$-cycles								[Chapter 4 notes](Networks_Ch4.html)          

  Oct 9 - Oct 13    		Classical random graph models					[Random graph notes](random-graphs.pdf) 
  
  Oct 16 - Oct 20   		break for Lynk Symposium    
  
  Oct 23 - Oct 27   		More on structure of classical models	  		[Chapter 5 notes](Networks_Ch5.Rmd)
  
  Oct 30 - Nov 3    		Small world models and preferential attachment     
  
  Nov 6 - Nov 10    		Intro to ERGMs, part 1    						[Chapter 6 notes](Networks_Ch6.Rmd)
  
  Nov 13 - Nov 17   		no meeting   

  Nov 20 - Nov 24   		Intro to ERGMs, part 2       	  
  
  Nov 27 - Dec 1    		Stochastic root finding   						[Robbins, Monro paper](https://projecteuclid.org/euclid.aoms/1177729586)
  
  Dec 4 - Dec 8     		Fitting ERGMs    								[ERGM fitting notes](ergm.Rmd)
  
  ----------------------------------------------------------------------------------------------------------------




	
[36-720, Statistical Network Models]: http://www.stat.cmu.edu/~cshalizi/networks/16-1/

